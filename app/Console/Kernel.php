<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        date_default_timezone_set("Asia/Jakarta");
        $hour = date('H');
        if($hour > '06' && $hour < '21'){
          $schedule->call('App\Http\Controllers\KirimController@kirim')->everyMinute();
          $schedule->call('App\Http\Controllers\KirimController@kirim_pic')->everyMinute();
        }

       //$schedule->call('App\Http\Controllers\whatsappController@auto_correction')->everyMinute();
       //$schedule->call('App\Http\Controllers\whatsappController@auto_phone')->everyMinute();
       $schedule->call('App\Http\Controllers\whatsappController@check_whatsapp')->everyMinute();
       $schedule->call('App\Http\Controllers\DashboardController@geolocation')->everyMinute();
       $schedule->call('App\Http\Controllers\ClientController@auto_parent_name')->everyMinute();

       $schedule->call('App\Http\Controllers\MailController@kirim_compose')->everyMinute();
       $schedule->call('App\Http\Controllers\MailController@verifikasi_email')->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
