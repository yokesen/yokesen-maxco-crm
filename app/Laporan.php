<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Laporan extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
  ];

  public $timestamps = true;

  public function comments()
  {
    return $this->hasMany('App\Laporan', 'parent_post');
  }

  public function komentator()
  {
    return $this->hasOne('App\Users_cabang', 'id', 'user_id');
  }
}
