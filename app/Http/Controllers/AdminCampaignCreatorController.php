<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use Route;

class AdminCampaignCreatorController extends \crocodicstudio\crudbooster\controllers\CBController {

  public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "title";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = false;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = false;
			$this->button_detail = false;
			$this->button_show = false;
			$this->button_filter = false;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "campaign";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Page","name"=>"page"];
			$this->col[] = ["label"=>"Media","name"=>"media","image"=>true];
			$this->col[] = ["label"=>"Url","name"=>"url"];
			$this->col[] = ["label"=>"Status","name"=>"active"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Page','name'=>'page','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-7','placeholder'=>'Nama Campaign'];
			$this->form[] = ['label'=>'Url','name'=>'url','type'=>'text','validation'=>'required|url','width'=>'col-sm-7','placeholder'=>'Please enter a valid URL'];
			$this->form[] = ['label'=>'Title','name'=>'title','type'=>'text','validation'=>'required|string|min:3|max:100','width'=>'col-sm-7','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'First Line of email Body','name'=>'line1','type'=>'textarea','width'=>'col-sm-10','placeholder'=>'Greetings! and summary'];
			$this->form[] = ['label'=>'Second Line of email Body','name'=>'line2','type'=>'textarea','width'=>'col-sm-10','placeholder'=>'Explain Everything that matter'];
			$this->form[] = ['label'=>'Closing Line of email Body','name'=>'line3','type'=>'textarea','width'=>'col-sm-10','placeholder'=>'Conclusion why you dont want to miss it'];
			$this->form[] = ['label'=>'Media','name'=>'media','type'=>'upload','validation'=>'required|image|max:1000','width'=>'col-sm-5','help'=>'format .JPG atau .JPEG 1140x350 px'];
			$this->form[] = ['label'=>'Call to Action Button','name'=>'coa','type'=>'text','width'=>'col-sm-3','placeholder'=>'Subscribe Now!'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Page','name'=>'page','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-7','placeholder'=>'Nama Campaign'];
			//$this->form[] = ['label'=>'Url','name'=>'url','type'=>'text','validation'=>'required|url','width'=>'col-sm-7','placeholder'=>'Please enter a valid URL'];
			//$this->form[] = ['label'=>'Title','name'=>'title','type'=>'text','validation'=>'required|string|min:3|max:100','width'=>'col-sm-7','placeholder'=>'You can only enter the letter only'];
			//$this->form[] = ['label'=>'First Line of email Body','name'=>'line1','type'=>'textarea','width'=>'col-sm-10','placeholder'=>'Greetings! and summary'];
			//$this->form[] = ['label'=>'Second Line of email Body','name'=>'line2','type'=>'textarea','width'=>'col-sm-10','placeholder'=>'Explain Everything that matter'];
			//$this->form[] = ['label'=>'Closing Line of email Body','name'=>'line3','type'=>'textarea','width'=>'col-sm-10','placeholder'=>'Conclusion why you dont want to miss it'];
			//$this->form[] = ['label'=>'Media','name'=>'media','type'=>'upload','validation'=>'required|image|max:1000','width'=>'col-sm-5','help'=>'format .JPG atau .JPEG 1140x350 px'];
			//$this->form[] = ['label'=>'Call to Action Button','name'=>'coa','type'=>'text','width'=>'col-sm-3','placeholder'=>'Subscribe Now!'];
			# OLD END FORM

			/*
      | ----------------------------------------------------------------------
      | Sub Module
      | ----------------------------------------------------------------------
	| @label          = Label of action
	| @path           = Path of sub module
	| @foreign_key 	  = foreign key of sub table/module
	| @button_color   = Bootstrap Class (primary,success,warning,danger)
	| @button_icon    = Font Awesome Class
	| @parent_columns = Sparate with comma, e.g : name,created_at
      |
      */
      $this->sub_module = array();


      /*
      | ----------------------------------------------------------------------
      | Add More Action Button / Menu
      | ----------------------------------------------------------------------
      | @label       = Label of action
      | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
      | @icon        = Font awesome class icon. e.g : fa fa-bars
      | @color 	   = Default is primary. (primary, warning, succecss, info)
      | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
      |
      */
      // $this->addaction = array();
      $this->addaction[] = ['label'=>'On','url'=>CRUDBooster::mainpath('set-status/active/[id]'),'color'=>'success', 'showIf'=>'[active] == "no"', 'confirmation' => true];
      $this->addaction[] = ['label'=>'Off','url'=>CRUDBooster::mainpath('set-status/deactive/[id]'),'color'=>'danger', 'showIf'=>'[active] == "yes"', 'confirmation' => true];


      /*
      | ----------------------------------------------------------------------
      | Add More Button Selected
      | ----------------------------------------------------------------------
      | @label       = Label of action
      | @icon 	   = Icon from fontawesome
      | @name 	   = Name of button
      | Then about the action, you should code at actionButtonSelected method
      |
      */
      $this->button_selected = array();


      /*
      | ----------------------------------------------------------------------
      | Add alert message to this module at overheader
      | ----------------------------------------------------------------------
      | @message = Text of message
      | @type    = warning,success,danger,info
      |
      */
      $this->alert        = array();



      /*
      | ----------------------------------------------------------------------
      | Add more button to header button
      | ----------------------------------------------------------------------
      | @label = Name of button
      | @url   = URL Target
      | @icon  = Icon from Awesome.
      |
      */
      $this->index_button = array();



      /*
      | ----------------------------------------------------------------------
      | Customize Table Row Color
      | ----------------------------------------------------------------------
      | @condition = If condition. You may use field alias. E.g : [id] == 1
      | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
      |
      */
      $this->table_row_color = array();


      /*
      | ----------------------------------------------------------------------
      | You may use this bellow array to add statistic at dashboard
      | ----------------------------------------------------------------------
      | @label, @count, @icon, @color
      |
      */
      $this->index_statistic = array();



      /*
      | ----------------------------------------------------------------------
      | Add javascript at body
      | ----------------------------------------------------------------------
      | javascript code in the variable
      | $this->script_js = "function() { ... }";
      |
      */
      $this->script_js = NULL;


        /*
      | ----------------------------------------------------------------------
      | Include HTML Code before index table
      | ----------------------------------------------------------------------
      | html code to display it before index table
      | $this->pre_index_html = "<p>test</p>";
      |
      */
      $this->pre_index_html = null;



      /*
      | ----------------------------------------------------------------------
      | Include HTML Code after index table
      | ----------------------------------------------------------------------
      | html code to display it after index table
      | $this->post_index_html = "<p>test</p>";
      |
      */
      $this->post_index_html = null;



      /*
      | ----------------------------------------------------------------------
      | Include Javascript File
      | ----------------------------------------------------------------------
      | URL of your javascript each array
      | $this->load_js[] = asset("myfile.js");
      |
      */
      $this->load_js = array();



      /*
      | ----------------------------------------------------------------------
      | Add css style at body
      | ----------------------------------------------------------------------
      | css code in the variable
      | $this->style_css = ".style{....}";
      |
      */
      $this->style_css = NULL;



      /*
      | ----------------------------------------------------------------------
      | Include css File
      | ----------------------------------------------------------------------
      | URL of your css each array
      | $this->load_css[] = asset("myfile.css");
      |
      */
      $this->load_css = array();


  }


  /*
  | ----------------------------------------------------------------------
  | Hook for button selected
  | ----------------------------------------------------------------------
  | @id_selected = the id selected
  | @button_name = the name of button
  |
  */
  public function actionButtonSelected($id_selected,$button_name) {
      //Your code here

  }


  /*
  | ----------------------------------------------------------------------
  | Hook for manipulate query of index result
  | ----------------------------------------------------------------------
  | @query = current sql query
  |
  */
  public function hook_query_index(&$query) {
      //Your code here

  }

  /*
  | ----------------------------------------------------------------------
  | Hook for manipulate row of index table html
  | ----------------------------------------------------------------------
  |
  */
  public function hook_row_index($column_index,&$column_value) {
  	if($column_index == 3){
      if($column_value == "yes"){
        $column_value = "Active";
      }else{
        $column_value = "Inactive";
      }
    }
  }

  /*
  | ----------------------------------------------------------------------
  | Hook for manipulate data input before add data is execute
  | ----------------------------------------------------------------------
  | @arr
  |
  */
  public function hook_before_add(&$postdata) {
      //Your code here
      $postdata['desc'] = $postdata['line1'];
      $data = [
        'title' => $postdata['page'],
        'link' => $postdata['url'],
        'line1' => $postdata['line1'],
        'line2' => $postdata['line2'],
        'line3' => $postdata['line3'],
        'callToAction' => $postdata['coa'],
      ];
      $postdata['email'] = serialize($data);
      $postdata['count'] = 0;
      $postdata['active'] = "no";

      unset($postdata['line1']);
      unset($postdata['line2']);
      unset($postdata['line3']);
      unset($postdata['coa']);
  }

  /*
  | ----------------------------------------------------------------------
  | Hook for execute command after add public static function called
  | ----------------------------------------------------------------------
  | @id = last insert id
  |
  */
  public function hook_after_add($id) {
      //Your code here
  }

  /*
  | ----------------------------------------------------------------------
  | Hook for manipulate data input before update data is execute
  | ----------------------------------------------------------------------
  | @postdata = input post data
  | @id       = current id
  |
  */
  public function hook_before_edit(&$postdata,$id) {
      //Your code here
      $postdata['desc'] = $postdata['line1'];
      $data = [
        'title' => $postdata['page'],
        'link' => $postdata['url'],
        'line1' => $postdata['line1'],
        'line2' => $postdata['line2'],
        'line3' => $postdata['line3'],
        'callToAction' => $postdata['coa'],
      ];
      $postdata['email'] = serialize($data);
      $postdata['count'] = 0;
      $postdata['active'] = "no";

      unset($postdata['line1']);
      unset($postdata['line2']);
      unset($postdata['line3']);
      unset($postdata['coa']);

  }

  /*
  | ----------------------------------------------------------------------
  | Hook for execute command after edit public static function called
  | ----------------------------------------------------------------------
  | @id       = current id
  |
  */
  public function hook_after_edit($id) {
      //Your code here

  }

  /*
  | ----------------------------------------------------------------------
  | Hook for execute command before delete public static function called
  | ----------------------------------------------------------------------
  | @id       = current id
  |
  */
  public function hook_before_delete($id) {
      //Your code here

  }

  /*
  | ----------------------------------------------------------------------
  | Hook for execute command after delete public static function called
  | ----------------------------------------------------------------------
  | @id       = current id
  |
  */
  public function hook_after_delete($id) {
      //Your code here

  }



  //By the way, you can still create your own method in here... :)
  // public function getAdd() {
  //   if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_add==FALSE) {
  //     CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
  //   }
  //   $data = [];
  //   $data['page_title'] = 'Add Campaign';
  //   $data['return_url'] = CRUDBooster::mainpath();
  //
  //   //Please use cbView method instead view method from laravel
  //   $this->cbView('custom_campaign_add',$data);
  // }

  public function getSetStatus($status,$id) {
    switch($status){
      case 'active' : $status = "yes"; break;
      case 'deactive' : $status = "no"; break;
    }

   DB::table('campaign')->where('id', $id)->update(['active'=>$status]);

   //This will redirect back and gives a message
   CRUDBooster::redirect($_SERVER['HTTP_REFERER'],"Status campaign berhasil dirubah!","success");
  }

  public function getEdit($id){
    $this->cbLoader();
		$row = DB::table($this->table)->where($this->primary_key,$id)->first();

		if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {
			CRUDBooster::insertLog(trans("crudbooster.log_try_edit",['name'=>$row->{$this->title_field},'module'=>CRUDBooster::getCurrentModule()->name]));
			CRUDBooster::redirect(CRUDBooster::adminPath(),trans('crudbooster.denied_access'));
		}

    $unser = unserialize($row->email);
    $row->line1 = $unser['line1'];
    $row->line2 = $unser['line2'];
    $row->line3 = $unser['line3'];
    $row->coa = $unser['callToAction'];

		$page_menu = Route::getCurrentRoute()->getActionName();
		$page_title = trans("crudbooster.edit_data_page_title",['module'=>CRUDBooster::getCurrentModule()->name,'name'=>$row->{$this->title_field}]);
		$command = 'edit';
		Session::put('current_row_id',$id);
		return view('crudbooster::default.form',compact('id','row','page_menu','page_title','command'));
	}
}
