<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class ApiController extends Controller
{
    //
    public function upsertclient(Request $request){
        $getData = DB::table('clients')
        ->where(['email'=>$request->email])
        ->orWhere('phone', '$request->phone')
        ->first();
        
        if(empty($getData)){
            $dataset = [
                'name' =>$request->name,
                'email' =>$request->email,
                'phone' =>$request->phone,
                'ktp' =>$request->ktp,
                'parent' =>$request->parent,
                'origin' =>$request->origin,
                'campaign' =>$request->campaign,
                'media_iklan' =>$request->media_iklan,
                'previous_url' =>$request->previous_url,
                'ipaddress' =>$request->ipaddress,
                'desktop' =>$request->desktop,
                'device' =>$request->device,
                'lang' =>$request->lang,
                'uuid' =>$request->uuid,
            ];
            $datainsert = DB::table('clients')->insertGetId($dataset);
        }else{
            $dataset = [
                'name' =>!empty($request->name)?$request->name:$getData->name,
                'email' =>!empty($request->email)?$request->email:$getData->email,
                'phone' =>!empty($request->phone)?$request->phone:$getData->phone,
                'ktp' =>!empty($request->ktp)?$request->ktp:$getData->ktp,
                'origin' =>!empty($request->origin)?$request->origin:$getData->origin,
                'campaign' =>!empty($request->campaign)?$request->campaign:$getData->campaign,
                'media_iklan' =>!empty($request->media_iklan)?$request->media_iklan:$getData->media_iklan,
                'previous_url' =>!empty($request->previous_url)?$request->previous_url:$getData->previous_url,
                'ipaddress' =>!empty($request->ipaddress)?$request->ipaddress:$getData->ipaddress,
                'desktop' =>!empty($request->desktop)?$request->desktop:$getData->desktop,
                'device' =>!empty($request->device)?$request->device:$getData->device,
                'lang' =>!empty($request->lang)?$request->lang:$getData->lang,
                'uuid' =>!empty($request->uuid)?$request->uuid:$getData->uuid,
            ];
            DB::table('clients')
            ->where('id',$getData->id)
            ->update($dataset);
        }
        dd(time(),$getData,$_SERVER['HTTP_USER_AGENT']);
    }
}
