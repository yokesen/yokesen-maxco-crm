<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;

class ChatController extends Controller
{
  function pushMessage(Request $request){

    $data = $request->json()->all();

    $mesages = $data['messages'];
    $return = $mesages;

    $id = $mesages[0]['id'];
    $body = $mesages[0]['body'];
    $type = $mesages[0]['type'];
    $senderName = $mesages[0]['senderName'];
    $fromMe = $mesages[0]['fromMe'];
    $author = $mesages[0]['author'];
    $time = $mesages[0]['time'];
    $chatId = $mesages[0]['chatId'];
    $messageNumber = $mesages[0]['messageNumber'];
    $caption = $mesages[0]['caption'];

    $body = iconv("UTF-8","UTF-8//IGNORE",$body);
    $body = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $body);

    $insert_control = DB::table('push_chat_api')->insert([
      'data' => 'this is data'
    ]);


    if($body == ''){
      $body = 'icon';
    }
    $phone = str_replace('@c.us','',$chatId);

    if($mesages){
      $insert_conversation = DB::table('conversations')->insert([
        'nama' => $senderName,
        'handphone' => $phone,
        'message' => $body,
        'type' => $type,
        'fromme' => $fromMe,
        'flag' => '0',
        'msg_id' => $chatId,
        'msg_num' => $messageNumber,
        'caption' => $caption
      ]);
      //return $messageNumber;

      if($fromMe == 0){
        $client = DB::table('clients')->where('whatsapp',$phone)->first();
        $pemilik = DB::table('users_jakarta')->where('id',$client->parent)->first();


        CRUDBooster::sendNotification($config=[
          'content' => $client->name . ' send a whatsapp message',
          'to' => config('app.url') . '/client-detail/'.$client->id,
          'id_cms_users' => [$pemilik->id]
        ]);

        if(!empty($pemilik->whatsapp)){
          CRUDBooster::insert('daftar_kiriman',[
            'phone' => $pemilik->whatsapp,
            'content' => $client->name . ' barusan hubungin kamu via whatsapp, cepetan login CRM-mu. Sekarang ya!'
          ]);
        }


      }elseif($fromMe == 1){
        $delete = DB::table('conversations')->where("handphone",$phone)->where("flag",'99')->delete();
      }

    }

    $ack = $data['ack'];

      $return = $ack;
      $chatId = $ack[0]['chatId'];
      $messageNumber = $ack[0]['messageNumber'];
      $status = $ack[0]['status'];

      if($ack){


        $phone = DB::table('conversations')->where('msg_id',$chatId)->first();
        $number = $phone->handphone;

        if($status == "delivered"){

          $update = DB::table('conversations')->where('msg_id',$chatId)->where('status','waiting')->update([
            'status' => $status
          ]);

          $update = DB::table('daftar_kiriman')->where('phone',$number)->where('flag','1')->update([
            'isDelivered' => '1'
          ]);

          $update = DB::table('daftar_kiriman_pic')->where('phone',$number)->where('flag','1')->update([
            'isDelivered' => '1'
          ]);

          $client = DB::table('clients')->where('whatsapp',$number)->where('status','lead')->update([
            'status' => 'contact'
          ]);
        }


        if($status == "viewed"){

          $update = DB::table('conversations')->where('msg_id',$chatId)->where('status','delivered')->update([
            'status' => $status
          ]);

          $update = DB::table('daftar_kiriman')->where('phone',$number)->where('flag','1')->update([
            'isRead' => '1',
            'isDelivered' => '1'
          ]);

          $update = DB::table('daftar_kiriman_pic')->where('phone',$number)->where('flag','1')->update([
            'isRead' => '1',
            'isDelivered' => '1'
          ]);
        }

        //return response($return,200);
      }
  }
}
