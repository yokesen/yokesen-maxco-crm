<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use CRUDBooster;

class UploadController extends Controller
{
  public function upload(){
    $data = DB::table('clients')->where('parent',CRUDBooster::myID())->where('flag','8')->get();
    $double = DB::table('clients_double')->where('parent',CRUDBooster::myID())->where('flag','9')->get();
    $ndata = count($data);
    $ndouble = count($double);

    $ubahdata = DB::table('clients')->where('parent',CRUDBooster::myID())->where('flag','8')->update([
      'flag' => '1'
    ]);
    $ubahdouble = DB::table('clients_double')->where('parent',CRUDBooster::myID())->where('flag','9')->update([
      'flag' => '1'
    ]);

    return view('marketing.upload',compact('data','double','ndata','ndouble'));
  }

  public function upload_process(Request $request){

    Excel::selectSheets('Sheet1')->load($request->file('email')->getRealPath(), function($reader) {

      $results = $reader->get();

      //dd($results);
      foreach($results as $n => $isi){
        $nama = $isi->nama;
        $email = $isi->email;
        $phone = $isi->phone;

        //echo $phone."<br>";
        $whatsapp = $phone;
        $whatsapp = str_replace('-','',$whatsapp);
        $whatsapp = str_replace(' ','',$whatsapp);
        $whatsapp = str_replace(' ','',$whatsapp);
        $whatsapp = str_replace('.','',$whatsapp);
        $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
        $check_number = str_split($whatsapp);
        $new_number = "62";

        if($check_number[0]=='0'){
          foreach($check_number as $n => $number){
            if($n > 0){
              if($check_number[1]=='8'){
                $new_number .= $number;
              }else{
                //Alert::success('Silahkan coba lagi','Nomor Tidak Valid '.$whatsapp);
                //Return redirect()->back();
                //echo "ga bisa dibenerin nomor $isi->id <br>";
                $new_number = 'fail';

              }
            }
          }
        }else{
          if($check_number[0]=='8'){
            $new_number = "62".$whatsapp;
          }elseif($check_number[0]=='6'){
            $new_number = $whatsapp;
          }elseif($check_number[0]=='+'){
            foreach($check_number as $n => $number){
              if($n > 2){
                  $new_number .= $number;
              }
            }
          }else{
            //Alert::success('Silahkan coba lagi','Nomor Tidak Valid '.$whatsapp);
            //Return redirect()->back();
            $new_number = 'fail';
            //echo "ga bisa dibenerin  $isi->id <br>";

          }
        }
        if($new_number != 'fail'){
          $check = DB::table('clients')->where('whatsapp',$new_number)->count();


          //echo $check."<br>";

          if($check == 0){
            $insert = DB::table('clients')->insert([
              'name' => $nama,
              'email' => $email,
              'phone' => $new_number,
              'whatsapp' => $new_number,
              'status' => 'lead',
              'parent' => CRUDBooster::myID(),
              'origin' => 'Import xls',
              'password' => bcrypt('melandas2019'),
              'flag' => '8'
            ]);
          }else{
            $insert = DB::table('clients_double')->insert([
              'name' => $nama,
              'email' => $email,
              'phone' => $new_number,
              'whatsapp' => $new_number,
              'status' => 'double',
              'parent' => CRUDBooster::myID(),
              'origin' => 'Import xls',
              'password' => bcrypt('melandas2019'),
              'flag' => '9'
            ]);
          }
          //echo $new_number."<br>";
        }else{

          //echo "ada yang ga bisa dibenerin nih <br>";
        }


      }

    });

    return redirect()->route('uploadView');
  }


}
