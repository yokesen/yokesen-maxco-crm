<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Alert;
use Cookie;
use Validator;

class phoneController extends Controller
{
    //
    public function nophonenumber($email)
    {
        $countryPhoneCode = DB::table('country')->select('phonecode')->get();
        //dd($countryPhoneCode);
        return view('marketing.no-phonenumber', compact('email', 'countryPhoneCode'));
    }

    public function postphonenumber(Request $request, $email)
    {
        $rules = array(
            'countryCode' => 'required',
            'phonenumber' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            Alert::error('Buktikan kamu bukan robot!', 'ERROR')->persistent('close');
            return redirect()->route('nophonenumber', $email)->withInput()->withErrors($validator);
        }

        $no_hp = $request->phonenumber;
        $checkNumber = DB::table('users_jakarta')->where('phone', $no_hp)->count();
        if ($checkNumber > 0) {
            Alert::error('Silahkan masukkan nomor lain', 'Nomor Sudah terpakai ' . $no_hp)->persistent('close');
            return redirect()->route('nophonenumber', $email);
        } else {
            $client = new Client();
            $url = env('API_URL') . 'api/message/sendvalidatecaptcha';
            $requestPost = $client->post($url, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Language' => 'en',
                ],
                'json' => [
                    'InfoType' =>  '0',
                    'ValidateType' => '1',
                    'AreaCode' => $request->countryCode,
                    'Mobile' => $request->phonenumber,

                ]
            ]);
            $response = json_decode($requestPost->getBody());
            if ($response->Code == 0) {
                Cookie::queue('nohptemp', $no_hp, 60);
                $countryCode = $request->countryCode;
                Cookie::queue('AreaCode', $countryCode, 60);
                Alert::success('Barusan DEST mengirimi kamu kode ke nomor handphone ' . $no_hp . ' untuk dimasukin di sini.', 'Tungguin ya! ')->persistent('close');
                return redirect()->route('verifyphonenumber',$email);
            } else {
                Alert::error($response->Message, 'Error')->persistent('close');
                return redirect()->back();
            }
        }
    }

    public function verify_phonenumber($email)
    {
        return view('marketing.verify-phoneno',compact('email'));
    }

    public function verificating_phonenumber(Request $request)
    {
        $code = $request->code1 . $request->code2 . $request->code3 . $request->code4 . $request->code5 . $request->code6;
        $user = db::table('users_jakarta')->where('email', $request->email)->first();
        // dd('$user', $user, $code);
        if (Cookie::get('nohptemp')) {
            $client = new Client();
            $url = env('API_URL') . 'api/account/addsmartmaxcoibuser';
            $password = "maxco12345";
            $requestPost = $client->post($url, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Language' => 'en',
                ],
                'json' => [
                    'IBId'=> $user->id,
                    'Password' => $password,
                    'ConfirmPassword' => $password,
                    'FullName' =>  $user->name,
                    'Mobile' => Cookie::get('nohptemp'),
                    'Email' => $user->email,
                    'VerifyCode' => $code,
                    'IDNO' => !empty($user->ktp) ? $user->ktp : time(), // harus unique
                    'AreaCode'=>Cookie::get('AreaCode'),
                    'DateOfBirth' => !empty($user->dob) ? $user->dob : '1990-03-20',
                    'Gender' => 'Male',
                    'MaritalStatus' => 'Single',
                    'AttractClient' => '1',
                    'OfferService' => '1',
                    'MonthlyClients' => '1',
                    'MonthlyVolume' => '1',
                    'AverageDeposit' => '1',
                ]
            ]);
            $response = json_decode($requestPost->getBody());
            // dd($response,Cookie::get('nohptemp'),$code);
            if ($response->Code == 0) {
                $user = DB::table('users_jakarta')->where('email', $request->email)->update([
                    'phone' => Cookie::get('nohptemp'),
                    'verifikasi' => $code
                ]);
                Alert::success('Kamu telah berhasil memverifikasi nomor handphone', 'Success');
                return redirect()->route('getLogin');
            } else {
                Alert::error($response->Message, 'Error')->persistent('close');
                return redirect()->route('getLogin');
            }

            // if ($code == $user->verifikasi) {

            //     $update = db::table('users_jakarta')->where('phone', Cookie::get('watemp'))->update([
            //         'password' => bcrypt($password)
            //     ]);

            //                 CRUDBooster::insert('daftar_kiriman', [
            //                     'phone' => Cookie::get('watemp'),
            //                     'content' => "Halo, kamu telah berhasil memverifikasi whatsapp. Sekarang silahkan login dengan data sebagai berikut :

            //     email : $user->email
            //     password : $password
            //     "
            //                 ]);


            // } else {
            //     Alert::error('Silahkan coba lagi', 'Wrong Code')->persistent('close');
            //     return redirect()->route('verifyphonenumber');
            // }
        } else {
            Alert::error('Silahkan coba lagi', 'Code Expired')->persistent('close');
            return redirect()->route('nophonenumber', $user->email);
        }
    }
}
