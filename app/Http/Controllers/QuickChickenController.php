<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use DB;

class QuickChickenController extends Controller
{
    public function plu(){
      $file_n = 'http://crm.yokesen.com/storage/sid/plu.csv';
      $header = NULL;

      if (($handle = fopen($file_n, 'r')) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {

          $cek = DB::table('qc_plu')->where('plu_id',$data[0])->count();

          if($cek < 1 ){
            $insert = DB::table('qc_plu')->insert([
              'plu_id' => $data[0],
              'nama_produk' => $data[2],
              'harga' => $data[7],
              'grup_produk' => $data[4],
              'flag' => $data[8]
            ]);
          }else{

            $update = DB::table('qc_plu')->where('plu_id',$data[0])->update([
              'nama_produk' => $data[2],
              'harga' => $data[7],
              'grup_produk' => $data[4],
              'flag' => $data[8]
            ]);

          }

        }
        fclose($handle);
      }
    }

    public function publish_plu(){
      $pull_db = DB::table('qc_plu')->orderby('id','asc')->get();
      $contents = '';
      foreach($pull_db as $n => $pull){
        $contents .= $pull->plu_id.';'.$pull->plu_id.';"'.$pull->nama_produk.'";"'.$pull->nama_produk.'";'.$pull->grup_produk.';;;'.$pull->harga.';'.$pull->flag.';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;'."\n";
      }
      $namefile = "plu.csv";
      Storage::put('public/sid/'.$namefile, $contents);
      return redirect()->back();
    }
}
