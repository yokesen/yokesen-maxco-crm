<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;

class ReksadanaController extends \crocodicstudio\crudbooster\controllers\CBController
{
  public function release($id){
    //dd($id);
    $data = DB::table('pembelian')->where('id',$id)->first();

    $tgl_release = date('Y-m-d H:i:s');
    $status = 'new';
    $releaser = CRUDBooster::myID();
    $update = DB::table('pembelian')->where('id',$id)->update([
      'tgl_approve' => $tgl_release,
      'status' => 'released',
      'releaser' => $releaser,
      'flag' => '1'
    ]);

    CRUDBooster::redirect($_SERVER['HTTP_REFERER'],"SUBCRIPTION BERHASIL DI RELEASE","success");
  }
}
