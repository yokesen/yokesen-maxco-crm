<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;

class BannerController extends \crocodicstudio\crudbooster\controllers\CBController
{
  public function cbInit()
  {
  }

  public function index()
  {
    $banners = DB::table('banners')->where('status', 'Active')->get();
    $this->cbView('tools.banner', compact('banners'));
  }
}
