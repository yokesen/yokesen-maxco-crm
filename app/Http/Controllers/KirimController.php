<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class KirimController extends Controller
{

    public function kirim(){
      /*
      date_default_timezone_set("Asia/Jakarta");
      $date = date('Y-m-d');
      $number_of_subscribers = DB::table('daftar_kiriman')->where('group_2','!=','1')->whereDate('send_at',$date)->get();
      $number_of_messages = DB::table('daftar_kiriman')->where('group_2','!=','1')->whereDate('send_at',$date)->select('phone')->distinct()->get();

      $nus = count($number_of_subscribers);
      $num = count($number_of_messages);

      if($nus < 5000 && $num < 20000){
        echo $nus." nus<br>";
        echo $num." num<br>";

        $client = new Client();
        $pull = DB::table('daftar_kiriman')->where('flag','2')->limit('3')->get();
        foreach($pull as $n => $isi){
          echo "begin check queing <br>";
          $check_queue = $client->post(env('WA_INSTANCE').'showMessagesQueue?token='.env('WA_TOKEN'));
          $decode_queue = json_decode($check_queue->getBody());
          $queue = $decode_queue->totalMessages;
          echo $queue." queing<br>";

          if($queue < 4 ){
            $res = $client->post(env('WA_INSTANCE').'sendMessage?token='.env('WA_TOKEN'),[
              'form_params' => [
                "phone" => $isi->phone,
                "body" => $isi->content
              ]
            ]);
            $date = date('Y-m-d H:i:s');

            $update = DB::table('daftar_kiriman')->where('id',$isi->id)->update([
              'flag' => '1',
              'send_at' => $date
            ]);
          }


          sleep(3);
        }
      }

      */
    }

    public function kirim_pic(){
      /*
      date_default_timezone_set("Asia/Jakarta");
      $date = date('Y-m-d');
      $number_of_subscribers = DB::table('daftar_kiriman_pic')->where('group_2','!=','1')->whereDate('send_at',$date)->get();
      $number_of_messages = DB::table('daftar_kiriman_pic')->where('group_2','!=','1')->whereDate('send_at',$date)->select('phone')->distinct()->get();

      $nus = count($number_of_subscribers);
      $num = count($number_of_messages);

      echo $nus."<br>";
      echo $num."<br>";

      if($nus < 500 && $num < 2000){
        $client = new Client();
        $pull = DB::table('daftar_kiriman_pic')->where('flag','2')->limit('3')->get();
        //dd($pull);
        foreach($pull as $n => $isi){
          $check_queue = $client->post(env('WA_INSTANCE').'showMessagesQueue?token='.env('WA_TOKEN'));
          $decode_queue = json_decode($check_queue->getBody());
          $queue = $decode_queue->totalMessages;
          $body = trim(preg_replace('/\s\s+/', ' ', $isi->filename));
          echo $queue."<br>".$body."<br>";

          if($queue < 4 ){
            $time = time();

            $res = $client->post(env('WA_INSTANCE').'sendFile?token='.env('WA_TOKEN'),[
              'form_params' => [
                "phone" => $isi->phone,
                "body" => url('/').$body,
                "filename" => $time.".jpg",
                "caption" => $isi->content
              ]
            ]);

            $date = date('Y-m-d H:i:s');

            $update = DB::table('daftar_kiriman_pic')->where('id',$isi->id)->update([
              'flag' => '1',
              'send_at' => $date
            ]);
          }

          sleep(3);

        }
      }

      */
    }

}
