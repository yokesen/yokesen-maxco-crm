<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use App\Laporan;

class LaporanController extends \crocodicstudio\crudbooster\controllers\CBController
{
  public function cbInit()
  {
  }

  public function index(){
    $laporan = Laporan::where('parent_post', '0')
    ->where(function($q){
      $q->orWhere([
        'user_id' => CRUDBooster::myID(),
        'lev_1' => CRUDBooster::myID(),
        'lev_2' => CRUDBooster::myID(),
        'lev_3' => CRUDBooster::myID(),
        'lev_4' => CRUDBooster::myID(),
        'lev_5' => CRUDBooster::myID(),
        'lev_6' => CRUDBooster::myID(),
      ]);
    })
    ->orderby('id','desc')
    ->with([
      'comments',
    ])->get();

    $user = DB::table('users_jakarta')
            ->join('penamaans','users_jakarta.level','penamaans.level')
            ->where('users_jakarta.id',CRUDBooster::myID())
            ->first();
    $this->cbView('marketing.laporan',compact('laporan','user'));
  }

  public function postComment(Request $request){
    DB::table('laporans')->insert([
      'parent_post' => $request->post_id,
      'content' => '<p>' .$request->comment. '</p>',
      'user_id' => CRUDBooster::myID(),
      'created_at' => now()
    ]);
    //notifikasi ke laporans.user_id where post = parent_post
    //update status_level = timestamp where lev_level = crudbooster::myID() and post = parent_post 
    return redirect()->back();
  }
}
