<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminMyTeamLevel1Controller extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "name";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = false;
			$this->button_show = false;
			$this->button_filter = false;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "users_jakarta";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Sales ID","name"=>"id"];
			$this->col[] = ["label"=>"Name","name"=>"name"];
			$this->col[] = ["label"=>"Email","name"=>"email"];
			$this->col[] = ["label"=>"Parent","name"=>"parent","join"=>"users_jakarta,name"];
			$this->col[] = ["label"=>"Branch Manager","name"=>"id"];
			$this->col[] = ["label"=>"SBDM","name"=>"id"];
			$this->col[] = ["label"=>"BDM","name"=>"id"];
			$this->col[] = ["label"=>"SBM","name"=>"id"];
			$this->col[] = ["label"=>"BM","name"=>"id"];
			$this->col[] = ["label"=>"PO","name"=>"id"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Name','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-3','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','validation'=>'required|min:1|max:255|email|unique:users_jakarta','width'=>'col-sm-3','placeholder'=>'Please enter a valid email address'];
			$this->form[] = ['label'=>'Password','name'=>'password','type'=>'password','validation'=>'min:3|max:32','width'=>'col-sm-3','help'=>'Minimum 5 characters. Please leave empty if you did not change the password.'];
			$this->form[] = ['label'=>'Parent','name'=>'parent','type'=>'select2','validation'=>'required','width'=>'col-sm-3','dataenum'=>'2|My Self','datatable'=>'users_jakarta,name','datatable_where'=>'id_cms_privileges != 1 and parent='.CRUDBooster::myID().' or lev_1='.CRUDBooster::myID().' or lev_2='.CRUDBooster::myID().' or lev_3='.CRUDBooster::myID().' or lev_4='.CRUDBooster::myID().' or lev_5='.CRUDBooster::myID().' or lev_6='.CRUDBooster::myID(),'dataenum'=>CRUDBooster::myID().'|My Self'];
			$this->form[] = ['label'=>'Level','name'=>'level','type'=>'select','validation'=>'required|integer|min:0','width'=>'col-sm-3','datatable'=>'penamaans,longname'];
			$this->form[] = ['label'=>'Status','name'=>'status','type'=>'select','validation'=>'required','width'=>'col-sm-3','dataenum'=>'Active;Non-Active;Resign;Banned;Pending'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Name','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-3','placeholder'=>'You can only enter the letter only'];
			//$this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','validation'=>'required|min:1|max:255|email|unique:users_jakarta','width'=>'col-sm-3','placeholder'=>'Please enter a valid email address'];
			//$this->form[] = ['label'=>'Password','name'=>'password','type'=>'password','validation'=>'min:3|max:32','width'=>'col-sm-3','help'=>'Minimum 5 characters. Please leave empty if you did not change the password.'];
			//$this->form[] = ['label'=>'Parent','name'=>'parent','type'=>'select2','validation'=>'required','width'=>'col-sm-3','datatable'=>'users_jakarta,name','datatable_where'=>'id_cms_privileges != 1','dataenum'=> CRUDBooster::myID().'|My Self'];
			//$this->form[] = ['label'=>'Level','name'=>'level','type'=>'select','validation'=>'required|integer|min:0','width'=>'col-sm-3','dataenum'=>'2|Vice Branch;3|SBDM;4|BDM;5|SBM;6|BM ;7|PO'];
			//$this->form[] = ['label'=>'Status','name'=>'status','type'=>'select','validation'=>'required','width'=>'col-sm-3','dataenum'=>'Active;Non-Active;Resign;Banned;Pending'];
			# OLD END FORM

			/*
	        | ----------------------------------------------------------------------
	        | Sub Module
	        | ----------------------------------------------------------------------
			| @label          = Label of action
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        |
	        */
	        $this->sub_module = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        |
	        */
	        $this->addaction = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Button Selected
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button
	        | Then about the action, you should code at actionButtonSelected method
	        |
	        */
	        $this->button_selected = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------
	        | @message = Text of message
	        | @type    = warning,success,danger,info
	        |
	        */
	        $this->alert        = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add more button to header button
	        | ----------------------------------------------------------------------
	        | @label = Name of button
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        |
	        */
	        $this->index_button = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
	        |
	        */
	        $this->table_row_color = array();


	        /*
	        | ----------------------------------------------------------------------
	        | You may use this bellow array to add statistic at dashboard
	        | ----------------------------------------------------------------------
	        | @label, @count, @icon, @color
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add javascript at body
	        | ----------------------------------------------------------------------
	        | javascript code in the variable
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code before index table
	        | ----------------------------------------------------------------------
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code after index table
	        | ----------------------------------------------------------------------
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include Javascript File
	        | ----------------------------------------------------------------------
	        | URL of your javascript each array
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add css style at body
	        | ----------------------------------------------------------------------
	        | css code in the variable
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;



	        /*
	        | ----------------------------------------------------------------------
	        | Include css File
	        | ----------------------------------------------------------------------
	        | URL of your css each array
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();


	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for button selected
	    | ----------------------------------------------------------------------
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here

	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate query of index result
	    | ----------------------------------------------------------------------
	    | @query = current sql query
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
					 $query->where('users_jakarta.id_cms_privileges','!=','1')->where('users_jakarta.id','>','9999')->where('users_jakarta.lev_1',CRUDBooster::myID());
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate row of index table html
	    | ----------------------------------------------------------------------
	    |
	    */
	    public function hook_row_index($column_index,&$column_value) {
	    	//Your code here
				if($column_index==5){
					$user = DB::table('users_jakarta')->where('id',$column_value)->first();
					$level = $user->level;
					$lev_2 = $user->lev_2;
					if($level == '2'){
						$act = DB::table('clients')->where('status','contact')->where('lev_2',$column_value)->count();
						$mkt = DB::table('users_jakarta')->where('lev_2',$column_value)->count();
							$client = DB::table('clients')->where('lev_2',$column_value)->count();
							$column_value = '<span class="label label-warning">'.$mkt.'</span> / <span class="label label-primary">'.$client.'</span> / <span class="label label-success">'.$act.'</span>';
					}else{
						$column_value = DB::table('users_jakarta')->where('id',$lev_2)->value('name');
					}
				}

				if($column_index==6){
					$user = DB::table('users_jakarta')->where('id',$column_value)->first();
					$level = $user->level;
					$lev_3 = $user->lev_3;
					if($level == '3'){
						$act = DB::table('clients')->where('status','contact')->where('lev_3',$column_value)->count();
						$mkt = DB::table('users_jakarta')->where('lev_3',$column_value)->count();
							$client = DB::table('clients')->where('lev_3',$column_value)->count();
							$column_value = '<span class="label label-warning">'.$mkt.'</span> / <span class="label label-primary">'.$client.'</span> / <span class="label label-success">'.$act.'</span>';
					}else{
						$column_value = DB::table('users_jakarta')->where('id',$lev_3)->value('name');
					}
				}

				if($column_index==7){
					$user = DB::table('users_jakarta')->where('id',$column_value)->first();
					$level = $user->level;
					$lev_4 = $user->lev_4;
					if($level == '4'){
						$act = DB::table('clients')->where('status','contact')->where('lev_4',$column_value)->count();
						$mkt = DB::table('users_jakarta')->where('lev_4',$column_value)->count();
							$client = DB::table('clients')->where('lev_4',$column_value)->count();
							$column_value = '<span class="label label-warning">'.$mkt.'</span> / <span class="label label-primary">'.$client.'</span> / <span class="label label-success">'.$act.'</span>';
					}else{
						$column_value = DB::table('users_jakarta')->where('id',$lev_4)->value('name');
					}
				}

				if($column_index==8){
					$user = DB::table('users_jakarta')->where('id',$column_value)->first();
					$level = $user->level;
					$lev_5 = $user->lev_5;
					if($level == '5'){
						$act = DB::table('clients')->where('status','contact')->where('lev_5',$column_value)->count();
						$mkt = DB::table('users_jakarta')->where('lev_5',$column_value)->count();
							$client = DB::table('clients')->where('lev_5',$column_value)->count();
							$column_value = '<span class="label label-warning">'.$mkt.'</span> / <span class="label label-primary">'.$client.'</span> / <span class="label label-success">'.$act.'</span>';
					}else{
						$column_value = DB::table('users_jakarta')->where('id',$lev_5)->value('name');
					}
				}

				if($column_index==9){
					$user = DB::table('users_jakarta')->where('id',$column_value)->first();
					$level = $user->level;
					$lev_6 = $user->lev_6;
					if($level == '6'){
						$act = DB::table('clients')->where('status','contact')->where('lev_6',$column_value)->count();
						$mkt = DB::table('users_jakarta')->where('lev_6',$column_value)->count();
							$client = DB::table('clients')->where('lev_6',$column_value)->count();
							$column_value = '<span class="label label-warning">'.$mkt.'</span> / <span class="label label-primary">'.$client.'</span> / <span class="label label-success">'.$act.'</span>';
					}else{
						$column_value = DB::table('users_jakarta')->where('id',$lev_6)->value('name');
					}
				}

				if($column_index==10){
					$user = DB::table('users_jakarta')->where('id',$column_value)->first();
					$level = $user->level;
					$lev_6 = $user->lev_6;
					if($level == '7'){
						$act = DB::table('clients')->where('status','contact')->where('parent',$column_value)->count();
							$client = DB::table('clients')->where('parent',$column_value)->count();
							$column_value = '<span class="label label-primary">'.$client.'</span> / <span class="label label-success">'.$act.'</span>';
					}else{
						$column_value = " ";
					}
				}
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before add data is execute
	    | ----------------------------------------------------------------------
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {
	        //Your code here
					$parent = DB::table('users_jakarta')->where('id',$postdata['parent'])->first();


					$lev_1 = $parent->lev_1;
					$lev_2 = $parent->lev_2;
					$lev_3 = $parent->lev_3;
					$lev_4 = $parent->lev_4;
					$lev_5 = $parent->lev_5;
					$lev_6 = $parent->lev_6;

					$p_level = $parent->level;

					switch($p_level){
						case '1' : $lev_1 = $parent->id;break;
						case '2' : $lev_2 = $parent->id;break;
						case '3' : $lev_3 = $parent->id;break;
						case '4' : $lev_4 = $parent->id;break;
						case '5' : $lev_5 = $parent->id;break;
						case '6' : $lev_6 = $parent->id;break;
					}

					$postdata['lev_1'] = $lev_1;
					$postdata['lev_2'] = $lev_2;
					$postdata['lev_3'] = $lev_3;
					$postdata['lev_4'] = $lev_4;
					$postdata['lev_5'] = $lev_5;
					$postdata['lev_6'] = $lev_6;

					$postdata['id_cms_privileges'] = $postdata['level'] + 2;
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after add public static function called
	    | ----------------------------------------------------------------------
	    | @id = last insert id
	    |
	    */
	    public function hook_after_add($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before update data is execute
	    | ----------------------------------------------------------------------
	    | @postdata = input post data
	    | @id       = current id
	    |
	    */
	    public function hook_before_edit(&$postdata,$id) {
	        //Your code here
					$parent = DB::table('users_jakarta')->where('id',$postdata['parent'])->first();

					$lev_1 = $parent->lev_1;
					$lev_2 = $parent->lev_2;
					$lev_3 = $parent->lev_3;
					$lev_4 = $parent->lev_4;
					$lev_5 = $parent->lev_5;
					$lev_6 = $parent->lev_6;

					$p_level = $parent->level;

					switch($p_level){
						case '1' : $lev_1 = $parent->id;break;
						case '2' : $lev_2 = $parent->id;break;
						case '3' : $lev_3 = $parent->id;break;
						case '4' : $lev_4 = $parent->id;break;
						case '5' : $lev_5 = $parent->id;break;
						case '6' : $lev_6 = $parent->id;break;
					}

					$postdata['lev_1'] = $lev_1;
					$postdata['lev_2'] = $lev_2;
					$postdata['lev_3'] = $lev_3;
					$postdata['lev_4'] = $lev_4;
					$postdata['lev_5'] = $lev_5;
					$postdata['lev_6'] = $lev_6;

					$postdata['id_cms_privileges'] = $postdata['level'] + 2;



	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_edit($id) {

				$teams = DB::table('users_jakarta')->where('parent',$id)->get();
				$self = DB::table('users_jakarta')->where('id',$id)->first();

				foreach ($teams as $team) {
					$nlev_1 = $self->lev_1;
					$nlev_2 = $self->lev_2;
					$nlev_3 = $self->lev_3;
					$nlev_4 = $self->lev_4;
					$nlev_5 = $self->lev_5;
					$nlev_6 = $self->lev_6;

					$p_level = $self->level;

					switch($p_level){
						case '1' : $nlev_1 = $self->id;break;
						case '2' : $nlev_2 = $self->id;break;
						case '3' : $nlev_3 = $self->id;break;
						case '4' : $nlev_4 = $self->id;break;
						case '5' : $nlev_5 = $self->id;break;
						case '6' : $nlev_6 = $self->id;break;
					}

					$update = DB::table('users_jakarta')->where('id',$team->id)->update([
						'lev_1' => $nlev_1,
						'lev_2' => $nlev_2,
						'lev_3' => $nlev_3,
						'lev_4' => $nlev_4,
						'lev_5' => $nlev_5,
						'lev_6' => $nlev_6
					]);

					$teams_2 = DB::table('users_jakarta')->where('parent',$team->id)->get();
					$self_2 = DB::table('users_jakarta')->where('id',$team->id)->first();

					foreach ($teams_2 as $team_2) {
						$n2lev_1 = $self_2->lev_1;
						$n2lev_2 = $self_2->lev_2;
						$n2lev_3 = $self_2->lev_3;
						$n2lev_4 = $self_2->lev_4;
						$n2lev_5 = $self_2->lev_5;
						$n2lev_6 = $self_2->lev_6;

						$p2_level = $self_2->level;

						switch($p2_level){
							case '1' : $n2lev_1 = $self_2->id;break;
							case '2' : $n2lev_2 = $self_2->id;break;
							case '3' : $n2lev_3 = $self_2->id;break;
							case '4' : $n2lev_4 = $self_2->id;break;
							case '5' : $n2lev_5 = $self_2->id;break;
							case '6' : $n2lev_6 = $self_2->id;break;
						}

						$update = DB::table('users_jakarta')->where('id',$team_2->id)->update([
							'lev_1' => $n2lev_1,
							'lev_2' => $n2lev_2,
							'lev_3' => $n2lev_3,
							'lev_4' => $n2lev_4,
							'lev_5' => $n2lev_5,
							'lev_6' => $n2lev_6
						]);

						$teams_3 = DB::table('users_jakarta')->where('parent',$team_2->id)->get();
						$self_3 = DB::table('users_jakarta')->where('id',$team_2->id)->first();
						foreach ($teams_3 as $team_3) {
							$n3lev_1 = $self_3->lev_1;
							$n3lev_2 = $self_3->lev_2;
							$n3lev_3 = $self_3->lev_3;
							$n3lev_4 = $self_3->lev_4;
							$n3lev_5 = $self_3->lev_5;
							$n3lev_6 = $self_3->lev_6;

							$p3_level = $self_3->level;

							switch($p3_level){
								case '1' : $n3lev_1 = $self_3->id;break;
								case '2' : $n3lev_2 = $self_3->id;break;
								case '3' : $n3lev_3 = $self_3->id;break;
								case '4' : $n3lev_4 = $self_3->id;break;
								case '5' : $n3lev_5 = $self_3->id;break;
								case '6' : $n3lev_6 = $self_3->id;break;
							}

							$update = DB::table('users_jakarta')->where('id',$team_3->id)->update([
								'lev_1' => $n3lev_1,
								'lev_2' => $n3lev_2,
								'lev_3' => $n3lev_3,
								'lev_4' => $n3lev_4,
								'lev_5' => $n3lev_5,
								'lev_6' => $n3lev_6
							]);

							$teams_4 = DB::table('users_jakarta')->where('parent',$team_3->id)->get();
							$self_4 = DB::table('users_jakarta')->where('id',$team_3->id)->first();
							foreach ($teams_4 as $team_4) {
								$n4lev_1 = $self_4->lev_1;
								$n4lev_2 = $self_4->lev_2;
								$n4lev_3 = $self_4->lev_3;
								$n4lev_4 = $self_4->lev_4;
								$n4lev_5 = $self_4->lev_5;
								$n4lev_6 = $self_4->lev_6;

								$p4_level = $self_4->level;

								switch($p4_level){
									case '1' : $n4lev_1 = $self_4->id;break;
									case '2' : $n4lev_2 = $self_4->id;break;
									case '3' : $n4lev_3 = $self_4->id;break;
									case '4' : $n4lev_4 = $self_4->id;break;
									case '5' : $n4lev_5 = $self_4->id;break;
									case '6' : $n4lev_6 = $self_4->id;break;
								}

								$update = DB::table('users_jakarta')->where('id',$team_4->id)->update([
									'lev_1' => $n4lev_1,
									'lev_2' => $n4lev_2,
									'lev_3' => $n4lev_3,
									'lev_4' => $n4lev_4,
									'lev_5' => $n4lev_5,
									'lev_6' => $n4lev_6
								]);

								$teams_5 = DB::table('users_jakarta')->where('parent',$team_4->id)->get();
								$self_5 = DB::table('users_jakarta')->where('id',$team_4->id)->first();
								foreach ($teams_5 as $team_5) {
									$n5lev_1 = $self_5->lev_1;
									$n5lev_2 = $self_5->lev_2;
									$n5lev_3 = $self_5->lev_3;
									$n5lev_4 = $self_5->lev_4;
									$n5lev_5 = $self_5->lev_5;
									$n5lev_6 = $self_5->lev_6;

									$p5_level = $self_5->level;

									switch($p5_level){
										case '1' : $n5lev_1 = $self_5->id;break;
										case '2' : $n5lev_2 = $self_5->id;break;
										case '3' : $n5lev_3 = $self_5->id;break;
										case '4' : $n5lev_4 = $self_5->id;break;
										case '5' : $n5lev_5 = $self_5->id;break;
										case '6' : $n5lev_6 = $self_5->id;break;
									}

									$update = DB::table('users_jakarta')->where('id',$team_5->id)->update([
										'lev_1' => $n5lev_1,
										'lev_2' => $n5lev_2,
										'lev_3' => $n5lev_3,
										'lev_4' => $n5lev_4,
										'lev_5' => $n5lev_5,
										'lev_6' => $n5lev_6
									]);
								}
							}
						}
					}
				}
				//dd($self,$parent,$teams);
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :)


	}
