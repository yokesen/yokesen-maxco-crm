<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DB;
use CRUDBooster;
use Image;
use Storage;
use Alert;
use Cookie;
use Validator;
use Mail;

class MailController extends Controller
{

    public function blastemail($id){
      $shares = DB::table('campaign')->where('id','=',$id)->first();

      $content = unserialize($shares->email);

      $image = $shares->media;
      $judul = $content['title'];
      $cta = $content['callToAction'];
      $link = $content['link'];
      $campaign = $id;

      $clients = DB::table('clients')->where('contactEmailValidation','true')->get();
      foreach($clients as $user){

          $sender = DB::table('users_jakarta')->where('id',CRUDBooster::myID())->first();

          $name = $user->name;
          $email = $user->email;

          $insert = DB::table('daftar_kiriman_email')->insert([

            'title' => $judul,
            'content' => $shares->email,
            'cta' => $cta,
            'link' => $link,
            'recepientName' => $name,
            'recepientEmail' => $email,
            'senderName' => "Smartmaxco",
            'senderEmail' => 'info@smartmaxco.co.id',
            'timeCreated'=> date('Y-m-d H:i:s'),
            'tipe_email' => 'campaign_blast',
            'campaign' => $campaign,
            'status' => '99'
          ]);

          $next_date = date('Y-m-d',strtotime($date.'+ 3 weekdays'));
          $save = DB::table('activities')->insert([
            'jenis' => 'send email to client',
            'activity' => 'Agency mengirimkan email blast dengan judul '.$judul,
            'bukti' => '',
            'client_id' => $user->id,
            'user_id' => CRUDBooster::myID(),
            'lev_1' => CRUDBooster::me()->lev_1,
            'lev_2' => CRUDBooster::me()->lev_2,
            'lev_3' => CRUDBooster::me()->lev_3,
            'lev_4' => CRUDBooster::me()->lev_4,
            'lev_5' => CRUDBooster::me()->lev_5,
            'lev_6' => CRUDBooster::me()->lev_6,
            'next_fu' => $next_date,
            'created_at' => now()
          ]);


        }
      
    }

    public function testmail(){
      /**paket email**/
      $time_email = date('d-m-Y H:i');
      $name_email = "Budi Arianto"; //Dinamik
      $email_email = "budi.arianto@yokesen.com"; //dinamaik
      $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
      $beautymail->send('email.testemail', [
        'name' => $name_email,
        'email' => $email_email,
        'time' => $time_email
      ],
        function($message) use ($name_email, $email_email)
      {
          $message
            ->from('noreply@smartmaxco.co.id','Smart Maxco')
              ->to($email_email,$name_email)
                ->subject('Baca Smart maxco app');
      });
      /**sampai sini**/

      dd($email_email,$beautymail,$message);
    }

    public function verifikasi($email){

      $client = new Client();
      $result = $client->get('https://api.mailgun.net/v3/address/validate?address='.$email.'&api_key='.env('MAILGUN_PUBLIC_KEY').'&mailbox_verification=true');
      $res = json_decode($result->getBody());
      $check = $res->mailbox_verification;
      dd($res, $check);
    }

    public function verifikasi_email(){
      //ALTER TABLE `clients` ADD `contactEmailValidation` VARCHAR(50) NOT NULL DEFAULT 'false' AFTER `parentFlag`, ADD `contactWhatsappValidation` VARCHAR(50) NOT NULL DEFAULT 'false' AFTER `contactEmailValidation`;
      //ALTER TABLE `clients` ADD `flagValidationEmail` INT NOT NULL DEFAULT '2' AFTER `contactWhatsappValidation`, ADD `flagValidationWhatsapp` INT NOT NULL DEFAULT '2' AFTER `flagValidationEmail`;
      $listClients = DB::table('clients')->where('flagValidationEmail','2')->orderby('id','asc')->limit(6)->get();
      //dd($listClients);
      foreach ($listClients as $list) {
        $client = new Client();
        $result = $client->get('https://api.mailgun.net/v3/address/validate?address='.$list->email.'&api_key='.env('MAILGUN_PUBLIC_KEY').'&mailbox_verification=true');
        $res = json_decode($result->getBody());
        $check = $res->mailbox_verification;


          $update = DB::table('clients')->where('id',$list->id)->update([
            'contactEmailValidation' => $check,
            'flagValidationEmail' => '1'
          ]);

        echo $list->email." --> ".$check."<br>";
      }

    }

    public function kirim_compose(){
      $lists = DB::table('daftar_kiriman_email')->where('status','2')->limit(60)->get();

      foreach ($lists as $email) {

        $sendername = $email->senderName;
        $senderemail = $email->senderEmail;
        $subject = $email->title;
        $name_email = $email->recepientName;
        $email_email = $email->recepientEmail;

        if($email->tipe_email == "campaign_blast"){
          $shares = DB::table('campaign')->where('id','=',$email->campaign)->first();

          $content = unserialize($shares->email);
          $image = $shares->media;
          $email_id = $email->campaign;

          Mail::send('marketing.email-template', [
            'content' => $content,
            'image' => $image,
            'email_id' => $email_id
          ], function ($message) use ($senderemail,$sendername,$email_email,$name_email,$subject)
          {
            $message->from($senderemail, $sendername);
            $message->to($email_email,$name_email);
            $message->subject($subject);
          });
        }else{
          $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
          $beautymail->send('email.compose', [
            'title' => $subject,
            'content'  => $email->content
          ],
            function($message) use ($name_email, $email_email,$sendername,$senderemail,$subject)
          {
              $message
                ->from($senderemail,$sendername)
                  ->to($email_email,$name_email)
                    ->subject($subject);
          });
        }





        $done = DB::table('daftar_kiriman_email')->where('id',$email->id)->update([
          'status' => '1'
        ]);

      }

    }

    public function daftarkirim(Request $request){
      //dd($request->all());
      $insert = DB::table('daftar_kiriman_email')->insert([

        'title' => $request->title,
        'content' => $request->content,
        'cta' => '',
        'link' => '',
        'recepientName' => $request->recepientName,
        'recepientEmail' => $request->recepientEmail,
        'senderName' => CRUDBooster::myName(),
        'senderEmail' => $request->senderName.'@smartmaxco.co.id',
        'timeCreated'=> date('Y-m-d H:i:s')
      ]);



      $next_date = date('Y-m-d',strtotime($date.'+ 3 weekdays'));
      $save = DB::table('activities')->insert([
        'jenis' => 'send email to client',
        'activity' => CRUDBooster::myName().' mengirimkan email dengan judul '.$request->title,
        'bukti' => '',
        'client_id' => $request->clientid,
        'user_id' => CRUDBooster::myID(),
        'lev_1' => CRUDBooster::me()->lev_1,
        'lev_2' => CRUDBooster::me()->lev_2,
        'lev_3' => CRUDBooster::me()->lev_3,
        'lev_4' => CRUDBooster::me()->lev_4,
        'lev_5' => CRUDBooster::me()->lev_5,
        'lev_6' => CRUDBooster::me()->lev_6,
        'next_fu' => $next_date,
        'created_at' => now()
      ]);

      Alert::error('Email berhasil dikirim','SUCCESS')->persistent('close');
      return redirect()->back();
    }
}
