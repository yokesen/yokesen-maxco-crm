<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DB;
use CRUDBooster;
use Image;
use Storage;
use Alert;
use Cookie;
use Validator;

class whatsappController extends Controller
{
    public function lab(){

    }

    public function send(Request $request,$phone){
      CRUDBooster::insert('daftar_kiriman',[
        'phone' => $phone,
        'content' => $request->message
      ]);

      CRUDBooster::insert('conversations',[
        'handphone' => $phone,
        'message' => "Dalam antrian mengirim pesan ....",
        'fromme' => "1",
        'flag' => "99"
      ]);

      return redirect()->back();
    }

    public function sendImage(Request $request,$phone,$id){
      $image = CRUDBooster::first('pictures',$id);

      CRUDBooster::insert('daftar_kiriman_pic',[
        'phone' => $phone,
        'filename' => $image->url,
        'content' => $request->caption
      ]);

      CRUDBooster::insert('conversations',[
        'handphone' => $phone,
        'message' => "Dalam antrian mengirim gambar ....",
        'fromme' => "1",
        'flag' => "99"
      ]);

      return redirect()->back();
    }

    public function auto_correction(){
      $pull = DB::table('clients')->where('flag','0')->limit(100)->get();

      foreach($pull as $n => $isi){

        $whatsapp = $isi->whatsapp;
        $whatsapp = str_replace('-','',$whatsapp);
        $whatsapp = str_replace(' ','',$whatsapp);
        $whatsapp = str_replace(' ','',$whatsapp);
        $whatsapp = str_replace('.','',$whatsapp);
        $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
        $check_number = str_split($whatsapp);
        $new_number = "62";

        if($check_number[0]=='0'){
          foreach($check_number as $n => $number){
            if($n > 0){
              if($check_number[1]=='8'){
                $new_number .= $number;
              }else{;
                $new_number = '-';
              }
            }
          }
        }else{
          if($check_number[0]=='8'){
            $new_number = "62".$whatsapp;
          }elseif($check_number[0]=='6'){
            $new_number = $whatsapp;
          }elseif($check_number[0]=='+'){
            foreach($check_number as $n => $number){
              if($n > 2){
                  $new_number .= $number;
              }
            }
          }else{

            $new_number = '-';

          }
        }

        $update = DB::table('clients')->where('id',$isi->id)->update([
          'whatsapp' => $new_number,
          'flag' => '1'
        ]);


        echo $new_number." ";
      }
    }

    public function check_whatsapp(){
      $pull = DB::table('clients')->whereNull('whatsapp')->get();
      foreach($pull as $n => $isi){
        if(is_null($isi->whatsapp)){
          echo $isi->id." ".$isi->phone." ".$isi->whatsapp."<br>";

            $update = DB::table('clients')->where('id',$isi->id)->update([
              'whatsapp' => $isi->phone
            ]);


        }
      }
    }

    public function auto_phone(){
      $pull = DB::table('clients')->where('flag','1')->limit(100)->get();

      foreach($pull as $n => $isi){

        $whatsapp = $isi->phone;
        $whatsapp = str_replace('-','',$whatsapp);
        $whatsapp = str_replace(' ','',$whatsapp);
        $whatsapp = str_replace(' ','',$whatsapp);
        $whatsapp = str_replace('.','',$whatsapp);
        $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
        $check_number = str_split($whatsapp);
        $new_number = "62";

        if($check_number[0]=='0'){
          foreach($check_number as $n => $number){
            if($n > 0){
              if($check_number[1]=='8'){
                $new_number .= $number;
              }else{

                $new_number = 'fail';

              }
            }
          }
        }else{
          if($check_number[0]=='8'){
            $new_number = "62".$whatsapp;
          }elseif($check_number[0]=='6'){
            $new_number = $whatsapp;
          }elseif($check_number[0]=='+'){
            foreach($check_number as $n => $number){
              if($n > 2){
                  $new_number .= $number;
              }
            }
          }else{
            $new_number = 'fail';
          }
        }
        if($new_number != 'fail'){


        }else{

        }


          $update = DB::table('clients')->where('id',$isi->id)->update([
            'phone' => $new_number,
            'flag' => '2'
          ]);

        echo $new_number." ";
      }
    }

    function pushMessage(Request $request){

      $client = new Client();

      $data = $request->json()->all();
      $mesages = $data['messages'];
      $return = $mesages;

      $id = $mesages[0]['id'];
      $body = $mesages[0]['body'];
      $type = $mesages[0]['type'];
      $senderName = $mesages[0]['senderName'];
      $fromMe = $mesages[0]['fromMe'];
      $author = $mesages[0]['author'];
      $time = $mesages[0]['time'];
      $chatId = $mesages[0]['chatId'];
      $messageNumber = $mesages[0]['messageNumber'];



      $phone = str_replace('@c.us','',$chatId);

      if($mesages){
        $insert_conversation = DB::table('conversations')->insert([
          'nama' => $senderName,
          'handphone' => $phone,
          'message' => $body,
          'fromme' => $fromMe,
          'flag' => '0',
          'msg_id' => $chatId,
          'msg_num' => $messageNumber
        ]);

      //$anu=  strpos($body,'k');
      //return response($anu,200);
      }
    }


    public function reveal(Request $request, $id){
      $client = CRUDBooster::first('clients',$id);

      $chats = DB::table('conversations')->where('handphone',$client->whatsapp)->orderby('id','desc')->limit(20)->get();
      $chats = $chats->reverse();

      $view = view('marketing.chat-reveal',compact('chats'))->render();
      return response()->json(['html'=>$view]);
    }

    public function terpilih(Request $request, $id,$client){

      $data = DB::connection('dev')->table('products')->where('id', $id)->first();
      $view = view('marketing.image-terpilih',compact('data','client'))->render();
      return response()->json(['html'=>$view]);
    }

    public function loadData(Request $request, $key)
    {
        if ($key) {
            $cari = $request->q;
            $data = DB::table('pictures')->select('id','title', 'url', 'keyword')->where('keyword', 'LIKE', "%$key%")->get();
            //return response()->json($data);

            $view = view('marketing.image-list',compact('data'))->render();
            return response()->json(['html'=>$view]);
        }
    }

    public function uploadFile(Request $request){
      //dd($request);
      if($request->image){
        $image = $request->file('image');
        list($width, $height, $type, $attr) = getimagesize($image);
        $time = time();
        $imgName  = str_replace(" ","",$time."-".$image->getClientOriginalName());

        if($width>$height){
            $image_normal = Image::make($image)->widen(1200);
          }else{
            $image_normal = Image::make($image)->heighten(600);
          }



        $image_thumb = Image::make($image)->fit(200, 200);
        $image_normal = $image_normal->stream('jpg', 60);
        $image_thumb = $image_thumb->stream('jpg', 60);

        Storage::disk('local')->put('uploads/'.$imgName, $image_normal->__toString());
        Storage::disk('local')->put('uploads/thumbnails/'.$imgName, $image_thumb->__toString());
      }

      $imgName = 'uploads/'.$imgName;
      $insert = DB::table('pictures')->insertGetId([
        'title' => 'hapus',
        'url' => $imgName,
        'keyword' => $request->keyword,
        'status' => 'active'
      ]);

      $data = $insert;

      return response()->json($data);
    }

    public function noWhatsapp($email){
      $countryPhoneCode = DB::table('country')->select('phonecode')->get();
      //dd($countryPhoneCode);
      return view('marketing.no-whatsapp',compact('email','countryPhoneCode'));
    }

    public function post_temp(Request $request,$email){

      $rules = array(
        'countryCode' => 'required',
      'whatsapp' => 'required'
    );

    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails())
    {
      Alert::error('Buktikan kamu bukan robot!','ERROR')->persistent('close');

      return redirect()->route('noWhatsapp',$email)->withInput()->withErrors($validator);
    }
    else
    {
      $whatsapp = $request->whatsapp;
      $whatsapp = str_replace('-','',$whatsapp);
      $whatsapp = str_replace(' ','',$whatsapp);
      $whatsapp = str_replace(' ','',$whatsapp);
      $whatsapp = str_replace('.','',$whatsapp);
      $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
      $check_number = str_split($whatsapp);
      $new_number = $request->countryCode.$whatsapp;
      $no_hp = $new_number;
      //dd($no_hp);
      $checkNumber = DB::table('users_jakarta')->where('whatsapp',$no_hp)->count();

      if($checkNumber > 0){
        Alert::error('Silahkan masukkan nomor lain','Nomor Sudah terpakai '.$no_hp)->persistent('close');
        Return redirect()->route('noWhatsapp',$email);
      }else{
        Cookie::queue('watemp', $no_hp, 60 );

        $kode = rand(100000, 999999);

        $user = DB::table('users_jakarta')->where('email',$email)->update([
          'whatsapp' => $no_hp,
          'verifikasi' => $kode
        ]);

        CRUDBooster::insert('daftar_kiriman',[
          'phone' => $no_hp,
          'content' => "Hi, kamu baru saja daftarin nomor Whatsapp ini. Skrg kembali ke halaman CRM kemudian masukin kode ini : $kode"
        ]);
      }


      Alert::success('Barusan DEST mengirimi kamu kode ke whatsapp '.$no_hp.' untuk dimasukin di sini.','Tungguin ya! ')->persistent('close');

      return redirect()->route('verify_whatsapp');
    }


  }

  public function verify_whatsapp(){
    return view('marketing.verify');
  }

  public function verificating_whatsapp(Request $request){
    $code = $request->code1.$request->code2.$request->code3.$request->code4.$request->code5.$request->code6;
    if(Cookie::get('watemp')){

      $user = db::table('users_jakarta')->where('whatsapp',Cookie::get('watemp'))->first();

      if($code == $user->verifikasi){

        $password = "maxco12345";

        $update = db::table('users_jakarta')->where('whatsapp',Cookie::get('watemp'))->update([
          'password' => bcrypt($password)
        ]);

        CRUDBooster::insert('daftar_kiriman',[
          'phone' => Cookie::get('watemp'),
          'content' => "Halo, kamu telah berhasil memverifikasi whatsapp. Sekarang silahkan login dengan data sebagai berikut :

email : $user->email
password : $password
"
        ]);

        return redirect()->route('getLogin');
      }else{
        Alert::error('Silahkan coba lagi','Wrong Code')->persistent('close');
        return redirect()->route('verify_whatsapp');
      }
    }else{
      Alert::error('Silahkan coba lagi','Code Expired')->persistent('close');
      return redirect()->route('noWhatsapp',$user->email);
    }
  }
}
