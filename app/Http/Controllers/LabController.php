<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;
use Cookie;
use DB;
use crudbooster;
use Carbon;
use Storage;

class LabController extends Controller
{
  public function uploadImage()
  {
    return view('lab.uploadImage');
  }

  public function laporan()
  {
    $get = db::table('clients')->where('status', 'lose')->get();
    foreach ($get as $isi) {

      $flag = db::table('activities')->where('client_id', $isi->id)->where('jenis', 'update status')->count();

      if ($flag == '0') {
        $date = $isi->updated_at;
        $next_date = date('Y-m-d', strtotime($date . '+ 3 weekdays'));
        $user = db::table('users_jakarta')->where('id', $isi->parent)->first();
        $save = DB::table('activities')->insert([
          'jenis' => 'update status',
          'activity' => 'update status ke lose',
          'client_id' => $isi->id,
          'user_id' => $user->id,
          'lev_1' => $user->lev_1,
          'lev_2' => $user->lev_2,
          'lev_3' => $user->lev_3,
          'lev_4' => $user->lev_4,
          'lev_5' => $user->lev_5,
          'lev_6' => $user->lev_6,
          'next_fu' => $next_date,
          'created_at' => $isi->updated_at
        ]);
        if ($save) {
          echo $isi->name . " " . $isi->status . " ----- " . $flag . "<br>";
        }
      }
    }
  }

  public function get_chat_follow_up()
  {
    $get = db::table('daftar_kiriman')->where('isDelivered', '1')->distinct()->select('phone')->get();
    //dd($get);
    foreach ($get as $list) {
      $client = db::table('clients')->where('whatsapp', $list->phone)->first();
      $conversation = db::table('daftar_kiriman')->where('phone', $list->phone)->first();
      $activity = db::table('activities')->where('client_id', $client->id)->count();
      //echo $activity."   ----> ".$list->phone." nama ".$client->name." whatsapp ".$client->whatsapp." --> ada ? >>> ".count($client)." >>>> ".$conversation->created_at."<br>";
      /*
        if(count($client)>0 && $activity ==0){
          $date = now();
          $next_date = date('Y-m-d',strtotime($date.'+ 3 weekdays'));

          $save = DB::table('activities')->insert([
            'jenis' => 'update new follow up',
            'activity' => 'contact via whatsapp',
            'client_id' => $client->id,
            'user_id' => $client->parent,
            'lev_1' => $client->lev_1,
            'lev_2' => $client->lev_2,
            'lev_3' => $client->lev_3,
            'lev_4' => $client->lev_4,
            'lev_5' => $client->lev_5,
            'lev_6' => $client->lev_6,
            'next_fu' => $next_date,
            'created_at' => $conversation->created_at
          ]);

          $client = db::table('clients')->where('id', $client->id);


          if($client->status =='lead'){
            $update = $client->update([
              'status' => 'contact',
              'next_fu' => $next_date
            ]);
          }else{
            $update = $client->update([
              'next_fu' => $next_date
            ]);
          }

          if($update){
            echo $activity."   ----> ".$list->phone." nama ".$client->name." whatsapp ".$client->whatsapp." --> ada ? >>> ".count($client)." >>>> ".$conversation->created_at."<br>";

          }
        }
        */
    }
  }

  public function postUploadImage(Request $request)
  {
    Storage::disk('public')->put('/images/shortcut.jpg', $request->image);
    dd($request->all());
  }

  public function getPicture()
  {

    $child1 = DB::table('users_jakarta')->where('users_jakarta.parent', CRUDBooster::myID())->get();

    return view('lab.test_structure', compact('child1'));
  }

  public function getDoublex()
  {
    $get = DB::table('clients_remove')->get();
    foreach ($get as $isi) {
      if ($isi->phone != $isi->whatsapp) {
        echo $isi->id . " " . $isi->phone . " != " . $isi->whatsapp . "<br>";

        $update = DB::table('clients')->where('id', $isi->id)->update([
          'whatsapp' => $isi->phone
        ]);
      } else {
        $activity = db::table('activities')->where('client_id', $isi->id)->count();
        if ($activity > 0) {
          $act = db::table('activities')->where('client_id', $isi->id)->first();
          $one = db::table('clients')->where('whatsapp', $isi->whatsapp)->first();

          echo $isi->parent . " <- last " . $isi->id . " vs " . $one->id . " -> first " . $one->parent . " isi " . $act->activity . " act by " . $act->user_id . "<br>";
          if ($act->user_id == $isi->parent) {
            echo $isi->parent . " WIN <br>";
            $update = DB::table('clients')->where('id', $one->id)->update([
              'phone' => '-',
              'whatsapp' => '-'
            ]);
          } elseif ($act->user_id == $one->parent) {
            echo $one->parent . " FIRST WIN <br>";
            $update = DB::table('clients')->where('id', $isi->id)->update([
              'phone' => '-',
              'whatsapp' => '-'
            ]);
          }
        } else {

          $update = DB::table('clients')->where('id', $isi->id)->update([
            'phone' => '-',
            'whatsapp' => '-'
          ]);
        }
      }
    }
  }

  public function getDouble()
  {
    $get = DB::table('clients')->where('id', '<', 17319)->where('id', '>', 17274)->orderby('id', 'asc')->get();
    foreach ($get as $isi) {
      $nohp = str_replace("@noemail.com", "", $isi->email);
      $update = DB::table('clients')->where('id', $isi->id)->update([
        'whatsapp' => $nohp,
        'phone' => $nohp,
        'flag' => '0'
      ]);
      if ($update) {
        echo $nohp . "<br>";
      }
    }
  }

  public function getDoubley()
  {


    $get_100 = DB::table('clients')->where('flag_double', '0')->where('whatsapp', '!=', '-')->orderby('id', 'asc')->get();
    foreach ($get_100 as $isi) {
      $check_wa = DB::table('clients')->where('whatsapp', $isi->whatsapp)->where('flag_double', '0')->count();
      if ($check_wa > 1) {
        $firstWa = DB::table('clients')->where('whatsapp', $isi->whatsapp)->where('flag_double', '0')->orderby('id', 'asc')->first();
        $doubleWa = DB::table('clients')->where('whatsapp', $isi->whatsapp)->where('flag_double', '0')->get();
        foreach ($doubleWa as $double) {
          if ($firstWa->id != $double->id) {
            /*
              $insert = DB::table('clients_remove')->insert([
                'id'=>$double->id,
                'name'=>$double->name,
                'email'=>$double->email,
                'password'=>$double->password,
                'username'=>$double->username,
                'dob'=>$double->dob,
                'address'=>$double->address,
                'kota'=>$double->kota,
                'ktp'=>$double->ktp,
                'upload_ktp'=>$double->upload_ktp,
                'photo'=>$double->photo,
                'phone'=>$double->phone,
                'whatsapp'=>$double->whatsapp,
                'line'=>$double->line,
                'telegram'=>$double->telegram,
                'facebook'=>$double->facebook,
                'google'=>$double->google,
                'twitter'=>$double->twitter,
                'linkedin'=>$double->linkedin,
                'instagram'=>$double->instagram,
                'bigo'=>$double->bigo,
                'parent'=>$double->parent,
                'level'=>$double->level,
                'lev_1'=>$double->lev_1,
                'lev_2'=>$double->lev_2,
                'lev_3'=>$double->lev_3,
                'lev_4'=>$double->lev_4,
                'lev_5'=>$double->lev_5,
                'lev_6'=>$double->lev_6,
                'id_old'=>$double->id_old,
                'id_mkt'=>$double->id_mkt,
                'ref_old'=>$double->ref_old,
                'id_cms_privileges'=>$double->id_cms_privileges,
                'status'=>$double->status,
                'origin'=>$double->origin,
                'campaign'=>$double->campaign,
                'previous_url'=>$double->previous_url,
                'ipaddress'=>$double->ipaddress,
                'desktop'=>$double->desktop,
                'device'=>$double->device,
                'lang'=>$double->lang,
                'flag'=>$double->flag,
                'flag_double'=>$double->flag_double,
                'deleted_at'=>$double->deleted_at,
                'created_at'=>$double->created_at,
                'updated_at'=>$double->updated_at
              ]);

              $update = DB::table('clients')->where('id',$double->id)->update([
                'flag_double' => '7'
              ]);
*/
            $update = DB::table('clients')->where('id', $double->id)->update([
              'phone' => '-',
              'whatsapp' => '-'
            ]);

            //echo $firstWa->id." | ".$firstWa->parent." | ".$firstWa->name." | ".$firstWa->email." | ".$firstWa->whatsapp." || ".$double->whatsapp." | ".$double->email." | ".$double->name." | ".$double->parent." | ".$double->id."<br>";

            if ($update) {
              echo $firstWa->id . " | " . $firstWa->parent . " | " . $firstWa->name . " | " . $firstWa->email . " | " . $firstWa->whatsapp . " || " . $double->whatsapp . " | " . $double->email . " | " . $double->name . " | " . $double->parent . " | " . $double->id . "<br>";
            }
          }
        }
      }

      $update = DB::table('clients')->where('id', $isi->id)->update([
        'flag_double' => '1'
      ]);
    }
  }

  public function LoadDataReminder()
  {
    $data = DB::table('activities')->join('clients', 'activities.client_id', 'clients.id')->whereDate('activities.next_fu', '<', date('Y-m-d'))->where('activities.user_id', CRUDBooster::myID())->get();
    // dd($data);
    return view('lab.reminder', compact('data'));
  }

  public function showdata()
  {
    $data = DB::connection('dev')->table('products')->where('productDescription', 'LIKE', "%recli%")->get();
    dd($data);
  }

  public function loadData(Request $request, $key)
  {
    if ($key) {
      $cari = $request->q;
      $data = DB::connection('dev')->table('products')->where('productDescription', 'LIKE', "%$key%")->get();
      //return response()->json($data);

      $view = view('marketing.image-list', compact('data'))->render();
      return response()->json(['html' => $view]);
    }
  }

  public function dataImage(Request $request, $id)
  {
    $data = DB::table('pictures')->select('id', 'title', 'url', 'keyword')->where('id', $id)->first();
    return response()->json($data);
  }

  public function apiMaxco()
  {
    $api_url = env('API_URL');
    $url = $api_url . 'api/account/listmatchusers';


    $client = new Client();
    $account_list = $client->post($url, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Language' => 'en'
      ],
      'json' => [
        'Username' => "yoke.endarto@gmail.com",
        'Password' => "Richie020908",
        'InputType' => "email"
      ]
    ]);

    $header = $account_list->getHeaders();
    $cookie = $header['Set-Cookie'][1];
    $cookie = explode(";", $cookie);
    $session = trim($cookie[0]);
    $accounts = json_decode($account_list->getBody());
    Session::put('user.shadow', 'shadow');
    Session::put('user.token', $session);
    Session::put('user.id', $accounts->Data->User->UserId);
    Session::put('user.parent', $accounts->Data->User->ParentId);
    Session::put('user.type', $accounts->Data->User->UserType);
    Session::put('user.email', $accounts->Data->User->Email);
    Session::put('user.mobile', $accounts->Data->User->Mobile);
    Session::put('user.gender', $accounts->Data->User->Gender);
    Session::put('user.name', $accounts->Data->IdCard->RealName);

    dd(Session::all());
  }

  public function registeribportal()
  {
    $getduplicatemobile = DB::table('users_jakarta')
      ->select(DB::raw('count(*) as whatsapp_count,whatsapp'))
      ->groupBy('whatsapp')
      ->get();
    $mobiledulicate = [];
    foreach ($getduplicatemobile as $data) {
      if ($data->whatsapp_count > 1) {
        $mobiledulicate[] = $data->whatsapp;
      }
    }
    $dataduplicatewhatsapp = DB::table('users_jakarta')
      ->whereIn('whatsapp', $mobiledulicate)
      ->select('id', 'name', 'email', 'whatsapp')
      ->get();


    $datawithoutduplicatewhatsapp = DB::table('users_jakarta')
      ->where([
        ['id', '!=', 2]
      ])
      ->whereNotIn('whatsapp', $mobiledulicate)
      ->get();

    $dataregister = [];
    foreach ($datawithoutduplicatewhatsapp as $key => $data) {
      $Mobile = !empty($data->phone) ? $data->phone : $data->whatsapp;
      $check_number = str_split($Mobile);
      $new_number="";
      foreach ($check_number as $n => $number) {
        if ($n > 1) {
          $new_number .= $number;
        }
      }
      $Mobile = "0".$new_number;
      // dd($Mobile);
      $dataregister[] = [
        "IBId" => $data->id,
        "FullName" => $data->name,
        "Password" => "maxco12345",
        "ConfirmPassword" => "maxco12345",
        "AreaCode" => "62",
        "Mobile" => $Mobile,
        "Email" => $data->email,
        "IDNO" => !empty($data->ktp) ? $data->ktp : '123456789' . $key,
        "DateOfBirth" => !empty($data->dob) ? $data->dob : "01/01/1990 00:00:00",
        "Gender" => "Male",
        "MaritalStatus" => "Single",
        "AttractClient" => "1",
        "OfferService" => "1",
        "MonthlyClients" => "1",
        "MonthlyVolume" => "1",
        "AverageDeposit" => "1",
        "ApproveStatus" => 0,
        "RebateAmt" => 0
      ];
    }
    // dd('data duplicate :',json_encode($dataduplicatewhatsapp), 'data register:', json_encode($dataregister));
    return response()->json(
      [
        'data-duplicate' => $dataduplicatewhatsapp,
        'dataregister' => $dataregister
      ]
    );
  }

    public function phoneEqualWhatsapp(){
      $users = DB::table('users_jakarta')->get();
      foreach ($users as $user) {
        $update = DB::table('users_jakarta')->where('id',$user->id)->update([
          'phone' => $user->whatsapp
        ]);
      }
    }
}
