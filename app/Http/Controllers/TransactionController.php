<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;

class TransactionController extends Controller
{
    public function list(){

      $distinct = DB::table('pembelian')->distinct()->select('reksadana')->get();
      $daftar = array();
      foreach($distinct as $n => $group){
        $trans = DB::table('pembelian')->join('reksadana','reksadana.kode','pembelian.reksadana')->where('pembelian.reksadana',$group->reksadana)->get();
        foreach($trans as $x => $trx){
          $daftar[$n][$x] = $trx;
        }

      }
      //dd($daftar);
      return view('mega.total_transaksi',compact('daftar'));
    }

    public function mestika(){
      $roiMain = 1046250000;
      $roiSofa = 500000000;
      $roiSocmed = 325000000;
      $roiAds = 1365000000;
      $roiDemand = 900000000;
      $subtotal = $roiMain + $roiSofa + $roiSocmed + $roiAds;
      $total = $roiMain + $roiSofa + $roiSocmed + $roiAds + $roiDemand;

      $smdev = 0.4 * $roiSocmed;
      $smmai = 0.6 * $roiSocmed;
      $mtn_cs = $roiSofa * 0.4;
      $mtn_smtp = $roiSofa * 0.08;
      $mtn_crm = $roiSofa * 0.52;

      $p1 = 41662586;
      $p2 = 546942;
      $p3 = 54694;
      $p4 = 16408;
      $p5 = 5469;
      $p6 = 2735;


            $payments = [
              '1' => [
                'Main Framework' => $roiMain/4,
                'SocMed Development' => $smdev/4,
                'SMTP Mail' => $mtn_smtp/4,
              ],
              '2' => [
                'Main Framework' => $roiMain/4,
                'SocMed Development' => $smdev/4,
                'CS BOT Training' => $mtn_cs/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '3' => [
                'Main Framework' => $roiMain/4,
                'SocMed Maintain' => $smmai/4,
                'CRM Maintenance' => $mtn_crm/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '4' => [
                'Main Framework' => $roiMain/4,
                'SocMed Development' => $smdev/4,
                'SMTP Mail' => $mtn_smtp/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '5' => [

                'CS BOT Training' => $mtn_cs/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '6' => [

                'SocMed Maintain' => $smmai/4,
                'CRM Maintenance' => $mtn_crm/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '7' => [

                'SocMed Development' => $smdev/4,
                'SMTP Mail' => $mtn_smtp/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '8' => [

                'CS BOT Training' => $mtn_cs/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '9' => [

                'SocMed Maintain' => $smmai/4,
                'CRM Maintenance' => $mtn_crm/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '10' => [

                'SMTP Mail' => $mtn_smtp/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '11' => [

                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '12' => [

                'SocMed Maintain' => $smmai/4,
                'CS BOT Training' => $mtn_cs/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '13' => [
                'CRM Maintenance' => $mtn_crm/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ]
            ];

      $barMain = 100; $colMain="success";
      $barAds = 100; $colAds="success";
      $barOdb = 100; $colOdb="success";
      $barSoc = 100; $colSoc="success";

      return view('mestika.calculate', compact('barMain','colMain','barAds','colAds','barOdb','colOdb','barSoc','colSoc','roiMain','roiSofa','roiSocmed','roiAds','roiDemand','subtotal','total','p1','p2','p3','p4','p5','p6','payments'));
    }

    public function hitung(Request $request){
      $sofa = $request->smf;
      $ads = $request->ads;
      $odb = $request->odb;
      $smd = $request->smdev;

      $roiMain = 1046250000;
      $roiSofa = 500000000;
      $roiSocmed = 325000000;
      $roiAds = 1365000000;
      $roiDemand = 900000000;


      $smdev = 0.4 * $roiSocmed;
      $smmai = 0.6 * $roiSocmed;
      $mtn_cs = $roiSofa * 0.4;
      $mtn_smtp = $roiSofa * 0.08;
      $mtn_crm = $roiSofa * 0.52;


      $p1 = 41662586;
      $p2 = 546942;
      $p3 = 54694;
      $p4 = 16408;
      $p5 = 5469;
      $p6 = 2735;

      switch($sofa){
        case '1' : $roiSofa = 500000000; $barMain = 100; $colMain="success";break;
        case '2' : $roiSofa = 304000000; $barMain = 25; $colMain="warning";break;
        case '3' : $roiSofa = 140000000; $barMain = 10; $colMain="danger";break;
      }

      switch($ads){
        case '1' : $ads_kali = 1; $barAds = 100; $colAds="success";break;
        case '2' : $ads_kali = 2; $barAds = 50; $colAds="warning";break;
        case '3' : $ads_kali = 3; $barAds = 33; $colAds="danger";break;
      }

      switch($odb){
        case '1' : $roiDemand = 900000000; $barOdb = 100; $colOdb="success";break;
        case '2' : $roiDemand = 450000000;  $barOdb = 50; $colOdb="warning";break;
        case '3' : $roiDemand = 250000000;  $barOdb = 25; $colOdb="danger";break;
      }

      switch($smd){
        case '1' : $roiSocmed = 325000000;$smdev = 0.4 * $roiSocmed; $smmai = 0.6 * $roiSocmed; $barSoc = 100; $colSoc="success";break;
        case '2' : $roiSocmed = 175000000;$smdev = 0; $smmai = $roiSocmed; $barSoc = 50; $colSoc="warning"; break;
      }

      $roiAds = $roiAds / $ads_kali;
      $p1 = $p1/ $ads_kali;
      $p2 = $p2/ $ads_kali;
      $p3 = $p3/ $ads_kali;
      $p4 = $p4/ $ads_kali;
      $p5 = $p5/ $ads_kali;
      $p6 = $p6/ $ads_kali;

      $subtotal = $roiMain + $roiSofa + $roiSocmed + $roiAds;
      $total = $roiMain + $roiSofa + $roiSocmed + $roiAds + $roiDemand;



            $mtn_cs = $roiSofa * 0.4;
            $mtn_smtp = $roiSofa * 0.08;
            $mtn_crm = $roiSofa * 0.52;

            $payments = [
              '1' => [
                'Main Framework' => $roiMain/4,
                'SocMed Development' => $smdev/4,
                'SMTP Mail' => $mtn_smtp/4,
              ],
              '2' => [
                'Main Framework' => $roiMain/4,
                'SocMed Development' => $smdev/4,
                'CS BOT Training' => $mtn_cs/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '3' => [
                'Main Framework' => $roiMain/4,
                'SocMed Maintain' => $smmai/4,
                'CRM Maintenance' => $mtn_crm/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '4' => [
                'Main Framework' => $roiMain/4,
                'SocMed Development' => $smdev/4,
                'SMTP Mail' => $mtn_smtp/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '5' => [

                'CS BOT Training' => $mtn_cs/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '6' => [

                'SocMed Maintain' => $smmai/4,
                'CRM Maintenance' => $mtn_crm/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '7' => [

                'SocMed Development' => $smdev/4,
                'SMTP Mail' => $mtn_smtp/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '8' => [

                'CS BOT Training' => $mtn_cs/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '9' => [

                'SocMed Maintain' => $smmai/4,
                'CRM Maintenance' => $mtn_crm/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '10' => [

                'SMTP Mail' => $mtn_smtp/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '11' => [

                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '12' => [

                'SocMed Maintain' => $smmai/4,
                'CS BOT Training' => $mtn_cs/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ],
              '13' => [
                'CRM Maintenance' => $mtn_crm/4,
                'Ads' => $roiAds/12,
                'On Demand' => $roiDemand/12
              ]
            ];



      return view('mestika.calculate', compact('barMain','colMain','barAds','colAds','barOdb','colOdb','barSoc','colSoc','smd','sofa','ads','odb','roiMain','roiSofa','roiSocmed','roiAds','roiDemand','subtotal','total','p1','p2','p3','p4','p5','p6','payments'));

    }

    public function hippo(){
      $roiMain = 732375000;
      $roiSofa = 140000000+28000000+182000000;
      $roiSocmed = 300000000;
      $roiAds = 900000000;
      $roiDemand = 900000000;
      $subtotal = $roiMain + $roiSofa + $roiSocmed + $roiAds;
      $total = $roiMain + $roiSofa + $roiSocmed + $roiAds + $roiDemand;
      $smdev = 0.4 * $roiSocmed;
      $smmai = 0.6 * $roiSocmed;
      $mtn_cs = $roiSofa * 0.4;
      $mtn_smtp = $roiSofa * 0.08;
      $mtn_crm = $roiSofa * 0.52;
      $p1 = 66807423;
      $p2 = 702399;
      $p3 = $p2*0.1;
      $p4 = $p2*0.03;
      $p5 = $p2*0.01;
      $p6 = $p5*0.5;

      $payments = [
        '1' => [
          'Main Framework' => $roiMain/12,
          'SocMed Development' => $smdev/4,
          'SMTP Mail' => $mtn_smtp/4,
        ],
        '2' => [
          'Main Framework' => $roiMain/12,
          'SocMed Development' => $smdev/4,
          'CS BOT Training' => $mtn_cs/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '3' => [
          'Main Framework' => $roiMain/12,
          'SocMed Maintain' => $smmai/4,
          'CRM Maintenance' => $mtn_crm/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '4' => [
          'Main Framework' => $roiMain/12,
          'SocMed Development' => $smdev/4,
          'SMTP Mail' => $mtn_smtp/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '5' => [
          'Main Framework' => $roiMain/12,
          'CS BOT Training' => $mtn_cs/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '6' => [
          'Main Framework' => $roiMain/12,
          'SocMed Maintain' => $smmai/4,
          'CRM Maintenance' => $mtn_crm/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '7' => [
          'Main Framework' => $roiMain/12,
          'SocMed Development' => $smdev/4,
          'SMTP Mail' => $mtn_smtp/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '8' => [
          'Main Framework' => $roiMain/12,
          'CS BOT Training' => $mtn_cs/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '9' => [
          'Main Framework' => $roiMain/12,
          'SocMed Maintain' => $smmai/4,
          'CRM Maintenance' => $mtn_crm/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '10' => [
          'Main Framework' => $roiMain/12,
          'SMTP Mail' => $mtn_smtp/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '11' => [
          'Main Framework' => $roiMain/12,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '12' => [
          'Main Framework' => $roiMain/12,
          'SocMed Maintain' => $smmai/4,
          'CS BOT Training' => $mtn_cs/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '13' => [
          'CRM Maintenance' => $mtn_crm/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ]
      ];

     //dd($payments);

      return view('hippo.calculate', compact('roiMain','roiSofa','roiSocmed','roiAds','roiDemand','subtotal','total','p1','p2','p3','p4','p5','p6','payments'));
    }

    public function hip_hitung(Request $request){
      $sofa = $request->smf;
      $ads = $request->ads;
      $odb = $request->odb;
      $smd = $request->smdev;

      $roiMain = 732375000;
      $roiSofa = 140000000+28000000+182000000;
      $roiSocmed = 300000000;
      $roiAds = 900000000;
      $roiDemand = 900000000;

      $p1 = 66807423;
      $p2 = 702399;
      $p3 = $p2*0.1;
      $p4 = $p2*0.03;
      $p5 = $p2*0.01;
      $p6 = $p5*0.5;

      switch($sofa){
        case '1' : $roiSofa = 500000000; break;
        case '2' : $roiSofa = $roiSofa*0.6; break;
        case '3' : $roiSofa = $roiSofa*0.4; break;
      }

      switch($ads){
        case '1' : $ads_kali = 1; break;
        case '2' : $ads_kali = 2; break;
        case '3' : $ads_kali = 3; break;
      }

      switch($ads){
        case '1' : $roiDemand = 900000000; break;
        case '2' : $roiDemand = 450000000; break;
        case '3' : $roiDemand = 250000000; break;
      }

      switch($smd){
        case '1' : $roiSocmed = 300000000;$smdev = 0.4 * $roiSocmed; $smmai = 0.6 * $roiSocmed; break;
        case '2' : $roiSocmed = 180000000;$smdev = 0; $smmai = $roiSocmed; break;
      }

      $roiAds = $roiAds / $ads_kali;
      $p1 = $p1/ $ads_kali;
      $p2 = $p2/ $ads_kali;
      $p3 = $p3/ $ads_kali;
      $p4 = $p4/ $ads_kali;
      $p5 = $p5/ $ads_kali;
      $p6 = $p6/ $ads_kali;

      $subtotal = $roiMain + $roiSofa + $roiSocmed + $roiAds;
      $total = $roiMain + $roiSofa + $roiSocmed + $roiAds + $roiDemand;



      $mtn_cs = $roiSofa * 0.4;
      $mtn_smtp = $roiSofa * 0.08;
      $mtn_crm = $roiSofa * 0.52;

      $payments = [
        '1' => [
          'Main Framework' => $roiMain/12,
          'SocMed Development' => $smdev/4,
          'SMTP Mail' => $mtn_smtp/4,
        ],
        '2' => [
          'Main Framework' => $roiMain/12,
          'SocMed Development' => $smdev/4,
          'CS BOT Training' => $mtn_cs/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '3' => [
          'Main Framework' => $roiMain/12,
          'SocMed Maintain' => $smmai/4,
          'CRM Maintenance' => $mtn_crm/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '4' => [
          'Main Framework' => $roiMain/12,
          'SocMed Development' => $smdev/4,
          'SMTP Mail' => $mtn_smtp/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '5' => [
          'Main Framework' => $roiMain/12,
          'CS BOT Training' => $mtn_cs/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '6' => [
          'Main Framework' => $roiMain/12,
          'SocMed Maintain' => $smmai/4,
          'CRM Maintenance' => $mtn_crm/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '7' => [
          'Main Framework' => $roiMain/12,
          'SocMed Development' => $smdev/4,
          'SMTP Mail' => $mtn_smtp/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '8' => [
          'Main Framework' => $roiMain/12,
          'CS BOT Training' => $mtn_cs/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '9' => [
          'Main Framework' => $roiMain/12,
          'SocMed Maintain' => $smmai/4,
          'CRM Maintenance' => $mtn_crm/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '10' => [
          'Main Framework' => $roiMain/12,
          'SMTP Mail' => $mtn_smtp/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '11' => [
          'Main Framework' => $roiMain/12,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '12' => [
          'Main Framework' => $roiMain/12,
          'SocMed Maintain' => $smmai/4,
          'CS BOT Training' => $mtn_cs/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ],
        '13' => [
          'CRM Maintenance' => $mtn_crm/4,
          'Ads' => $roiAds/12,
          'On Demand' => $roiDemand/12
        ]
      ];

      return view('hippo.calculate', compact('smd','sofa','ads','odb','roiMain','roiSofa','roiSocmed','roiAds','roiDemand','subtotal','total','p1','p2','p3','p4','p5','p6','payments'));

    }
}
