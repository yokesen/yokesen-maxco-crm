<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use App\Laporan;
use Mail;
use Session;
use Maatwebsite\Excel\Facades\Excel;


class ToolsController extends \crocodicstudio\crudbooster\controllers\CBController
{
    public function cbInit()
    {
    }

    public function laporan(){
      $clients = DB::table('clients')->get();
      return view('clients.laporan',compact('clients'));
    }

    public function export(){
    $date = date('d-m-Y');
      $file = 'data customer - '.$date;
      Excel::create($file, function($excel) {

        $excel->sheet('Sheet1', function($sheet) {
          $date = date('Y-m-d',strtotime('-30 days'));
          //dd($date);
          $clients = DB::table('clients')->get();
          $sheet->loadView('clients.laporan',compact('clients'));
        });

      })->store('xls', storage_path('excel/exports'))->export('xls');

      return redirect()->back();
  }

  public function selectedshare(Request $request){
    Session::put('selectedshare', $request->list);
    $selectedshare = Session::get('selectedshare');
    // dd($selectedshare);
  }


    public function share($id,Request $request){
      $getpage = $request->query('page');
      // dd($getpage);
      if($getpage==null){
        Session::forget('selectedshare');
      }
      $share = DB::table('campaign')->where('id',$id)->first();
      $my_id = CRUDBooster::myID();
      $leads = DB::table('clients')->where('parent', $my_id)->where('contactEmailValidation','true')->paginate(10);
      $selectedshare = Session::get('selectedshare');
      if(!isset($selectedshare)){$selectedshare=array();}
      $selShareString = json_encode($selectedshare);
      return view('marketing.share',compact('share','leads','selectedshare','selShareString'));
    }

    public function share2($id){
      $share = DB::table('campaign')->where('id',$id)->first();

      $my_id = CRUDBooster::myID();
      $leads = DB::table('clients')->where('parent', $my_id)->paginate(10);

      return view('marketing.share2',compact('share','leads'));
    }

    public function emailTemplate($email_id){
      $shares = DB::table('campaign')->where('id','=',$email_id)->first();

      $content = unserialize($shares->email);
      $image = $shares->media;
      return view('marketing.email-template', compact('content','image','email_id'));
    }

    public function postComment(Request $request){
      DB::table('laporans')->insert([
        'parent_post' => $request->post_id,
        'content' => '<p>' .$request->comment. '</p>',
        'user_id' => CRUDBooster::myID(),
        'created_at' => now()
      ]);

      return redirect()->back();
    }

    public function sendemail(Request $request){

      $shares = DB::table('campaign')->where('id','=',$request->id)->first();

      $content = unserialize($shares->email);

      $image = $shares->media;
      $judul = $content['title'];
      $cta = $content['callToAction'];
      $link = $content['link'];
      $campaign = $request->id;

      foreach($request->recipients as $email){

        $user = DB::table('clients')->where('email',$email)->first();
        $sender = DB::table('users_jakarta')->where('id',CRUDBooster::myID())->first();

        $name = $user->name;

        $insert = DB::table('daftar_kiriman_email')->insert([

          'title' => $judul,
          'content' => $shares->email,
          'cta' => $cta,
          'link' => $link,
          'recepientName' => $name,
          'recepientEmail' => $email,
          'senderName' => "Smartmaxco",
          'senderEmail' => 'info@smartmaxco.co.id',
          'timeCreated'=> date('Y-m-d H:i:s'),
          'tipe_email' => 'campaign_blast',
          'campaign' => $campaign
        ]);

        $next_date = date('Y-m-d',strtotime($date.'+ 3 weekdays'));
        $save = DB::table('activities')->insert([
          'jenis' => 'send email to client',
          'activity' => $sender->name.' mengirimkan email dengan judul '.$judul,
          'bukti' => '',
          'client_id' => $user->id,
          'user_id' => CRUDBooster::myID(),
          'lev_1' => CRUDBooster::me()->lev_1,
          'lev_2' => CRUDBooster::me()->lev_2,
          'lev_3' => CRUDBooster::me()->lev_3,
          'lev_4' => CRUDBooster::me()->lev_4,
          'lev_5' => CRUDBooster::me()->lev_5,
          'lev_6' => CRUDBooster::me()->lev_6,
          'next_fu' => $next_date,
          'created_at' => now()
        ]);


      }
      Session::forget('selectedshare');
      return redirect()->back();
    }

    public function addpictures(){
      return view('marketing.addpicture');
    }
}
