<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DB;
use CRUDBooster;

class DashboardController extends \crocodicstudio\crudbooster\controllers\CBController
{
    public function cbInit()
    {
    }

    public function index()
    {
      $admin_id = Session::get('admin_id');
      $facebook = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'facebook', 'parent' => CRUDBooster::myID()])->first();
      $twitter = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'twitter', 'parent' => CRUDBooster::myID()])->first();
      $messenger = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'messenger', 'parent' => CRUDBooster::myID()])->first();
      $telegram = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'telegram', 'parent' => CRUDBooster::myID()])->first();
      $wechat = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'wechat', 'parent' => CRUDBooster::myID()])->first();
      $whatsapp = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'whatsapp', 'parent' => CRUDBooster::myID()])->first();
      $linkedin = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'linkedin', 'parent' => CRUDBooster::myID()])->first();
      $line = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'line', 'parent' => CRUDBooster::myID()])->first();
      $email = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'email', 'parent' => CRUDBooster::myID()])->first();
      $direct = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'direct', 'parent' => CRUDBooster::myID()])->first();
      $organics = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'organics', 'parent' => CRUDBooster::myID()])->first();
      $ads = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'ads', 'parent' => CRUDBooster::myID()])->first();

      $registered_all = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['parent' => CRUDBooster::myID()])->first();
      $registered_this_week = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['parent' => CRUDBooster::myID()])->first();

      $registered = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['parent' => CRUDBooster::myID()])->first();
      $contact = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['status' => 'contact', 'parent' => CRUDBooster::myID()])->first();
      $potential = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['status' => 'potential', 'parent' => CRUDBooster::myID()])->first();
      $lose = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['status' => 'lose', 'parent' => CRUDBooster::myID()])->first();
      $win = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['status' => 'win', 'parent' => CRUDBooster::myID()])->first();
      $teammember = DB::table('users_jakarta')->where('parent',$admin_id)->orderBy('created_at', 'desc')->limit(8)->get();
      // dd($teammember);
      $this->cbView('clients.dashboard', compact('facebook', 'twitter', 'messenger', 'telegram', 'wechat', 'whatsapp', 'linkedin', 'line', 'email', 'direct', 'organics', 'ads', 'registered', 'contact', 'potential', 'lose', 'win','teammember'));
    }

    public function clientgraph(Request $request){
      $nWeek = date('W');
      $nYear = date('Y');
      for($i=0;$i<$nWeek;$i++){
        $count_week = DB::table('clients')->where('parent',CRUDBooster::myID())->where(\DB::raw("WEEKOFYEAR(created_at)"), $i)->count();
        $dataWeekly[] = [
          'y'=>strval($nYear)." W".strval($i+1),
          'item1'=>$count_week
        ];
      }
      return $dataWeekly;
    }

    public function visitorreport(Request $request){
      $today = date("Y-m-d");
      $today = date("Y-m-d",strtotime($today . ' +1 day'));
      $beforeday = date("Y-m-d",strtotime($today . ' -4 day'));
      $visitor= DB::table('analytics')
        ->select(DB::raw('cityName,AVG(`latitude`) as Lat ,AVG(`longitude`) as Lng, count(id) as _count '))
        ->where('created_at','>',$beforeday)
        ->where('device','!=','bot')
        ->whereNotNull('cityName')
        ->groupBy('cityName')->get();
      return $visitor;
    }

    public function lab()
    {
      $admin_id = Session::get('admin_id');
      $facebook = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'facebook', 'parent' => CRUDBooster::myID()])->first();
      $twitter = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'twitter', 'parent' => CRUDBooster::myID()])->first();
      $messenger = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'messenger', 'parent' => CRUDBooster::myID()])->first();
      $telegram = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'telegram', 'parent' => CRUDBooster::myID()])->first();
      $wechat = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'wechat', 'parent' => CRUDBooster::myID()])->first();
      $whatsapp = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'whatsapp', 'parent' => CRUDBooster::myID()])->first();
      $linkedin = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'linkedin', 'parent' => CRUDBooster::myID()])->first();
      $line = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'line', 'parent' => CRUDBooster::myID()])->first();
      $email = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'email', 'parent' => CRUDBooster::myID()])->first();
      $direct = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'direct', 'parent' => CRUDBooster::myID()])->first();
      $organics = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'organics', 'parent' => CRUDBooster::myID()])->first();
      $ads = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'ads', 'parent' => CRUDBooster::myID()])->first();

      $registered_all = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['parent' => CRUDBooster::myID()])->first();
      $registered_this_week = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['parent' => CRUDBooster::myID()])->first();

      $registered = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['parent' => CRUDBooster::myID()])->first();
      $contact = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['status' => 'contact', 'parent' => CRUDBooster::myID()])->first();
      $potential = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['status' => 'potential', 'parent' => CRUDBooster::myID()])->first();
      $lose = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['status' => 'lose', 'parent' => CRUDBooster::myID()])->first();
      $win = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['status' => 'win', 'parent' => CRUDBooster::myID()])->first();
      $teammember = DB::table('users_jakarta')->where('parent',$admin_id)->orderBy('created_at', 'desc')->limit(8)->get();

      $nWeek = date('W');
      for($i=0;$i<$nWeek;$i++){
        $count_week = DB::table('clients')->where('parent',CRUDBooster::myID())->where(\DB::raw("WEEKOFYEAR(created_at)"), $i)->count();
        $dataWeekly[] = $count_week;
      }
      $today = date("Y-m-d");
      $today = date("Y-m-d",strtotime($today . ' +1 day'));
      $visitorCount = DB::table('analytics')
      ->where('created_at','<',$today)
      ->where('device','!=','bot')
      ->whereNotNull('cityName')->count();

      $visitorOrganic = DB::table('analytics')
      ->where('created_at','<',$today)
      ->where('so','direct')
      ->where('device','!=','bot')
      ->whereNotNull('cityName')->count();
      $visitOrganicPercent = ($visitorOrganic / $visitorCount) * 100;
      $visitOrganicPercent = ceil($visitOrganicPercent);
      $visitOrganicPercent = number_format($visitOrganicPercent,0);

      $visitorReferal = DB::table('analytics')
      ->where('created_at','<',$today)
      ->where('so','<>','direct')
      ->where('device','!=','bot')
      ->whereNotNull('cityName')->count();

      $visitRefPercent = ($visitorReferal / $visitorCount) * 100;
      $visitRefPercent = ceil($visitRefPercent);
      $visitRefPercent = number_format($visitRefPercent,0);

      $this->cbView('clients.dashboardlab', compact('facebook', 'twitter', 'messenger', 'telegram', 'wechat', 'whatsapp', 'linkedin', 'line', 'email', 'direct', 'organics', 'ads', 'registered', 'contact', 'potential', 'lose', 'win','teammember','dataWeekly','visitorCount','visitOrganicPercent','visitRefPercent'));
    }

    public function geolocation(){
       $pull = DB::table('analytics')->distinct()->select('ipaddress')->where('flagIP','1')->where('device','!=','Bot')->limit(10)->get();
       //dd($pull);
       foreach ($pull as $key => $isi) {
         $count = DB::table('analytics')->where('uuid',$isi->uuid)->where('flagIP','2')->count();
         echo "count ".$count." --> ";
         if($count<1){
           $ipaddress = $isi->ipaddress;

           $client = new Client();
           $resultip = $client->get('http://api.ipinfodb.com/v3/ip-city/?key=16d22af0d63647b4756113a22a8e036835756ad9d974fd4f88afee09dd09eb05&ip='.$ipaddress.'&format=json');
           $resip = json_decode($resultip->getBody());

           $update = DB::table('analytics')->where('ipaddress',$ipaddress)->update([
             'flagIP' => '2',
             'countryName' => $resip->countryName,
             'regionName' => $resip->regionName,
             'cityName' => $resip->cityName,
             'zipCode' => $resip->zipCode,
             'latitude' => $resip->latitude,
             'longitude' => $resip->longitude,
             'timeZone' => $resip->timeZone
           ]);
           sleep('5');
           echo $ipaddress."<br>";
         }else{
           $uuid = DB::table('analytics')->where('uuid',$isi->uuid)->where('flagIP','2')->first();

           $update = DB::table('analytics')->where('uuid',$isi->uuid)->update([
             'flagIP' => '2',
             'countryName' => $uuid->countryName,
             'regionName' => $uuid->regionName,
             'cityName' => $uuid->cityName,
             'zipCode' => $uuid->zipCode,
             'latitude' => $uuid->latitude,
             'longitude' => $uuid->longitude,
             'timeZone' => $uuid->timeZone
           ]);

           echo "ADA<br>";
         }


       }
    }

    public function board(){
      $today = date("Y-m-d");
      $today = date("Y-m-d",strtotime($today . ' +1 day'));
      $visitorCount = DB::table('analytics')
      ->where('created_at','<',$today)
      ->where('device','!=','bot')
      ->whereNotNull('cityName')->count();

      $visitorOrganic = DB::table('analytics')
      ->where('created_at','<',$today)
      ->where('so','direct')
      ->where('device','!=','bot')
      ->whereNotNull('cityName')->count();
      $visitOrganicPercent = ($visitorOrganic / $visitorCount) * 100;
      $visitOrganicPercent = ceil($visitOrganicPercent);
      $visitOrganicPercent = number_format($visitOrganicPercent,0);

      $visitorReferal = DB::table('analytics')
      ->where('created_at','<',$today)
      ->where('so','<>','direct')
      ->where('device','!=','bot')
      ->whereNotNull('cityName')->count();

      $visitRefPercent = ($visitorReferal / $visitorCount) * 100;
      $visitRefPercent = ceil($visitRefPercent);
      $visitRefPercent = number_format($visitRefPercent,0);

      $admin_id = Session::get('admin_id');
      $facebook = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'facebook'])->first();
      $twitter = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'twitter'])->first();
      $messenger = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'messenger'])->first();
      $telegram = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'telegram'])->first();
      $wechat = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'wechat'])->first();
      $whatsapp = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'whatsapp'])->first();
      $linkedin = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'linkedin'])->first();
      $line = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'line'])->first();
      $email = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'email'])->first();
      $direct = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'direct'])->first();
      $organics = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'organics'])->first();
      $ads = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['origin' => 'ads'])->first();
      $registered_all = DB::table('clients')->select(DB::raw('count(*) as count'))->first();
      $registered_this_week = DB::table('clients')->select(DB::raw('count(*) as count'))->first();
      $registered = DB::table('clients')->select(DB::raw('count(*) as count'))->first();
      $contact = DB::table('clients')->select(DB::raw('count(*) as count'))->first();
      $potential = DB::table('clients')->select(DB::raw('count(*) as count'))->first();
      $lose = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['status' => 'lose'])->first();
      $win = DB::table('clients')->select(DB::raw('count(*) as count'))->where(['status' => 'win'])->first();
      $teammember = DB::table('users_jakarta')->where('lev_1',$admin_id)->orderBy('created_at', 'desc')->limit(8)->get();
      // dd($teammember);
      return view('marketing.board-dashboard',compact('countTM','visitorCount','visitOrganicPercent','visitRefPercent','facebook', 'twitter', 'messenger', 'telegram', 'wechat', 'whatsapp', 'linkedin', 'line', 'email', 'direct', 'organics', 'ads', 'registered', 'contact', 'potential', 'lose', 'win','teammember'));
    }
}
