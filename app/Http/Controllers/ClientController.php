<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use App\Client;
use App\Activity;
use Alert;
use Carbon;
use Storage;
use Image;

class ClientController extends \crocodicstudio\crudbooster\controllers\CBController
{
  public function cbInit()
  {
  }

  public function detail($id)
  {
    $client = Client::find($id);
    if($client->parent == CRUDBooster::myID())
    {
      $chats = DB::table('conversations')->where('handphone',$client->whatsapp)->orderby('id','desc')->limit(20)->get();
      $chats = $chats->reverse();
      $activities = Activity::where('client_id', $id)->orderby('id','desc')->get();
      $this->cbView('clients.client-detail', compact('chats','client','activities'));
    }
    else
    {
      $this->cbView('clients.client-forbiden');
    }
  }

  public function labClient($id)
  {
      $client = Client::find($id);

      $chats = DB::table('conversations')->where('handphone',$client->whatsapp)->orderby('id','desc')->limit(20)->get();
      $chats = $chats->reverse();
      $activities = Activity::where('client_id', $id)->orderby('id','desc')->get();

      $brand = DB::connection('dev')->table('brand')->get();

      $this->cbView('clients.client-lab', compact('chats','client','activities'));

  }

  public function detail_other($id){
    $client = Client::find($id);
    $activities = Activity::where('client_id', $id)->orderby('id','desc')->get();
    $chats = DB::table('conversations')->where('handphone',$client->whatsapp)->orderby('id','desc')->get();
    $follows = DB::table('follows')->where([
      ['user_id','=',CRUDBooster::myID()],
      ['client_id','=', $id]
    ])->first();
    $chats = $chats->reverse();
    // dd($follows);
    return view('clients.client-detail-other',compact('client','activities','chats','follows'));
  }


  public function saveactivity(Request $request){
    $date = now();
    $next_date = date('Y-m-d',strtotime($date.'+ 3 weekdays'));

    if(!empty($request->bukti)){

          $image = $request->bukti;
          list($width, $height, $type, $attr) = getimagesize($image);
          $time = time();
          $imgName  = str_replace(" ","",$time."-".$image->getClientOriginalName());
          if($width>$height){
            $image_normal = Image::make($image)->widen(1200, function ($constraint)
            {
              $constraint->upsize();
            });
          }else{
            $image_normal = Image::make($image)->heighten(600, function ($constraint)
            {
              $constraint->upsize();
            });
          }

        //  $arrayName[] = $imgName;

          $image_thumb = Image::make($image)->fit(200, 200);
          $image_normal = $image_normal->stream();
          $image_thumb = $image_thumb->stream();

          Storage::disk('local')->put('uploads/'.$imgName, $image_normal->__toString());
          Storage::disk('local')->put('uploads/thumbnails/'.$imgName, $image_thumb->__toString());

      }

    $save = DB::table('activities')->insert([
      'jenis' => 'update new follow up',
      'activity' => $request->activity,
      'bukti' => $imgName,
      'client_id' => $request->client_id,
      'user_id' => CRUDBooster::myID(),
      'lev_1' => CRUDBooster::me()->lev_1,
      'lev_2' => CRUDBooster::me()->lev_2,
      'lev_3' => CRUDBooster::me()->lev_3,
      'lev_4' => CRUDBooster::me()->lev_4,
      'lev_5' => CRUDBooster::me()->lev_5,
      'lev_6' => CRUDBooster::me()->lev_6,
      'next_fu' => $next_date,
      'created_at' => now()
    ]);

    $client = Client::where('id', $request->client_id);
    $ubah = DB::table('clients')->where('id', $request->client_id)->first();
    //dd($client);
    if($ubah->status !='lead'){
      $update = $client->update([

        'next_fu' => $next_date
      ]);
    }else{
      $update = $client->update([

        'next_fu' => $next_date
      ]);
    }

    if($save && $update){
      $client = $client->first();

      $recipient = [
        CRUDBooster::me()->parent,
        CRUDBooster::me()->lev_1,
        CRUDBooster::myID() != $client->parent ? $client->parent : null
      ];

      CRUDBooster::sendNotification($config=[
        'content' => CRUDBooster::me()->name . ' updated activity for : '.$client->name,
        'to' => config('app.url') . '/client-detail-other/'.$client->id,
        'id_cms_users' => $recipient
      ]);

      $mkt = DB::table('users_jakarta')->where('id',$client->parent)->first();

      CRUDBooster::insert('daftar_kiriman',[
        'phone' => $mkt->whatsapp,
        'content' => CRUDBooster::me()->name . ' updated activity for : '.$client->name.' pesan :
*'.$request->activity.'*
, ayo cepetan klik ke sini : '.url('/').'/crm/client-detail-other/'.$client->id
      ]);
    }

    return redirect()->back();
  }

  public function updateDetail(Request $request, $id)
  {

    $whatsapp = $request->phone;
    $whatsapp = str_replace('-','',$whatsapp);
    $whatsapp = str_replace(' ','',$whatsapp);
    $whatsapp = str_replace(' ','',$whatsapp);
    $whatsapp = str_replace('.','',$whatsapp);
    $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
    $check_number = str_split($whatsapp);
    $new_number = "62";

    if($check_number[0]=='0'){
      foreach($check_number as $n => $number){
        if($n > 0){
          if($check_number[1]=='8'){
            $new_number .= $number;
          }else{
            //Alert::success('Silahkan coba lagi','Nomor Tidak Valid '.$whatsapp);
            //Return redirect()->back();
            //echo "ga bisa dibenerin nomor $isi->id <br>";
            $new_number = '-';

          }
        }
      }
    }else{
      if($check_number[0]=='8'){
        $new_number = "62".$whatsapp;
      }elseif($check_number[0]=='6'){
        $new_number = $whatsapp;
      }elseif($check_number[0]=='+'){
        foreach($check_number as $n => $number){
          if($n > 2){
              $new_number .= $number;
          }
        }
      }else{
        //Alert::success('Silahkan coba lagi','Nomor Tidak Valid '.$whatsapp);
        //Return redirect()->back();
        $new_number = '-';
        //echo "ga bisa dibenerin  $isi->id <br>";

      }
    }
    $new_phone = $new_number;

    $whatsapp = $request->whatsapp;
    $whatsapp = str_replace('-','',$whatsapp);
    $whatsapp = str_replace(' ','',$whatsapp);
    $whatsapp = str_replace(' ','',$whatsapp);
    $whatsapp = str_replace('.','',$whatsapp);
    $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
    $check_number = str_split($whatsapp);
    $new_number = "62";

    if($check_number[0]=='0'){
      foreach($check_number as $n => $number){
        if($n > 0){
          if($check_number[1]=='8'){
            $new_number .= $number;
          }else{
            //Alert::success('Silahkan coba lagi','Nomor Tidak Valid '.$whatsapp);
            //Return redirect()->back();
            //echo "ga bisa dibenerin nomor $isi->id <br>";
            $new_number = '-';

          }
        }
      }
    }else{
      if($check_number[0]=='8'){
        $new_number = "62".$whatsapp;
      }elseif($check_number[0]=='6'){
        $new_number = $whatsapp;
      }elseif($check_number[0]=='+'){
        foreach($check_number as $n => $number){
          if($n > 2){
              $new_number .= $number;
          }
        }
      }else{
        //Alert::success('Silahkan coba lagi','Nomor Tidak Valid '.$whatsapp);
        //Return redirect()->back();
        $new_number = '-';
        //echo "ga bisa dibenerin  $isi->id <br>";

      }
    }

    $new_whatsapp = $new_number;


    //dd($new_phone,$new_whatsapp);

    $checkPhone = DB::table('clients')->Where('phone',$new_phone)->where('id','!=',$id)->count();
    $checkWhatsapp = DB::table('clients')->where('whatsapp',$new_whatsapp)->where('id','!=',$id)->count();
    $checkEmail = DB::table('clients')->Where('email',$request->email)->where('id','!=',$id)->count();

    if($checkPhone>0){
      Alert::error('PHONE sudah terdaftar!', 'EDIT CLIENT GAGAL')->persistent('Close');
      CRUDBooster::redirect('crm/client-detail/' . $id,    'PHONE sudah terdaftar!', 'warning');

    }

    if($checkWhatsapp>0){
      Alert::error('WHATSAPP sudah terdaftar!', 'EDIT CLIENT GAGAL')->persistent('Close');
      CRUDBooster::redirect('crm/client-detail/' . $id,    'WHATSAPP sudah terdaftar!', 'warning');

    }

    if($checkEmail>0){
      Alert::error('EMAIL sudah terdaftar!', 'EDIT CLIENT GAGAL')->persistent('Close');
      CRUDBooster::redirect('crm/client-detail/' . $id,    'EMAIL sudah terdaftar!', 'warning');
    }

    $client = Client::find($id);
    $client->update($request->all());
    $client->save();

    CRUDBooster::redirect('crm/client-detail/' . $id,    'Data berhasil dirubah!', 'success');
  }

  public function dateFollowUp(Request $request,$id)
  {
    $update = Client::where('id', $id)->update([
      'updated_at' => $request->date . " 00:00:00"
    ]);
    return redirect()->back();
  }

  public function assignLead(Request $request,$id){
    //dd($request,$id,$request->assign);
    $client = DB::table('clients')->where('id',$id)->first();
    $oldparent = DB::table('users_jakarta')->where('id',$client->parent)->first();
    $atasan = DB::table('users_jakarta')->where('id',$request->assign)->first();
    $update = DB::table('clients')->where('id',$id)->update([
      'parent' => $atasan->id,
      'lev_1' => $atasan->lev_1,
      'lev_2' => $atasan->lev_2,
      'lev_3' => $atasan->lev_3,
      'lev_4' => $atasan->lev_4,
      'lev_5' => $atasan->lev_5,
      'lev_6' => $atasan->lev_6,
      'ref_old' => $client->parent
    ]);

    $date = now();
    $next_date = date('Y-m-d',strtotime($date.'+ 3 weekdays'));

    $save = DB::table('activities')->insertGetId([
      'jenis' => 'Assign leads',

      'activity' => '<a href="'.env('APP_URL').'/mkt-detail/'.CRUDBooster::myID().'" target="_blank">'.CRUDBooster::myName().'</a> assign this lead from <a href="'.env('APP_URL').'/mkt-detail/'.$oldparent->id.'" target="_blank">'.$oldparent->name.'</a> to <a href="'.env('APP_URL').'/mkt-detail/'.$atasan->id.'" target="_blank">'.$atasan->name.'</a>',
      'client_id' => $id,
      'user_id' => CRUDBooster::myID(),
      'lev_1' => $atasan->lev_1,
      'lev_2' => $atasan->lev_2,
      'lev_3' => $atasan->lev_3,
      'lev_4' => $atasan->lev_4,
      'lev_5' => $atasan->lev_5,
      'lev_6' => $atasan->lev_6,
      'next_fu' => $next_date,
      'created_at' => now()
    ]);

    return redirect()->route('ClientDetailOther',$id);
  }

  public function statusFollowUp(Request $request,$id){
    $update = Client::where('id', $id)->update([
      'status' => $request->status
    ]);

    $date = now();
    $next_date = date('Y-m-d',strtotime($date.'+ 3 weekdays'));

    $save = DB::table('activities')->insertGetId([
      'jenis' => 'update status',
      'activity' => 'update status ke '.$request->status,
      'client_id' => $id,
      'user_id' => CRUDBooster::myID(),
      'lev_1' => CRUDBooster::me()->lev_1,
      'lev_2' => CRUDBooster::me()->lev_2,
      'lev_3' => CRUDBooster::me()->lev_3,
      'lev_4' => CRUDBooster::me()->lev_4,
      'lev_5' => CRUDBooster::me()->lev_5,
      'lev_6' => CRUDBooster::me()->lev_6,
      'next_fu' => $next_date,
      'created_at' => now()
    ]);

    if ($request->status == "lose") {

      $count_lose_log = DB::table('lose_log')->where('client_id',$id)->count();

      if ($count_lose_log > 1) {
        $update = Client::where('id', $id)->update([
          'status' => "trash"
        ]);
      }

      $lose_log = DB::table('lose_log')->insert([
        'client_id' => $id,
        'user_id' => CRUDBooster::myID(),
        'activity_id' => $save
      ]);

      $update_owner = DB::table('clients')->where('id',$id)->update([

        'ref_old' => CRUDBooster::myID()
      ]);
    }

    return redirect()->back();
  }

  public function claim($id){
    $client = DB::table('clients')->where('id',$id)->first();
    $oldparent = DB::table('users_jakarta')->where('id',$client->parent)->first();
    $atasan = DB::table('users_jakarta')->where('id',CRUDBooster::myID())->first();
    $update = DB::table('clients')->where('id',$id)->update([
      'parent' => $atasan->id,
      'lev_1' => $atasan->lev_1,
      'lev_2' => $atasan->lev_2,
      'lev_3' => $atasan->lev_3,
      'lev_4' => $atasan->lev_4,
      'lev_5' => $atasan->lev_5,
      'lev_6' => $atasan->lev_6,
      'status' => 'lead'
    ]);

    $date = now();
    $next_date = date('Y-m-d',strtotime($date.'+ 3 weekdays'));

    $save = DB::table('activities')->insertGetId([
      'jenis' => 'collect leads',
      'activity' => 'collect from <a href="'.env('APP_URL').'/mkt-detail/'.$oldparent->id.'" target="_blank">'.$oldparent->name.'</a> '.$client->status,
      'client_id' => $id,
      'user_id' => CRUDBooster::myID(),
      'lev_1' => CRUDBooster::me()->lev_1,
      'lev_2' => CRUDBooster::me()->lev_2,
      'lev_3' => CRUDBooster::me()->lev_3,
      'lev_4' => CRUDBooster::me()->lev_4,
      'lev_5' => CRUDBooster::me()->lev_5,
      'lev_6' => CRUDBooster::me()->lev_6,
      'next_fu' => $next_date,
      'created_at' => now()
    ]);

    $save = DB::table('activities')->insertGetId([
      'jenis' => 'lead ownership',
      'activity' => 'Lead ownership change from <a href="'.env('APP_URL').'/mkt-detail/'.$oldparent->id.'" target="_blank">'.$oldparent->name.'</a> to <a href="'.env('APP_URL').'/mkt-detail/'.$atasan->id.'" target="_blank">'.$atasan->name.'</a>',
      'client_id' => $id,
      'user_id' => CRUDBooster::myID(),
      'lev_1' => CRUDBooster::me()->lev_1,
      'lev_2' => CRUDBooster::me()->lev_2,
      'lev_3' => CRUDBooster::me()->lev_3,
      'lev_4' => CRUDBooster::me()->lev_4,
      'lev_5' => CRUDBooster::me()->lev_5,
      'lev_6' => CRUDBooster::me()->lev_6,
      'next_fu' => $next_date,
      'created_at' => now()
    ]);
    return redirect()->route('ClientDetail',$id);
  }

  public function addClient(){
    return view('clients.add-new-client');
  }

  public function postClient(Request $request){

    $whatsapp = $request->phone;
    $whatsapp = str_replace('-','',$whatsapp);
    $whatsapp = str_replace(' ','',$whatsapp);
    $whatsapp = str_replace(' ','',$whatsapp);
    $whatsapp = str_replace('.','',$whatsapp);
    $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
    $check_number = str_split($whatsapp);
    $new_number = "62";

    if($check_number[0]=='0'){
      foreach($check_number as $n => $number){
        if($n > 0){
          if($check_number[1]=='8'){
            $new_number .= $number;
          }else{
            $new_number = '0';

          }
        }
      }
    }else{
      if($check_number[0]=='8'){
        $new_number = "62".$whatsapp;
      }elseif($check_number[0]=='6'){
        $new_number = $whatsapp;
      }elseif($check_number[0]=='+'){
        foreach($check_number as $n => $number){
          if($n > 2){
              $new_number .= $number;
          }
        }
      }else{
        $new_number = '0';
      }
    }

    $checkEmail = DB::table('clients')->where('email',$request->email)->count();
    $checkWhatsapp = DB::table('clients')->where('whatsapp',$new_number)->count();
    $checkPhone = DB::table('clients')->where('phone',$new_number)->count();


    $checkPhone = DB::table('clients')->Where('phone',$new_number)->where('id','!=',$id)->count();
    $checkWhatsapp = DB::table('clients')->where('whatsapp',$new_number)->where('id','!=',$id)->count();
    $checkEmail = DB::table('clients')->Where('email',$request->email)->where('id','!=',$id)->count();

    if($checkPhone>0){

      $let = DB::table('clients')->Where('phone',$new_number)->first();
      $pemilik = DB::table('users_jakarta')->where('id',$let->parent)->first();

      Alert::error('PHONE sudah terdaftar! atas nama '.$pemilik->name, 'ADD CLIENT GAGAL')->persistent('Close');
      CRUDBooster::redirect('crm/client-detail-other/'.$let->id, 'PHONE sudah terdaftar! atas nama '.$pemilik->name, 'warning');

    }

    if($checkWhatsapp>0){

      $let = DB::table('clients')->Where('whataspp',$new_number)->first();
      $pemilik = DB::table('users_jakarta')->where('id',$let->parent)->first();

      Alert::error('WHATSAPP sudah terdaftar!', 'EDIT CLIENT GAGAL')->persistent('Close');
      CRUDBooster::redirect('crm/client-detail-other/'.$let->id, 'PHONE sudah terdaftar! atas nama '.$pemilik->name, 'warning');

    }

    if($checkEmail>0){

      $let = DB::table('clients')->Where('email',$request->email)->first();
      $pemilik = DB::table('users_jakarta')->where('id',$let->parent)->first();

      Alert::error('EMAIL sudah terdaftar!', 'EDIT CLIENT GAGAL')->persistent('Close');
      CRUDBooster::redirect('crm/client-detail-other/'.$let->id, 'PHONE sudah terdaftar! atas nama '.$pemilik->name, 'warning');

    }

    $user = DB::table('users_jakarta')->where('id',CRUDBooster::myID())->first();

    $postdata['lev_1'] = $user->lev_1;
    $postdata['lev_2'] = $user->lev_2;
    $postdata['lev_3'] = $user->lev_3;
    $postdata['lev_4'] = $user->lev_4;
    $postdata['lev_5'] = $user->lev_5;
    $postdata['lev_6'] = $user->lev_6;

    $insert = DB::table('clients')->insert([
      'name' => $request->name,
      'email' => $request->email,
      'whatsapp' => $new_number,
      'phone' => $new_number,
      'parent' => CRUDBooster::myID(),
      'lev_1' => $user->lev_1,
      'lev_2' => $user->lev_2,
      'lev_3' => $user->lev_3,
      'lev_4' => $user->lev_4,
      'lev_5' => $user->lev_5,
      'lev_6' => $user->lev_6,
      'status' => 'lead',
      'origin' => $request->origin
    ]);

    CRUDBooster::redirect('crm/clients_po_from_level_7', 'Data '.$request->name.' berhasil disimpan', 'success');

    //dd($request->all());
  }

  public function postFollow(Request $request){

      // dd($request);
      $getData= DB::table('follows')->where([
        ['user_id','=',CRUDBooster::myID()],
        ['client_id','=', $request->client_id]
        ])->first();

      if(isset($getData)){
        DB::table('follows')
            ->where([
              ['user_id','=',CRUDBooster::myID()],
              ['client_id','=', $request->client_id]
              ])
            ->update([
              'status' => $request->status,
              'updated_at'=>now()
            ]);

      }else{
        $insert = DB::table('follows')->insert([
          'client_id'=> $request->client_id,
          'user_id' => CRUDBooster::myID(),
          'status' => $request->status,
          'created_at' => now()
        ]);
      }


      return redirect()->back();

    }

    public function auto_parent_name(){
      //ALTER TABLE 'clients' ADD 'parentName' VARCHAR(255) NULL DEFAULT NULL AFTER 'updated_at', ADD 'parentFlag' INT NOT NULL DEFAULT '1' AFTER 'parentName';
      $pull_data = DB::table('clients')->where('parentFlag','1')->get();
      foreach($pull_data as $client){
        $parent = DB::table('users_jakarta')->where('id',$client->parent)->first();
        //dd($client,$parent);

        $update = DB::table('clients')->where('id',$client->id)->update([
          'parentName' => $parent->name,
          'parentFlag' => '2'
        ]);

        if($client->origin == "0"){
          $update = DB::table('clients')->where('id',$client->id)->update([
            'origin' => $client->previous_url
          ]);
				}
      }

    }


}
