<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use TwitterOAuth;

class SocialDataController extends Controller
{
    public function push_social(){
      return view('scrapper.push_social');
    }

    public function view_social_search(){
      return view('scrapper.social');
    }

    public function view_marketplace_search(){
      return view('scrapper.marketplace');
    }

    public function view_twitter_search(){
      return view('scrapper.twitter');
    }

    public function social_search(Request $request){
      //dd($request);

      $client = new Client();
      //dd($client);
      $converter_list = $client->get('https://www.googleapis.com/customsearch/v1?key=AIzaSyBzIXV9EJekIjk15h1w9jewcpuhYm2RMDM&cx=014892679458315289628%3Azuo79llywgk&q='.$request->keyword, [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $converter = json_decode($converter_list->getBody());
      $activities = $converter->items;
      return view('scrapper.social',compact('activities'));

    }

    public function marketplace_search(Request $request){
      $client = new Client();
      //dd($client);
      $converter_list = $client->get('https://www.googleapis.com/customsearch/v1?key=AIzaSyBzIXV9EJekIjk15h1w9jewcpuhYm2RMDM&cx=014892679458315289628%3Aaywdtromxaa&q='.$request->keyword, [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $converter = json_decode($converter_list->getBody());
      //dd($converter);
      $activities = $converter->items;
      return view('scrapper.marketplace',compact('activities'));
    }

    public function twitter_search(Request $request,$q){
      /*
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://crm.yokesen.com/public/nes/satu/tweet_search.php?q=powerbank",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_TIMEOUT => 30000,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          // Set Here Your Requesred Headers
          'Content-Type: application/json',
        ),
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);

      $array = explode('<p>end</p>',$response);
      foreach($array as $n => $isi){
        $activities = $isi[0];
      }*/

      require_once("https://crm.yokesen.com/public/nes/satu/twitteroauth/twitteroauth/twitteroauth.php"); //Path to twitteroauth library

      // These info is specific for your twitter appplication - Replace the "XXXX" with your own values (inside the double quotes) - DO NOT EVER DISTRIBUT THIS INFORMATION
      $consumerkey = "km6eN32hHLNKsACXMkAf4sHu3";
      $consumersecret = "MOe0z1lzXRirAwfHgfbtLUy90yKQb9DYBIPV8OfLhsZAbYUi1T";
      $accesstoken = "76286563-UMqWE9r2xn59N1m1NdEYr8keQ1rHKjeN9IUoejklk";
      $accesstokensecret = "E7Q2dptiCdELSgzpzudt5Xo3zwHLbe0WPI4os49qoiVuE";

      function getConnectionWithAccessToken($cons_key, $cons_secret, $oauth_token, $oauth_token_secret) {
        $connection = new TwitterOAuth($cons_key, $cons_secret, $oauth_token, $oauth_token_secret);
        return $connection;
      }

      $connection = getConnectionWithAccessToken($consumerkey, $consumersecret, $accesstoken, $accesstokensecret);


      $hashtags_text = $_GET['q'];
      $twitterhashtag = $hashtags_text;
      //for more hashtags to be followed just inserted new OR #hashtag

      $count_num=99; // max number of tweets to be returned in each call
      $results_mode='recent'; //get the most recent tweets


      $tweets = $connection->get("https://api.twitter.com/1.1/search/tweets.json?q=$hashtags_text&result_type=recent&geocode=-6.235954500000001,106.6537958,10mi");





      //echo "<center><h2>Tweets fetched!</h2></center><br />";

      $count = 0; //counter to get the new fetched tweets
      foreach ($tweets->statuses as $tweet){
        echo $tweet->created_at."|";
        echo $tweet->user->name."|";
        echo $tweet->text."|";
        echo $tweet->user->screen_name."|";
        echo $tweet->source."|";
        echo $tweet->lang."|";
        echo $tweet->user->location."|";
        echo $tweet->user->followers_count."|";
        foreach($tweet->entities->hashtags as $tag){
          echo $tag."<p>tag</p>";
        }
        echo "<p>end</p>";
      }
    }
}
