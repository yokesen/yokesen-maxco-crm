<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;
use CRUDBooster;


class MegaController extends Controller
{
    public function viewprofile(Request $request,$id){
      $client = DB::table('mega_sid')->where('id',$id)->first();
      $reksadana = DB::table('pembelian')->where('pembelian.client',$id)->join('reksadana','reksadana.kode','pembelian.reksadana')->orderby('pembelian.id','desc')->get();
      return view('mega.view_profile',compact('client','reksadana'));
    }

    public function makeprofiletxt(Request $request,$id){

      $client = DB::table('mega_sid')->where('id',$id)->first();
      $contents = "Type|SA Code|SID|First Name|Middle Name|Last Name|Country of Nationality|ID No.|ID Expiration Date|NPWP No.|NPWP Registration Date|Country of Birth|Place of Birth|Date of Birth|Gender|Educational Background|Mother's Maiden Name|Religion|Occupation|Income Level (IDR)|Marital Status|Spouse's Name|Investor's Risk Profile|Investment Objective|Source of Fund|Asset Owner|KTP Address|KTP City Code|KTP Postal Code|Correspondence Address|Correspondence City Code|Correspondence City Name|Correspondence Postal Code|Country of Correspondence|Domicile Address|Domicile City Code|Domicile City Name|Domicile Postal Code|Country of Domicile|Home Phone|Mobile Phone|Facsimile|Email|Statement Type|FATCA (Status)|TIN / Foreign TIN|TIN / Foreign TIN Issuance Country|REDM Payment Bank BIC Code 1|REDM Payment Bank BI Member Code 1|REDM Payment Bank Name 1|REDM Payment Bank Country 1|REDM Payment Bank Branch 1|REDM Payment A/C CCY 1|REDM Payment A/C No. 1|REDM Payment A/C Name 1|REDM Payment Bank BIC Code 2|REDM Payment Bank BI Member Code 2|REDM Payment Bank Name 2|REDM Payment Bank Country 2|REDM Payment Bank Branch 2|REDM Payment A/C CCY 2|REDM Payment A/C No. 2|REDM Payment A/C Name 2|REDM Payment Bank BIC Code 3|REDM Payment Bank BI Member Code 3|REDM Payment Bank Name 3|REDM Payment Bank Country 3|REDM Payment Bank Branch 3|REDM Payment A/C CCY 3|REDM Payment A/C No. 3|REDM Payment A/C Name 3";

      $contents .= "|".$client->Type;
      $contents .= "|".$client->SA_Code;
      $contents .= "|".$client->SID;
      $contents .= "|".$client->First_Name;
      $contents .= "|".$client->Middle_Name;
      $contents .= "|".$client->Last_Name;
      $contents .= "|".$client->Country_of_Nationality;
      $contents .= "|".$client->ID_No;
      $contents .= "|".$client->ID_Expiration_Date;
      $contents .= "|".$client->NPWP_No;
      $contents .= "|".$client->NPWP_Registration_Date;
      $contents .= "|".$client->Country_of_Birth;
      $contents .= "|".$client->Place_of_Birth;
      $contents .= "|".$client->Date_of_Birth;
      $contents .= "|".$client->Gender;
      $contents .= "|".$client->Educational_Background;
      $contents .= "|".$client->Mother_Maiden_Name;
      $contents .= "|".$client->Religion;
      $contents .= "|".$client->Occupation;
      $contents .= "|".$client->Income_Level;
      $contents .= "|".$client->Marital_Status;
      $contents .= "|".$client->Spouse_Name;
      $contents .= "|".$client->Investor_Risk_Profile;
      $contents .= "|".$client->Investment_Objective;
      $contents .= "|".$client->Source_of_Fund;
      $contents .= "|".$client->Asset_Owner;
      $contents .= "|".$client->KTP_Address;
      $contents .= "|".$client->KTP_City_Code;
      $contents .= "|".$client->KTP_Postal_Code;
      $contents .= "|".$client->Correspondence_Address;
      $contents .= "|".$client->Correspondence_City_Code;
      $contents .= "|".$client->Correspondence_City_Name;
      $contents .= "|".$client->Correspondence_Postal_Code;
      $contents .= "|".$client->Country_of_Correspondence;
      $contents .= "|".$client->Domicile_Address;
      $contents .= "|".$client->Domicile_City_Code;
      $contents .= "|".$client->Domicile_City_Name;
      $contents .= "|".$client->Domicile_Postal_Code;
      $contents .= "|".$client->Country_of_Domicile;
      $contents .= "|".$client->Home_Phone;
      $contents .= "|".$client->Mobile_Phone;
      $contents .= "|".$client->Facsimile;
      $contents .= "|".$client->Email;
      $contents .= "|".$client->Statement_Type;
      $contents .= "|".$client->FATCA_Status;
      $contents .= "|".$client->TIN_Foreign_TIN;
      $contents .= "|".$client->TIN_Foreign_TIN_Issuance_Country;
      $contents .= "|".$client->REDM_Payment_Bank_BIC_Code_1;
      $contents .= "|".$client->REDM_Payment_Bank_BI_Member_Code_1;
      $contents .= "|".$client->REDM_Payment_Bank_Name_1;
      $contents .= "|".$client->REDM_Payment_Bank_Country_1;
      $contents .= "|".$client->REDM_Payment_Bank_Branch_1;
      $contents .= "|".$client->REDM_Payment_AC_CCY_1;
      $contents .= "|".$client->REDM_Payment_AC_No_1;
      $contents .= "|".$client->REDM_Payment_AC_Name_1;
      $contents .= "|".$client->REDM_Payment_Bank_BIC_Code_2;
      $contents .= "|".$client->REDM_Payment_Bank_BI_Member_Code_2;
      $contents .= "|".$client->REDM_Payment_Bank_Name_2;
      $contents .= "|".$client->REDM_Payment_Bank_Country_2;
      $contents .= "|".$client->REDM_Payment_Bank_Branch_2;
      $contents .= "|".$client->REDM_Payment_AC_CCY_2;
      $contents .= "|".$client->REDM_Payment_AC_No_2;
      $contents .= "|".$client->REDM_Payment_AC_Name_2;
      $contents .= "|".$client->REDM_Payment_Bank_BIC_Code_3;
      $contents .= "|".$client->REDM_Payment_Bank_BI_Member_Code_3;
      $contents .= "|".$client->REDM_Payment_Bank_Name_3;
      $contents .= "|".$client->REDM_Payment_Bank_Country_3;
      $contents .= "|".$client->REDM_Payment_Bank_Branch_3;
      $contents .= "|".$client->REDM_Payment_AC_CCY_3;
      $contents .= "|".$client->REDM_Payment_AC_No_3;
      $contents .= "|".$client->REDM_Payment_AC_Name_3;


      $namefile = "sid".$client->id.".txt";


      Storage::put('public/sid/'.$namefile, $contents);

      $update = DB::table('mega_sid')->where('id',$id)->update([
        'flag' => '1'
      ]);

      return redirect()->route('MegaViewProfile',$id);
    }

    public function makeSub(){
      $subs = DB::table('pembelian')->whereDate('tgl_approve',Date('Y-m-d'))->get();
      //dd($subs);

      $content = "Transaction Date|Transaction Type|SA Code|Investor Fund Unit A/C No.|Fund Code|Amount (Nominal)|Amount (Unit)|Amount (All Units)|Fee (Nominal)|Fee (Unit)|Fee (%)|REDM Payment A/C Sequential Code|REDM Payment Bank BIC Code|REDM Payment Bank BI Member Code|REDM Payment Bank A/C No.|Payment Date|Transfer Type|SA Reference No.|";

      foreach($subs as $n => $isi){
          $content .= Date('Ymd').'|1|MGA69|MGA6900006TF1234|EL002EQCASHEKN00|'.$isi->amount.'||||||||||||448|';
      }


      Storage::put('public/sid/subscription.txt', $content);
      //Storage::download('public/sid/subscription.txt');
      //dd('hore');
      return redirect()->back();
    }

    public function update(Request $request,$id){
      //dd($request);
      $update = DB::table('mega_sid')->where('id',$id)->update([
        "Type" => $request->Type,
        "SA_Code" => $request->SA_Code,
        "SID" => $request->SID,
        "First_Name" => $request->First_Name,
        "Middle_Name" => $request->Middle_Name,
        "Last_Name" => $request->Last_Name,
        "Country_of_Nationality" => $request->Country_of_Nationality,
        "ID_No" => $request->ID_No,
        "ID_Expiration_Date" => $request->ID_Expiration_Date,
        "NPWP_No" => $request->NPWP_No,
        "NPWP_Registration_Date" => $request->NPWP_Registration_Date,
        "Country_of_Birth" => $request->Country_of_Birth,
        "Place_of_Birth" => $request->Place_of_Birth,
        "Date_of_Birth" => $request->Date_of_Birth,
        "Gender" => $request->Gender,
        "Educational_Background" => $request->Educational_Background,
        "Mother_Maiden_Name" => $request->Mother_Maiden_Name,
        "Religion" => $request->Religion,
        "Occupation" => $request->Occupation,
        "Income_Level" => $request->Income_Level,
        "Marital_Status" => $request->Marital_Status,
        "Spouse_Name" => $request->Spouse_Name,
        "Investor_Risk_Profile" => $request->Investor_Risk_Profile,
        "Investment_Objective" => $request->Investment_Objective,
        "Source_of_Fund" => $request->Source_of_Fund,
        "Asset_Owner" => $request->Asset_Owner,
        "KTP_Address" => $request->KTP_Address,
        "KTP_City_Code" => $request->KTP_City_Code,
        "KTP_Postal_Code" => $request->KTP_Postal_Code,
        "Correspondence_Address" => $request->Correspondence_Address,
        "Correspondence_City_Code" => $request->Correspondence_City_Code,
        "Correspondence_City_Name" => $request->Correspondence_City_Name,
        "Correspondence_Postal_Code" => $request->Correspondence_Postal_Code,
        "Country_of_Correspondence" => $request->Country_of_Correspondence,
        "Domicile_Address" => $request->Domicile_Address,
        "Domicile_City_Code" => $request->Domicile_City_Code,
        "Domicile_City_Name" => $request->Domicile_City_Name,
        "Domicile_Postal_Code" => $request->Domicile_Postal_Code,
        "Country_of_Domicile" => $request->Country_of_Domicile,
        "Home_Phone" => $request->Home_Phone,
        "Mobile_Phone" => $request->Mobile_Phone,
        "Facsimile" => $request->Facsimile,
        "Email" => $request->Email,
        "Statement_Type" => $request->Statement_Type,
        "FATCA_Status" => $request->FATCA_Status,
        "TIN_Foreign_TIN" => $request->TIN_Foreign_TIN,
        "TIN_Foreign_TIN_Issuance_Country" => $request->TIN_Foreign_TIN_Issuance_Country,
        "REDM_Payment_Bank_BIC_Code_1" => $request->REDM_Payment_Bank_BIC_Code_1,
        "REDM_Payment_Bank_BI_Member_Code_1" => $request->REDM_Payment_Bank_BI_Member_Code_1,
        "REDM_Payment_Bank_Name_1" => $request->REDM_Payment_Bank_Name_1,
        "REDM_Payment_Bank_Country_1" => $request->REDM_Payment_Bank_Country_1,
        "REDM_Payment_Bank_Branch_1" => $request->REDM_Payment_Bank_Branch_1,
        "REDM_Payment_AC_CCY_1" => $request->REDM_Payment_AC_CCY_1,
        "REDM_Payment_AC_No_1" => $request->REDM_Payment_AC_No_1,
        "REDM_Payment_AC_Name_1" => $request->REDM_Payment_AC_Name_1,
        "REDM_Payment_Bank_BIC_Code_2" => $request->REDM_Payment_Bank_BIC_Code_2,
        "REDM_Payment_Bank_BI_Member_Code_2" => $request->REDM_Payment_Bank_BI_Member_Code_2,
        "REDM_Payment_Bank_Name_2" => $request->REDM_Payment_Bank_Name_2,
        "REDM_Payment_Bank_Country_2" => $request->REDM_Payment_Bank_Country_2,
        "REDM_Payment_Bank_Branch_2" => $request->REDM_Payment_Bank_Branch_2,
        "REDM_Payment_AC_CCY_2" => $request->REDM_Payment_AC_CCY_2,
        "REDM_Payment_AC_No_2" => $request->REDM_Payment_AC_No_2,
        "REDM_Payment_AC_Name_2" => $request->REDM_Payment_AC_Name_2,
        "REDM_Payment_Bank_BIC_Code_3" => $request->REDM_Payment_Bank_BIC_Code_3,
        "REDM_Payment_Bank_BI_Member_Code_3" => $request->REDM_Payment_Bank_BI_Member_Code_3,
        "REDM_Payment_Bank_Name_3" => $request->REDM_Payment_Bank_Name_3,
        "REDM_Payment_Bank_Country_3" => $request->REDM_Payment_Bank_Country_3,
        "REDM_Payment_Bank_Branch_3" => $request->REDM_Payment_Bank_Branch_3,
        "REDM_Payment_AC_CCY_3" => $request->REDM_Payment_AC_CCY_3,
        "REDM_Payment_AC_No_3" => $request->REDM_Payment_AC_No_3,
        "REDM_Payment_AC_Name_3" => $request->REDM_Payment_AC_Name_3
      ]);

      return redirect()->back();
    }

    public function subscription(Request $request,$id){
        //dd($request);
      $insert = DB::table('pembelian')->insert([
        'client' => $id,
        'reksadana' => $request->reksadana,
        'amount' => $request->amount,
        'fee' => $request->fee,
        'tgl_pesan' => date('Y-m-d H:i:s'),
        'sales' => crudbooster::myID(),
        'status' => 'new'
      ]);

      //dd($insert);

      return redirect()->route('MegaViewProfile',$id);
    }
}
