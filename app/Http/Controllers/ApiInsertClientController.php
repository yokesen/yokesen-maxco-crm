<?php

namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use Exception;

class ApiInsertClientController extends \crocodicstudio\crudbooster\controllers\ApiController
{

	function __construct()
	{
		$this->table       = "clients";
		$this->permalink   = "insert_client";
		$this->method_type = "post";
	}


	public function hook_before(&$postdata)
	{
		//This method will be execute before run the main process
		$countData = DB::table('clients')
			->where(['email' => $postdata['email']])
			->orWhere('phone', $postdata['phone'])
			->count();
		
		if ($countData > 0) {
			$getData = DB::table('clients')
				->where(['email' => $postdata['email']])
				->orWhere('phone', $postdata['phone'])->first();
				
			$dataset['username'] = !empty($postdata['username']) ? $postdata['username'] : $getData->username;
			$dataset['name'] = !empty($postdata['name']) ? $postdata['name'] : $getData->name;
			$dataset['email'] = !empty($postdata['email']) ? $postdata['email'] : $getData->email;
			$dataset['phone'] = !empty($postdata['phone']) ? $postdata['phone'] : $getData->phone;
			$dataset['ktp'] = !empty($postdata['ktp']) ? $postdata['ktp'] : $getData->ktp;
			$dataset['origin'] = !empty($postdata['origin']) ? $postdata['origin'] : $getData->origin;
			$dataset['campaign'] = !empty($postdata['campaign']) ? $postdata['campaign'] : $getData->campaign;
			$dataset['media_iklan'] = !empty($postdata['media_iklan']) ? $postdata['media_iklan'] : $getData->media_iklan;
			$dataset['previous_url'] = !empty($postdata['previous_url']) ? $postdata['previous_url'] : $getData->previous_url;
			$dataset['ipaddress'] = !empty($postdata['ipaddress']) ? $postdata['ipaddress'] : $getData->ipaddress;
			$dataset['desktop'] = !empty($postdata['desktop']) ? $postdata['desktop'] : $getData->desktop;
			$dataset['device'] = !empty($postdata['device']) ? $postdata['device'] : $getData->device;
			$dataset['lang'] = !empty($postdata['lang']) ? $postdata['lang'] : $getData->lang;
			$dataset['uuid'] = !empty($postdata['uuid']) ? $postdata['uuid'] : $getData->uuid;

			DB::table('clients')
				->where('id', $getData->id)
				->update($dataset);

			$postdata=[];
		}
		
		$user = DB::table('users_jakarta')->where('id',$postdata['parent'])->first();
		
		$postdata['lev_1'] = $user->lev_1;
		$postdata['lev_2'] = $user->lev_2;
		$postdata['lev_3'] = $user->lev_3;
		$postdata['lev_4'] = $user->lev_4;
		$postdata['lev_5'] = $user->lev_5;
		$postdata['lev_6'] = $user->lev_6;
	}

	public function hook_query(&$query)
	{
		//This method is to customize the sql query
	}

	public function hook_after($postdata, &$result)
	{
		//This method will be execute after run the main process
		if(!isset($postdata['email'])){
			DB::table('clients')->where('id', '=', $result['id'])->delete();
		}
	}
}
