<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DB;
use CRUDBooster;
use Session;
use Cookie;
use Redirect;

class DataController extends Controller
{
  public function login(){
    $api_url = env('API_URL');
    $url = $api_url . 'api/account/adminlogin';
    $client = new Client();
    $account_list = $client->post($url, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Language' => 'en'
      ],
      'json' => [
        'Username' => 'agroot',
        'Password' => '123qwe'
      ]
    ]);
    $accounts = json_decode($account_list->getBody());
    if ($accounts->Code === 0) {
      $header = $account_list->getHeaders();
      $cfuid = $header['Set-Cookie'][0];
      $cookie = $header['Set-Cookie'][1];
      $cookie = explode(";", $cookie);
      $session = trim($cookie[0]);
      Cookie::queue('__cfduid', substr($cfuid, 9));
      Cookie::queue('NBCRMWEBAPI.SESSION', substr($session, 20));
      Session::put('user.token', $session);
    }

    return redirect()->back();
  }

  public function dataList(){
    try {
      $api_url = env('API_URL');
      $url = $api_url . 'api/account/listusers';
      $client = new Client();
      $account_detail = $client->post($url, [
        'headers' => [
          'Content-Type' => 'application/json',
          'Language' => 'en',
          'Cookie' => Session::get('user.token')
        ],
        'json' => [
            "IsContainMT" => true,
	          "UserType" => 2,
	          "LoadIdCard" => true
        ]
      ]);
      $accounts = json_decode($account_detail->getBody());
      dd($accounts);
    } catch (\Exception $e) {
      if ($e->getResponse()->getStatusCode() == '401') {
        return redirect()->route('secretLoginDataApi');
      } else if ($e->getResponse()->getStatusCode() == '500') {
      } else {
        dd('error code : ' . $e->getResponse()->getStatusCode());
      }
    }
  }

}
