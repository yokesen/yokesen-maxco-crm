<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use App\Pengumuman;
use Crypt;

class PengumumanController extends \crocodicstudio\crudbooster\controllers\CBController
{
    public function cbInit()
    {
    }

    public function index(){
      $announcements = Pengumuman::where(['status' => 'active'])->where(function($q){
          $q->orWhere('role_lvl_' . CRUDBooster::me()->id_cms_privileges, '1');
      })->orderBy('id','desc')->get();
      $this->cbView('clients.pengumuman', compact('announcements'));
    }

    public function detail($id){
      try{
        $id = Crypt::decrypt($id);
        $announcement = Pengumuman::find($id);
        $this->cbView('clients.pengumuman_detail', compact('announcement'));
      }
      catch(\Illuminate\Contracts\Encryption\DecryptException $e)
      {
        abort(404);
      }
    }
}
