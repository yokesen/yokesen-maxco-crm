<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use App\Pengumuman;

class WelcomeController extends \crocodicstudio\crudbooster\controllers\CBController
{
    public function cbInit()
    {
    }

    public function index(Request $request){

      date_default_timezone_set("Asia/Jakarta");
      $date = date('Y-m-d');
      $announcements = Pengumuman::where(['status' => 'active'])->orderBy('id','desc')->limit(2)->get();
      $list = DB::table('activities')->distinct()->select('client_id')->where('user_id',CRUDBooster::myID())->get();
      //dd($list);
      foreach($list as $isi){
        $client = DB::table('activities')->join('clients','clients.id','activities.client_id')->where('activities.client_id',$isi->client_id)->select('activities.next_fu','activities.client_id','clients.name','clients.status')->first();
        if($client->status != 'lose' && $client->status != 'win'){
          if($client->next_fu < $date){
            $lates[] = $client;
          }elseif($client->next_fu == $date){
            $todays[] = $client;
          }else{
            $upcomings[] = $client;
          }
        }

      }
      
      $teammember = DB::table('users_jakarta')->where('parent',CRUDBooster::myID())->orderBy('created_at', 'desc')->limit(8)->get();
      $countTM = DB::table('users_jakarta')
                ->where('parent',CRUDBooster::myID())
                ->orWhereRaw('lev_1',CRUDBooster::myID())
                ->orWhereRaw('lev_2',CRUDBooster::myID())
                ->orWhereRaw('lev_2',CRUDBooster::myID())
                ->orWhereRaw('lev_3',CRUDBooster::myID())
                ->orWhereRaw('lev_4',CRUDBooster::myID())
                ->orWhereRaw('lev_5',CRUDBooster::myID())
                ->orWhereRaw('lev_6',CRUDBooster::myID())
                ->count();
      $campaigns = DB::table('campaign')->where('active','yes')->orderBy('id','desc')->take(5)->get();
      $banners = DB::table('banners')->where('status','Active')->orderBy('id','desc')->take(5)->get();
      $videos = DB::table('videos')->where('status','active')->orderBy('id','desc')->take(1)->get();

      $this->load_js[] = '../js/clipboard.min.js';
      $this->script_js = "
      $(function() {
        var clipboard = new Clipboard('.btn');
        clipboard.on('success', function(e) {
          swal({
            type: 'success',
            title: 'Text copied!',
            text: 'ctrl+v to paste',
            timer: 2000,
            showConfirmButton: false,
          })
        });
      })
      ";
      $campaigns = DB::table('campaign')->orderby('id','desc')->limit(4)->get();

      $today = date("Y-m-d");
      $today = date("Y-m-d",strtotime($today . ' +1 day'));
      $visitorCount = DB::table('analytics')        
      ->where('created_at','<',$today)
      ->where('device','!=','bot')
      ->whereNotNull('cityName')->count();

    $visitorCount = 1;
      $visitorOrganic = DB::table('analytics')        
      ->where('created_at','<',$today)
      ->where('so','direct')
      ->where('device','!=','bot')
      ->whereNotNull('cityName')->count();
      $visitOrganicPercent = ($visitorOrganic / $visitorCount) * 100;
      $visitOrganicPercent = ceil($visitOrganicPercent);
      $visitOrganicPercent = number_format($visitOrganicPercent,0);

      $visitorReferal = DB::table('analytics')        
      ->where('created_at','<',$today)
      ->where('so','<>','direct')
      ->where('device','!=','bot')
      ->whereNotNull('cityName')->count();

      $visitRefPercent = ($visitorReferal / $visitorCount) * 100;
      $visitRefPercent = ceil($visitRefPercent);
      $visitRefPercent = number_format($visitRefPercent,0);

      $this->cbView('clients.welcome-page', compact('announcements', 'campaigns', 'banners', 'videos','lates','todays','teammember','campaigns','countTM','visitorCount','visitOrganicPercent','visitRefPercent'));
    }
}
