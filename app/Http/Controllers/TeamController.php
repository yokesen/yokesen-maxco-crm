<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use App\Laporan;

class TeamController extends \crocodicstudio\crudbooster\controllers\CBController
{

  public function index(){
    $satus =[
              'user_id' => "",
              'name' => "",
              'parent' => "",
              'level' => "",
              'total' => "",
              'client' => "",
              'fu' => ""
            ];

    $twos[]=[
              'user_id' => "",
              'name' => "",
              'parent' => "",
              'level' => "",
              'total' => "",
              'client' => "",
              'fu' => ""
            ];

    $threes[]=[
              'user_id' => "",
              'name' => "",
              'parent' => "",
              'level' => "",
              'total' => "",
              'client' => "",
              'fu' => ""
            ];

    $fours[]=[
              'user_id' => "",
              'name' => "",
              'parent' => "",
              'level' => "",
              'total' => "",
              'client' => "",
              'fu' => ""
            ];

    $fives[]=[
              'user_id' => "",
              'name' => "",
              'parent' => "",
              'level' => "",
              'total' => "",
              'client' => "",
              'fu' => ""
            ];

    $sixes[]=[
              'user_id' => "",
              'name' => "",
              'parent' => "",
              'level' => "",
              'total' => "",
              'client' => "",
              'fu' => ""
            ];

    $user_id = CRUDBooster::myID();
    $level = DB::table('users_jakarta')->where('id',$user_id)->first();
    $lev = 'lev_'.$level->level;
    if($level->level < 7){
      $satus = DB::table('users_jakarta')->where($lev,$user_id)->get();
      foreach($satus as $satu){
        $n_total = DB::table('clients')->where('parent',$satu->id)->count();
        $n_client = DB::table('clients')->where('parent',$satu->id)->where('status','!=','lose')->count();
        $n_fu = DB::table('clients')->where('parent',$satu->id)->where('status','contact')->count();
        $ones[] = [
          'user_id' => $satu->id,
          'name' => $satu->name,
          'parent' => $satu->parent,
          'level' => $satu->level,
          'total' => $n_total,
          'client' => $n_client,
          'fu' => $n_fu
        ];
        $level = DB::table('users_jakarta')->where('id',$satu->id)->first();
        $lev = 'lev_'.$level->level;
        if($level->level < 7){
          $duas = DB::table('users_jakarta')->where($lev,$satu->id)->get();
          foreach ($duas as $dua) {
            $n_total = DB::table('clients')->where('parent',$dua->id)->count();
            $n_client = DB::table('clients')->where('parent',$dua->id)->where('status','!=','lose')->count();
            $n_fu = DB::table('clients')->where('parent',$dua->id)->where('status','contact')->count();
            $twos[] = [
              'user_id' => $dua->id,
              'name' => $dua->name,
              'parent' => $dua->parent,
              'level' => $dua->level,
              'total' => $n_total,
              'client' => $n_client,
              'fu' => $n_fu
            ];
            $level = DB::table('users_jakarta')->where('id',$dua->id)->first();
            $lev = 'lev_'.$level->level;
            if($level->level < 7){
              $tigas = DB::table('users_jakarta')->where($lev,$dua->id)->get();
              foreach ($tigas as $tiga) {
                $n_fu = DB::table('clients')->where('parent',$tiga->id)->where('status','contact')->count();
                $n_total = DB::table('clients')->where('parent',$tiga->id)->count();
                $n_client = DB::table('clients')->where('parent',$tiga->id)->where('status','!=','lose')->count();
                $threes[] = [
                  'user_id' => $tiga->id,
                  'name' => $tiga->name,
                  'parent' => $tiga->parent,
                  'level' => $tiga->level,
                  'total' => $n_total,
                  'client' => $n_client,
                  'fu' => $n_fu
                ];
                $level = DB::table('users_jakarta')->where('id',$tiga->id)->first();
                $lev = 'lev_'.$level->level;
                if($level->level < 7){
                  $empats= DB::table('users_jakarta')->where($lev,$tiga->id)->get();
                  foreach ($empats as $empat) {
                    $n_total = DB::table('clients')->where('parent',$empat->id)->count();
                    $n_client = DB::table('clients')->where('parent',$empat->id)->where('status','!=','lose')->count();
                    $n_fu = DB::table('clients')->where('parent',$empat->id)->where('status','contact')->count();
                    $fours[] = [
                      'user_id' => $empat->id,
                      'name' => $empat->name,
                      'parent' => $empat->parent,
                      'level' => $empat->level,
                      'total' => $n_total,
                      'client' => $n_client,
                      'fu' => $n_fu
                    ];
                    $level = DB::table('users_jakarta')->where('id',$empat->id)->first();
                    $lev = 'lev_'.$level->level;
                    if($level->level < 7){
                      $limas= DB::table('users_jakarta')->where($lev,$empat->id)->get();
                      foreach ($limas as $lima) {
                        $n_total = DB::table('clients')->where('parent',$lima->id)->count();
                        $n_client = DB::table('clients')->where('parent',$lima->id)->where('status','!=','lose')->count();
                        $n_fu = DB::table('clients')->where('parent',$lima->id)->where('status','contact')->count();
                        $fives[] = [
                          'user_id' => $lima->id,
                          'name' => $lima->name,
                          'parent' => $lima->parent,
                          'level' => $lima->level,
                          'total' => $n_total,
                          'client' => $n_client,
                          'fu' => $n_fu
                        ];
                        $level = DB::table('users_jakarta')->where('id',$lima->id)->first();
                        $lev = 'lev_'.$level->level;
                        if($level->level < 7){
                          $enams= DB::table('users_jakarta')->where($lev,$lima->id)->get();
                          foreach ($enams as $enam) {
                            $n_total = DB::table('clients')->where('parent',$enam->id)->count();
                            $n_client = DB::table('clients')->where('parent',$enam->id)->where('status','!=','lose')->count();
                            $n_fu = DB::table('clients')->where('parent',$enam->id)->where('status','contact')->count();
                            $sixes[] = [
                              'user_id' => $enam->id,
                              'name' => $enam->name,
                              'parent' => $enam->parent,
                              'level' => $enam->level,
                              'total' => $n_total,
                              'client' => $n_client,
                              'fu' => $n_fu
                            ];
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    //dd($satus,$twos,$threes,$fours,$fives,$sixes);
    return view('marketing.team',compact('user_id','ones','twos','threes','fours','fives','sixes'));
  }

  public function myTeam(){
    $child1 = DB::table('users_jakarta')->where('users_jakarta.parent',CRUDBooster::myID())->whereNull('deleted_at')->get();
    $stores = DB::table('stores')->get();
    return view('marketing.my-team',compact('child1','stores'));
  }

  public function myTeamStore($toko){
    //dd($toko);
    $child1 = DB::table('users_jakarta')->where('id',$toko)->whereNull('deleted_at')->get();
    $stores = DB::table('stores')->get();
    return view('marketing.my-team',compact('child1','stores'));
  }

  public function myTeamMonth($timeframe){
    $child1 = DB::table('users_jakarta')->where('users_jakarta.parent',CRUDBooster::myID())->whereNull('deleted_at')->get();
    //dd($timeframe);
    return view('marketing.my-team-month',compact('child1','timeframe'));
  }

  public function myTeamCustom(Request $request){
    $mulai = strtotime($request->date_start);
    $selesai = strtotime($request->date_end);

    $child1 = DB::table('users_jakarta')->where('users_jakarta.parent',CRUDBooster::myID())->whereNull('deleted_at')->get();
    //dd($timeframe);
    return view('marketing.my-team-custom',compact('child1','mulai','selesai'));
  }

  public function reporting(){
    $child1 = DB::table('users_jakarta')->where('users_jakarta.parent',CRUDBooster::myID())->whereNull('deleted_at')->get();

    return view('marketing.my-team',compact('child1'));
  }

  public function marketing($id){

    $user = DB::table('users_jakarta')
            ->join('penamaans','users_jakarta.level','penamaans.level')
            ->where('users_jakarta.id',$id)
            ->first();

    $distictClient = DB::table('activities')->distinct()->select('client_id')->where('user_id',$id)->get();

    foreach($distictClient as $cli){
      $activities = DB::table('activities')->join('users_jakarta','users_jakarta.id','activities.user_id')->select(
        'activities.created_at',
        'activities.user_id',
        'activities.activity',
        'activities.next_fu',
        'users_jakarta.name',
        'users_jakarta.photo'
        )->where('activities.client_id',$cli->client_id)->orderBy('activities.id','desc')->limit(10)->get();

      $firstContact = DB::table('activities')->where('client_id',$cli->client_id)->orderBy('id','asc')->first();
      $activities = $activities->reverse();

      $client = DB::table('clients')->select('name','id','whatsapp')->where('id',$cli->client_id)->first();

      $laporan[] = [
        'client' => $client,
        'sales' => $user,
        'comments' => $activities,
        'firstContact' => $firstContact
      ];
    }
    $nLap = count($laporan);
    if($nLap >0){
      $laporan = array_reverse($laporan);

    }

    /////////////STRUKTUR TEAM/////////////////////////////////////////

    $level_1 = DB::table('users_jakarta')
            ->join('penamaans','users_jakarta.level','penamaans.level')
            ->where('users_jakarta.id',$user->lev_1)
            ->first();

    if($level_1){
      $atasan[] = $level_1;
    }

    $level_2 = DB::table('users_jakarta')
            ->join('penamaans','users_jakarta.level','penamaans.level')
            ->where('users_jakarta.id',$user->lev_2)
            ->first();

    if($level_2){
      $atasan[] = $level_2;
    }

    $level_3 = DB::table('users_jakarta')
            ->join('penamaans','users_jakarta.level','penamaans.level')
            ->where('users_jakarta.id',$user->lev_3)
            ->first();

    if($level_3){
      $atasan[] = $level_3;
    }

    $level_4 = DB::table('users_jakarta')
            ->join('penamaans','users_jakarta.level','penamaans.level')
            ->where('users_jakarta.id',$user->lev_4)
            ->first();

    if($level_4){
      $atasan[] = $level_4;
    }

    $level_5 = DB::table('users_jakarta')
            ->join('penamaans','users_jakarta.level','penamaans.level')
            ->where('users_jakarta.id',$user->lev_5)
            ->first();

    if($level_5){
      $atasan[] = $level_5;
    }

    $level_6 = DB::table('users_jakarta')
            ->join('penamaans','users_jakarta.level','penamaans.level')
            ->where('users_jakarta.id',$user->lev_6)
            ->first();

    if($level_6){
      $atasan[] = $level_6;
    }


    ////////////////////////last LOGIN///////////////////////////

    $log = DB::table('cms_logs')->where('id_cms_users',$id)->orderBy('id','desc')->take(5)->get();

    $lead = DB::table('clients')->where('parent',$id)->count();
    $contact = DB::table('clients')->where('parent',$id)->where('status','!=','lose')->count();
    $potential = DB::table('clients')->where('parent',$id)->where('status','contact')->count();

    $internal = Laporan::where('parent_post', '0')
                ->where('user_id',$id)
                ->orderby('id','desc')
                ->with([
                    'comments',
                    ])->get();




    // if( $user->lev_1 == CRUDBooster::myID() ||
    //     $user->lev_2 == CRUDBooster::myID() ||
    //     $user->lev_3 == CRUDBooster::myID() ||
    //     $user->lev_4 == CRUDBooster::myID() ||
    //     $user->lev_5 == CRUDBooster::myID() ||
    //     $user->lev_6 == CRUDBooster::myID() ||
    //     $user->parent == CRUDBooster::myID() ||
    //     $id == CRUDBooster::myID()
    // ){
      return view('marketing.detail',compact('id','laporan','user','atasan','log','lead','contact','potential','internal'));
    // }else{
    //   return view('marketing.marketing-forbiden');
    // }


  }

  public function initiateChat($id){
    DB::table('laporans')->insert([
      'parent_post' => 0,
      'content' => '<p>Chat started</p>',
      'user_id' => $id,
      'created_at' => now()
    ]);

    return redirect()->back();
  }

  public function postComment(Request $request){
    DB::table('laporans')->insert([
      'parent_post' => $request->post_id,
      'content' => '<p>' .$request->comment. '</p>',
      'user_id' => CRUDBooster::myID(),
      'created_at' => now()
    ]);
    $recipient = [
      CRUDBooster::myID() == $request->poster_id ? null : $request->parent_id,
      CRUDBooster::myID() == $request->poster_id ? CRUDBooster::me()->parent : null,
      CRUDBooster::myID() == $request->poster_id ? CRUDBooster::me()->lev_1 : null,
      $request->poster_id
    ];

    $mkt = DB::table('users_jakarta')->where('id',$request->poster_id)->first();

    if(CRUDBooster::myID() != $request->poster_id){
      CRUDBooster::insert('daftar_kiriman',[
        'phone' => $mkt->whatsapp,
        'content' => CRUDBooster::me()->name . ' mengirimi pesan :
*'.$request->comment.'*
,ayo cepetan klik ke sini : '.url('/').'/crm/mkt-detail/'.$request->poster_id
      ]);
    }

    CRUDBooster::sendNotification($config=[
      'content' => ucwords(CRUDBooster::me()->name) . ' membalas laporan',
      'to' => config('app.url') . '/mkt-detail/' . $request->poster_id,
      'id_cms_users' => $recipient
    ]);

    return redirect()->back();
  }

  public function myActivities(){
    $laporans = DB::table('laporans')->join('users_jakarta','users_jakarta.id','laporans.user_id')->where('laporans.user_id',CRUDBooster::myID())->limit(5)->orderby('laporans.id','desc')->get();
    foreach($laporans as $laporan){
      $first_post = $laporan->parent_post;
      $postup = DB::table('laporans')->join('users_jakarta','users_jakarta.id','laporans.user_id')->where('laporans.id',$first_post)->first();
      $data[] = [
        'laporan' => $laporan,
        'postup' => $postup
      ];
    }

    $prospects = DB::table('activities')->join('clients','clients.id','activities.client_id')->where('activities.user_id',CRUDBooster::myID())->orderby('activities.id','desc')->get();

    //dd($prospects);
    return view('marketing.my-activities',compact('data','prospects'));
  }

  public function inactive(){
    return view('marketing.locked');
  }

    public function share_counter(Request $request,$id,$link){
    //get data
    $users = DB::table('users_jakarta')->where('id', $id)->first();
    $ua = $request->server('HTTP_USER_AGENT');
    $ipaddress = $request->server('REMOTE_ADDR');
    $url = $request->server('REQUEST_URI');
    $host = $request->server('HTTP_HOST');
    $url_all = 'https://'.$host.$url;
    $desc = ($users->name).' telah melakukan share di '.$link.' lewat crm '.$host;
    //dd($users,$ua,$ipaddress,$url_all,$link,$desc);

      $save = DB::table('cms_logs')->insert([
                    'ipaddress' => $ipaddress,
                    'useragent' => $ua,
                    'url' => $url_all,
                    'description' => $desc,
                    'details' => '',
                    'id_cms_users' => $id,
                    'created_at' => now()
                ]);

  }

  public function campaign(){
    $campaigns = DB::table('campaign')->orderby('id','desc')->get();
    return view('marketing.campaign',compact('campaigns'));
  }
}
