<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;

class VideoController extends \crocodicstudio\crudbooster\controllers\CBController
{
  public function cbInit()
  {
  }

  public function index()
  {
    $videos = DB::table('videos')->where('status','Active')->orderby('id','desc')->get();
    $this->cbView('tools.video', compact('videos'));
  }

  public function downloadVideo($file = null)
  {
    if($file)
    {
      header("Content-type: octet/stream");
      header("Content-disposition: attachment; filename=".$file.";");
      header("Content-Length: ".filesize($file));
      readfile($file);
      exit;
    }
    else
    {
      return redirect()->back();
    }
  }
}
