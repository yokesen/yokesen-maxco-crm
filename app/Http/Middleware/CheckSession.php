<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use CRUDBooster;


class CheckSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if(!CRUDBooster::myId()) {
        Session::flush();
        return redirect()->route('getLogin');
      }else{
        return $next($request);
      }

    }
}
