<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'user_id',
    'created_at'
  ];


  public function komentator()
  {
    return $this->hasOne('App\Users_cabang', 'id', 'user_id');
  }
}
