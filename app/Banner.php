<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

  public $timestamps = true;

  public function induk()
  {
    return $this->hasOne('App\Users_cabang', 'id', 'parent');
  }
}
