<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengumuman extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $table = 'pengumumans';

  protected $fillable = [
    'user_id',
    'created_at'
  ];


  public function poster()
  {
    return $this->hasOne('App\Users_cabang', 'id', 'post_by');
  }
}
