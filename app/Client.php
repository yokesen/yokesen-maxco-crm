<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'dob',
    'address',
    'kota',
    'ktp',
    'upload_ktp',
    'photo',
    'phone',
    'whatsapp',
    'line',
    'telegram',
    'facebook',
    'google',
    'twitter',
    'linkedin',
    'instagram',
    'bigo',
    'status',
  ];

  public $timestamps = true;

  public function induk()
  {
    return $this->hasOne('App\Users_cabang', 'id', 'parent');
  }
}
