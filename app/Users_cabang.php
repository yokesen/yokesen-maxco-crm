<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_cabang extends Model
{
  protected $table;

  public function __construct()
  {
    $this->table = config('crudbooster.USER_TABLE');
  }

  public function poster() {
    return $this->belongsTo('App\Laporan', 'user_id');
  }
}
