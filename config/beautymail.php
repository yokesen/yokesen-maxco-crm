<?php

return [

    // These CSS rules will be applied after the regular template CSS

    /*
        'css' => [
            '.button-content .button { background: red }',
        ],
    */

    'colors' => [

        'highlight' => '#d62027',
        'button'    => '#1e4996',

    ],

    'view' => [
        'senderName'  => "Smart Maxco",
        'reminder'    => "You are receiving this email beacuse you subscribe or register on https://smartmaxco.co.id",
        'unsubscribe' => "Please contact us to un-subscribe",
        'address'     => "Panin Bank Centre - Ground Floor, Jl. Jend. Sudirman Kav-1 Senayan Jakarta Selatan 10270, DKI Jakarta, Indonesia",

        'logo'        => [
            'path'   => env('ABS_PATH').'/images/logo_300.png',
            'width'  => '300',
            'height' => '66',
        ],


        'facebook' => 'smartmaxco'

    ],

];
