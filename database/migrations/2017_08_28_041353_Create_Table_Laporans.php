<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLaporans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->text('content');
            $table->integer('parent_post');
            $table->integer('lev_1')->nullable();
            $table->integer('lev_2')->nullable();
            $table->integer('lev_3')->nullable();
            $table->integer('lev_4')->nullable();
            $table->integer('lev_5')->nullable();
            $table->integer('lev_6')->nullable();
            $table->integer('status_1')->nullable();
            $table->integer('status_2')->nullable();
            $table->integer('status_3')->nullable();
            $table->integer('status_4')->nullable();
            $table->integer('status_5')->nullable();
            $table->integer('status_6')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporans');
    }
}
