<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreColumnToAllTableUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('users_jakarta', function (Blueprint $table) {
        $table->string('status')->nullable();
        $table->string('origin')->nullable();
        $table->string('campaign')->nullable();
        $table->string('ipaddress')->nullable();
        $table->string('desktop')->nullable();
        $table->string('device')->nullable();
        $table->string('lang')->nullable();
      });

      Schema::table('users_surabaya', function (Blueprint $table) {
        $table->string('status')->nullable();
        $table->string('origin')->nullable();
        $table->string('campaign')->nullable();
        $table->string('ipaddress')->nullable();
        $table->string('desktop')->nullable();
        $table->string('device')->nullable();
        $table->string('lang')->nullable();
      });

      Schema::table('users_bandung', function (Blueprint $table) {
        $table->string('status')->nullable();
        $table->string('origin')->nullable();
        $table->string('campaign')->nullable();
        $table->string('ipaddress')->nullable();
        $table->string('desktop')->nullable();
        $table->string('device')->nullable();
        $table->string('lang')->nullable();
      });

      Schema::table('users_medan', function (Blueprint $table) {
        $table->string('status')->nullable();
        $table->string('origin')->nullable();
        $table->string('campaign')->nullable();
        $table->string('ipaddress')->nullable();
        $table->string('desktop')->nullable();
        $table->string('device')->nullable();
        $table->string('lang')->nullable();
      });

      Schema::table('users_jogja', function (Blueprint $table) {
        $table->string('status')->nullable();
        $table->string('origin')->nullable();
        $table->string('campaign')->nullable();
        $table->string('ipaddress')->nullable();
        $table->string('desktop')->nullable();
        $table->string('device')->nullable();
        $table->string('lang')->nullable();
      });

      Schema::table('users_purwokerto', function (Blueprint $table) {
        $table->string('status')->nullable();
        $table->string('origin')->nullable();
        $table->string('campaign')->nullable();
        $table->string('ipaddress')->nullable();
        $table->string('desktop')->nullable();
        $table->string('device')->nullable();
        $table->string('lang')->nullable();
      });

      Schema::table('users_malang', function (Blueprint $table) {
        $table->string('status')->nullable();
        $table->string('origin')->nullable();
        $table->string('campaign')->nullable();
        $table->string('ipaddress')->nullable();
        $table->string('desktop')->nullable();
        $table->string('device')->nullable();
        $table->string('lang')->nullable();
      });

      Schema::table('users_banjarmasin', function (Blueprint $table) {
        $table->string('status')->nullable();
        $table->string('origin')->nullable();
        $table->string('campaign')->nullable();
        $table->string('ipaddress')->nullable();
        $table->string('desktop')->nullable();
        $table->string('device')->nullable();
        $table->string('lang')->nullable();
      });

      Schema::table('users_bali', function (Blueprint $table) {
        $table->string('status')->nullable();
        $table->string('origin')->nullable();
        $table->string('campaign')->nullable();
        $table->string('ipaddress')->nullable();
        $table->string('desktop')->nullable();
        $table->string('device')->nullable();
        $table->string('lang')->nullable();
      });

      Schema::table('users_oims', function (Blueprint $table) {
        $table->string('status')->nullable();
        $table->string('origin')->nullable();
        $table->string('campaign')->nullable();
        $table->string('ipaddress')->nullable();
        $table->string('desktop')->nullable();
        $table->string('device')->nullable();
        $table->string('lang')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
