<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_jakarta', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('username')->nullable();
            $table->date('dob')->nullable();
            $table->string('address')->nullable();
            $table->string('kota');
            $table->string('ktp')->nullable();
            $table->string('upload_ktp')->nullable();
            $table->string('photo')->nullable();
            $table->integer('phone')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('line')->nullable();
            $table->string('telegram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('instagram')->nullable();
            $table->string('bigo')->nullable();
            $table->integer('parent');
            $table->integer('level');
            $table->integer('lev_1')->nullable();
            $table->integer('lev_2')->nullable();
            $table->integer('lev_3')->nullable();
            $table->integer('lev_4')->nullable();
            $table->integer('lev_5')->nullable();
            $table->integer('lev_6')->nullable();
            $table->integer('id_old')->nullable();
            $table->integer('id_mkt')->nullable();
            $table->integer('ref_old')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('users_surabaya', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('username')->nullable();
            $table->date('dob')->nullable();
            $table->string('address')->nullable();
            $table->string('kota');
            $table->string('ktp')->nullable();
            $table->string('upload_ktp')->nullable();
            $table->string('photo')->nullable();
            $table->integer('phone')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('line')->nullable();
            $table->string('telegram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('instagram')->nullable();
            $table->string('bigo')->nullable();
            $table->integer('parent');
            $table->integer('level');
            $table->integer('lev_1')->nullable();
            $table->integer('lev_2')->nullable();
            $table->integer('lev_3')->nullable();
            $table->integer('lev_4')->nullable();
            $table->integer('lev_5')->nullable();
            $table->integer('lev_6')->nullable();
            $table->integer('id_old')->nullable();
            $table->integer('id_mkt')->nullable();
            $table->integer('ref_old')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('users_bandung', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('username')->nullable();
            $table->date('dob')->nullable();
            $table->string('address')->nullable();
            $table->string('kota');
            $table->string('ktp')->nullable();
            $table->string('upload_ktp')->nullable();
            $table->string('photo')->nullable();
            $table->integer('phone')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('line')->nullable();
            $table->string('telegram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('instagram')->nullable();
            $table->string('bigo')->nullable();
            $table->integer('parent');
            $table->integer('level');
            $table->integer('lev_1')->nullable();
            $table->integer('lev_2')->nullable();
            $table->integer('lev_3')->nullable();
            $table->integer('lev_4')->nullable();
            $table->integer('lev_5')->nullable();
            $table->integer('lev_6')->nullable();
            $table->integer('id_old')->nullable();
            $table->integer('id_mkt')->nullable();
            $table->integer('ref_old')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('users_medan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('username')->nullable();
            $table->date('dob')->nullable();
            $table->string('address')->nullable();
            $table->string('kota');
            $table->string('ktp')->nullable();
            $table->string('upload_ktp')->nullable();
            $table->string('photo')->nullable();
            $table->integer('phone')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('line')->nullable();
            $table->string('telegram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('instagram')->nullable();
            $table->string('bigo')->nullable();
            $table->integer('parent');
            $table->integer('level');
            $table->integer('lev_1')->nullable();
            $table->integer('lev_2')->nullable();
            $table->integer('lev_3')->nullable();
            $table->integer('lev_4')->nullable();
            $table->integer('lev_5')->nullable();
            $table->integer('lev_6')->nullable();
            $table->integer('id_old')->nullable();
            $table->integer('id_mkt')->nullable();
            $table->integer('ref_old')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('users_jogja', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('username')->nullable();
            $table->date('dob')->nullable();
            $table->string('address')->nullable();
            $table->string('kota');
            $table->string('ktp')->nullable();
            $table->string('upload_ktp')->nullable();
            $table->string('photo')->nullable();
            $table->integer('phone')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('line')->nullable();
            $table->string('telegram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('instagram')->nullable();
            $table->string('bigo')->nullable();
            $table->integer('parent');
            $table->integer('level');
            $table->integer('lev_1')->nullable();
            $table->integer('lev_2')->nullable();
            $table->integer('lev_3')->nullable();
            $table->integer('lev_4')->nullable();
            $table->integer('lev_5')->nullable();
            $table->integer('lev_6')->nullable();
            $table->integer('id_old')->nullable();
            $table->integer('id_mkt')->nullable();
            $table->integer('ref_old')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('users_purwokerto', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('username')->nullable();
            $table->date('dob')->nullable();
            $table->string('address')->nullable();
            $table->string('kota');
            $table->string('ktp')->nullable();
            $table->string('upload_ktp')->nullable();
            $table->string('photo')->nullable();
            $table->integer('phone')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('line')->nullable();
            $table->string('telegram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('instagram')->nullable();
            $table->string('bigo')->nullable();
            $table->integer('parent');
            $table->integer('level');
            $table->integer('lev_1')->nullable();
            $table->integer('lev_2')->nullable();
            $table->integer('lev_3')->nullable();
            $table->integer('lev_4')->nullable();
            $table->integer('lev_5')->nullable();
            $table->integer('lev_6')->nullable();
            $table->integer('id_old')->nullable();
            $table->integer('id_mkt')->nullable();
            $table->integer('ref_old')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('users_malang', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('username')->nullable();
            $table->date('dob')->nullable();
            $table->string('address')->nullable();
            $table->string('kota');
            $table->string('ktp')->nullable();
            $table->string('upload_ktp')->nullable();
            $table->string('photo')->nullable();
            $table->integer('phone')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('line')->nullable();
            $table->string('telegram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('instagram')->nullable();
            $table->string('bigo')->nullable();
            $table->integer('parent');
            $table->integer('level');
            $table->integer('lev_1')->nullable();
            $table->integer('lev_2')->nullable();
            $table->integer('lev_3')->nullable();
            $table->integer('lev_4')->nullable();
            $table->integer('lev_5')->nullable();
            $table->integer('lev_6')->nullable();
            $table->integer('id_old')->nullable();
            $table->integer('id_mkt')->nullable();
            $table->integer('ref_old')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('users_banjarmasin', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('username')->nullable();
            $table->date('dob')->nullable();
            $table->string('address')->nullable();
            $table->string('kota');
            $table->string('ktp')->nullable();
            $table->string('upload_ktp')->nullable();
            $table->string('photo')->nullable();
            $table->integer('phone')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('line')->nullable();
            $table->string('telegram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('instagram')->nullable();
            $table->string('bigo')->nullable();
            $table->integer('parent');
            $table->integer('level');
            $table->integer('lev_1')->nullable();
            $table->integer('lev_2')->nullable();
            $table->integer('lev_3')->nullable();
            $table->integer('lev_4')->nullable();
            $table->integer('lev_5')->nullable();
            $table->integer('lev_6')->nullable();
            $table->integer('id_old')->nullable();
            $table->integer('id_mkt')->nullable();
            $table->integer('ref_old')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('users_bali', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('username')->nullable();
            $table->date('dob')->nullable();
            $table->string('address')->nullable();
            $table->string('kota');
            $table->string('ktp')->nullable();
            $table->string('upload_ktp')->nullable();
            $table->string('photo')->nullable();
            $table->integer('phone')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('line')->nullable();
            $table->string('telegram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('instagram')->nullable();
            $table->string('bigo')->nullable();
            $table->integer('parent');
            $table->integer('level');
            $table->integer('lev_1')->nullable();
            $table->integer('lev_2')->nullable();
            $table->integer('lev_3')->nullable();
            $table->integer('lev_4')->nullable();
            $table->integer('lev_5')->nullable();
            $table->integer('lev_6')->nullable();
            $table->integer('id_old')->nullable();
            $table->integer('id_mkt')->nullable();
            $table->integer('ref_old')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('users_oims', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('username')->nullable();
            $table->date('dob')->nullable();
            $table->string('address')->nullable();
            $table->string('kota');
            $table->string('ktp')->nullable();
            $table->string('upload_ktp')->nullable();
            $table->string('photo')->nullable();
            $table->integer('phone')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('line')->nullable();
            $table->string('telegram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('instagram')->nullable();
            $table->string('bigo')->nullable();
            $table->integer('parent');
            $table->integer('level');
            $table->integer('lev_1')->nullable();
            $table->integer('lev_2')->nullable();
            $table->integer('lev_3')->nullable();
            $table->integer('lev_4')->nullable();
            $table->integer('lev_5')->nullable();
            $table->integer('lev_6')->nullable();
            $table->integer('id_old')->nullable();
            $table->integer('id_mkt')->nullable();
            $table->integer('ref_old')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
