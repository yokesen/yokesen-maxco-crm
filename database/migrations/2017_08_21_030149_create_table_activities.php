<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableActivities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('activity');
            $table->integer('client_id');
            $table->integer('user_id');
            $table->integer('lev_1')->nullable();
            $table->integer('lev_2')->nullable();
            $table->integer('lev_3')->nullable();
            $table->integer('lev_4')->nullable();
            $table->integer('lev_5')->nullable();
            $table->integer('lev_6')->nullable();
            $table->date('next_fu')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actitivies');
    }
}
