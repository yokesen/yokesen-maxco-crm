<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIdCmsPrivillegesToAllTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('users_jakarta', function (Blueprint $table) {
          $table->integer('id_cms_privileges')->default(0);
      });

      Schema::table('users_surabaya', function (Blueprint $table) {
          $table->integer('id_cms_privileges')->default(0);
      });

      Schema::table('users_bandung', function (Blueprint $table) {
          $table->integer('id_cms_privileges')->default(0);
      });

      Schema::table('users_medan', function (Blueprint $table) {
          $table->integer('id_cms_privileges')->default(0);
      });

      Schema::table('users_jogja', function (Blueprint $table) {
          $table->integer('id_cms_privileges')->default(0);
      });

      Schema::table('users_purwokerto', function (Blueprint $table) {
          $table->integer('id_cms_privileges')->default(0);
      });

      Schema::table('users_malang', function (Blueprint $table) {
          $table->integer('id_cms_privileges')->default(0);
      });

      Schema::table('users_banjarmasin', function (Blueprint $table) {
          $table->integer('id_cms_privileges')->default(0);
      });

      Schema::table('users_bali', function (Blueprint $table) {
          $table->integer('id_cms_privileges')->default(0);
      });

      Schema::table('users_oims', function (Blueprint $table) {
          $table->integer('id_cms_privileges')->default(0);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
