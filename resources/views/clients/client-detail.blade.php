@extends("crudbooster::admin_template")

@section('csspage')
<link rel="stylesheet" href="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<style media="screen">
  .inputUploadManual {
    display: none;
  }
</style>

<style>
  .lb-outerContainer {
    width: 80%;
    height: auto;
  }

  .lb-image{
    width: 100%;
    height: auto;
  }
</style>

@endsection

@section("content")
<div class="row">
  <div class="col-md-4">

    <!-- Profile Image -->
    <div class="box box-primary">
      <div class="box-body box-profile">
        <img class="profile-user-img img-responsive img-circle" src="{{ $client->photo == '' ? '/images/user-default.png' : $client->photo}}" alt="User profile picture">

        <h3 class="profile-username text-center">{{ ucwords($client->username) }}</h3>

        <p class="text-muted text-center">{{ ucfirst($client->status) }}</p>

        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b>Name</b> <a class="pull-right">{{ $client->name ? ucwords($client->name) : '-' }}</a>
          </li>
          <li class="list-group-item">
            <b>Username</b> <a class="pull-right">{{ $client->username ? $client->username : '-' }}</a>
          </li>
          <li class="list-group-item">
            <b>Email</b> <a class="pull-right"><i class="fa fa-envelope-o text-{{$client->contactEmailValidation == "true" ? "success" : "danger"}}"></i> {{ $client->email ? $client->email : '-' }}</a>
          </li>
          <li class="list-group-item">
            <b>Phone</b> <a class="pull-right">{{ $client->phone ? $client->phone : '-' }}</a>
          </li>
          <li class="list-group-item">
            <b>Parent</b> <a class="pull-right">{{ $client->parent ? ucwords($client->induk->name) : '-' }}</a>
          </li>
          <li class="list-group-item">
            <b>Origin</b> <a class="pull-right">{{ $client->origin ? ucwords($client->origin) : '-' }}</a>
          </li>
          <li class="list-group-item">
            <b>Campaign</b> <a class="pull-right" href="/crm/share-campaign/{{$client->campaign}}" target="_blank">{{ $client->campaign ? $client->campaign : '-' }}</a>
          </li>
        </ul>

        <form action="{{ route('ChangeDateFollowUp',$client->id)}}" method="post">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="exampleInputEmail1">Next Follow Up</label>
            <input type="date" class="form-control" name="date" value="{{ $client->updated_at ? $client->updated_at->format('Y-m-d')  : ''}}">
          </div>
          <div class="form-group">
            <input type="submit" class="form-control btn btn-warning" value="Submit Follow Up">
          </div>
        </form>
        <form action="{{ route('ChangeStatusClient', $client->id)}}" method="post">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="exampleInputEmail1">Status</label>
            <select class="form-control" name="status">
              <option value="registered" {{ $client->status == 'registered' ? 'selected' : '' }}>Lead</option>
              <option value="no-respond" {{ $client->status == 'no-respond' ? 'selected' : '' }}>No Respond</option> 
              <option value="contact" {{ $client->status == 'contact' ? 'selected' : '' }}>Contact</option> 
              <option value="potential" {{ $client->status == 'potential' ? 'selected' : '' }}>Potential</option>
              <option value="win" {{ $client->status == 'win' ? 'selected' : '' }}>Win</option>
              <option value="lose" {{ $client->status == 'lose' ? 'selected' : '' }}>Lose</option>
            </select>
          </div>
          <div class="form-group">
            <input type="submit" class="form-control btn btn-primary" value="Submit Status">
          </div>
        </form>
        
        @if(CRUDBooster::myPrivilegeID() == '3' || CRUDBooster::myPrivilegeID() == '4')
        <hr>
        <?php

        $child1 = DB::table('users_jakarta')->where('parent', CRUDBooster::myID())->get();
        ?>

        <form action="{{ route('assignLead', $client->id)}}" method="post">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="exampleInputEmail1">Assign This Lead to</label>
            @if (count($child1)>0)
            <select class="form-control" name="assign">
              <option value="">---Pilih----</option>


              @foreach ($child1 as $person1)
              @php

              switch ($person1->level) {
              case '2': $color = "teal"; break;
              case '3': $color = "teal"; break;
              case '4': $color = "teal"; break;
              case '5': $color = "orange"; break;
              case '6': $color = "maroon"; break;
              default: $color = "white"; break;
              }

              @endphp
              <option value="{{$person1->id}}" class="bg-{{$color}}">{{$person1->name}}</option>
              @php
              $child2 = DB::table('users_jakarta')->where('parent',$person1->id)->get();
              @endphp
              @if (count($child2)>0)
              @foreach ($child2 as $person2)
              <option value="{{$person2->id}}">{{$person2->name}}</option>
              @php
              $child3 = DB::table('users_jakarta')->where('parent',$person2->id)->get();
              @endphp
              @endforeach
              @if (count($child3)>0)
              @foreach ($child3 as $person3)
              <option value="{{$person3->id}}">{{$person3->name}}</option>
              @endforeach
              @endif
              @endif

              @endforeach


            </select>
            @else
            <p>You don't have team member</p>
            @endif
          </div>
          <div class="form-group">
            <input type="submit" class="form-control btn btn-danger" value="Assign Leads">
          </div>
        </form>
        @endif

        <!--<a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>-->
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->

    <!-- About Me Box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Client's Profile</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <strong><i class="fa fa-book margin-r-5"></i> Metadata</strong>

        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b>IP addr</b> <a class="pull-right">{{ $client->ipaddress}}</a>
          </li>
          <li class="list-group-item">
            <b>Desktop</b> <a class="pull-right">{{ $client->desktop}}</a>
          </li>
          <li class="list-group-item">
            <b>Device</b> <a class="pull-right">{{ $client->device}}</a>
          </li>
          <li class="list-group-item">
            <b>Lang</b> <a class="pull-right">{{ $client->lang}}</a>
          </li>
        </ul>

        <hr>

        <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

        <p class="text-muted">{{ $client->kota }}</p>

        <hr>

        <strong><i class="fa fa-pencil margin-r-5"></i> Social Media</strong>

        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b>Facebook</b> <a class="pull-right">{{ $client->facebook}}</a>
          </li>
          <li class="list-group-item">
            <b>Google +</b> <a class="pull-right">{{ $client->google}}</a>
          </li>
          <li class="list-group-item">
            <b>Twitter</b> <a class="pull-right">{{ $client->twitter}}</a>
          </li>
          <li class="list-group-item">
            <b>Linkedin</b> <a class="pull-right">{{ $client->linkedin}}</a>
          </li>
          <li class="list-group-item">
            <b>Instagram</b> <a class="pull-right">{{ $client->instagram}}</a>
          </li>

        </ul>

        <hr>

        <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

        <p></p>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
  <div class="col-md-8">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#timeline" data-toggle="tab">Timeline</a></li>
        <li><a href="#chat" data-toggle="tab">Chat</a></li>
        <li><a href="#settings" data-toggle="tab">Details</a></li>
        @if($client->contactEmailValidation == "true")
        <li><a href="#email" data-toggle="tab">Email</a></li>
        @endif
        {{--
              <li><a href="#socialdata" data-toggle="tab">Social Data</a></li>
              <li><a href="#autobot" data-toggle="tab">Activities Feed</a></li>--}}
      </ul>
      <div class="tab-content">
        <div class="tab-pane" id="chat">

          <!-- DIRECT CHAT -->
          <div class="box box-success direct-chat direct-chat-success">
            <div class="box-header with-border">
              <h3 class="box-title">Direct Chat</h3>

              <div class="box-tools pull-right">

              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <!-- Conversations are loaded here -->
              <div class="direct-chat-messages" id="directChatMessages" style="heigth:600px!important;">
                @if($client->whatsapp != '-')

                <!-- Message to the right -->
                <div class="direct-chat-msg center">
                  <div class="direct-chat-info clearfix">
                    <span class="direct-chat-name pull-center">&nbsp DEST &nbsp</span>
                    <span> </span>
                    <span class="direct-chat-timestamp pull-center">{{ date('d M, h:i a',strtotime(date('Y-m-d H:i:s'))) }}</span>
                  </div>
                  <!-- /.direct-chat-info -->
                  <img class="direct-chat-img" src="{{ url('/') }}/images/logo-dest.png" alt="message user image">
                  <!-- /.direct-chat-img -->
                  <div class="direct-chat-text pull-center">
                    <p>please wait while DEST fetching data from Whatsapp</p>
                  </div>
                  <!-- /.direct-chat-text -->
                </div>
                <!-- /.direct-chat-msg -->
                @else
                <div class="text-center text danger">
                  <p>No Whatsapp Number</p>
                </div>
                @endif
              </div>
              <!--/.direct-chat-messages-->



              @if($client->whatsapp != '-')

              <span class="input-group-btn">
                <button type="submit" class="btn btn-success btn-flat btn-block" data-toggle="modal" data-target="#whatspp-temp">Chat Client di sini</button>
              </span>



              @else
              <p class="text-right"> <small class="text-danger">No Whatsapp Number</small> </p>
              @endif

              <!-- /.box-body -->
              <div class="box-footer">
                @if($client->whatsapp != '-')
                <form action="{{route('whatsappSend',$client->whatsapp)}}" method="post">
                  <div class="input-group">
                    <input type="text" name="message" placeholder="Type Message ..." class="form-control" required disabled>
                    <span class="input-group-btn">
                      <span class="btn btn-primary btn-flat" data-toggle="modal" data-target="#whatspp-image" disabled><i class="fa fa-file-image-o margin-r-5"></i></span>
                    </span>
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-success btn-flat" data-toggle="modal" data-target="#whatspp-temp">Send</button>
                    </span>
                    {{csrf_field()}}
                  </div>
                </form>
                @else
                <p class="text-right"> <small class="text-danger">No Whatsapp Number</small> </p>
                @endif
              </div>
              <!-- /.box-footer-->
            </div>
            <!--/.direct-chat -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.tab-pane -->
        <div class="active tab-pane" id="timeline">
          <p><b>Follow Up :</b></p>
          <form class="form-horizontal" method="post" action="{{ route('SaveActivity') }}" enctype="multipart/form-data">
            <div class="form-group">
              <div class="col-sm-12">
                <textarea name="activity" rows="2" class="form-control" required></textarea>
              </div>
            </div>
            {{csrf_field()}}
            <input type="hidden" name="client_id" value="{{$client->id}}">
            <input type="hidden" name="parent_id" value="{{$client->parent}}">
            <div class="form-group">
              <div class="col-sm-6">
                <input type="file" name="bukti" class="form-control">
              </div>
              <div class="col-sm-6">
                <button type="submit" class="btn btn-success pull-right">Submit</button>
              </div>
            </div>

          </form>

          <!-- The timeline -->

          <ul class="timeline timeline-inverse">
            @foreach($activities as $activity)

            <!-- timeline time label -->

            <li class="time-label">
              <span class="bg-blue">
                {{ date('d-m-Y',strtotime($activity->created_at))}}
              </span>
            </li>
            <!-- /.timeline-label -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-user bg-aqua"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> {{ $activity->created_at ? $activity->created_at->diffForHumans() : '-' }}</span>

                <h3 class="timeline-header"><a href="{{env('APP_URL')}}/mkt-detail/{{$activity->user_id}}" target="_blank">{{ ucwords($activity->komentator->name) }}</a> {{ $activity->jenis }}</h3>

                <div class="timeline-body">
                  {!! $activity->activity !!}
                  @if(!empty($activity->bukti))
                  <br>
                  <a data-lightbox="roadtrip" href="{{url('/')}}/uploads/{{$activity->bukti}}">
                    <img src="{{url('/')}}/uploads/{{$activity->bukti}}" alt="" width="50%">
                  </a>
                  @endif
                </div>
              </div>
            </li>
            <!-- END timeline item -->
            @endforeach

          </ul>
        </div>
        <!-- /.tab-pane -->

        <div class="tab-pane" id="settings">
          <form class="form-horizontal" action="{{ route('UpdateClientDetail', ['id' => $client->id ])}}" method="POST">
            <div class="form-group">
              <label for="inputName" class="col-sm-2 control-label">Name</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="name" placeholder="Name" value="{{ $client->name }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Tanggal Lahir</label>
              <div class="col-sm-10">
                <input type="date" class="form-control" name="dob" value="{{ $client->dob }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Alamat</label>
              <div class="col-sm-10">
                <textarea class="form-control" name="address" placeholder="Alamat">{{ $client->address }}</textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Kota</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="kota" placeholder="Kota" value="{{ $client->kota }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">KTP</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="ktp" placeholder="No KTP" value="{{ $client->ktp }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Email</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="email" placeholder="Email" value="{{ $client->email }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Phone</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="phone" placeholder="Telepon" value="{{ $client->phone }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">WhatsApp</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="whatsapp" placeholder="WhatsApp" value="{{ $client->whatsapp }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">LINE</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="line" placeholder="LINE" value="{{ $client->line }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Telegram</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="telegram" placeholder="Telegram" value="{{ $client->telegram }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Facebook</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="facebook" placeholder="Facebook" value="{{ $client->facebook }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Google</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="google" placeholder="Google" value="{{ $client->google }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Twitter</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="twitter" placeholder="Twitter" value="{{ $client->twitter }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">LinkedIn</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="linkedin" placeholder="LinkedIn" value="{{ $client->linkedin }}">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Instagram</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="instagram" placeholder="Instagram" value="{{ $client->instagram }}">
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                {{ csrf_field() }}
                <button type="submit" class="btn btn-danger">Submit</button>
              </div>
            </div>
          </form>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="email">

          <form action="{{route('postEmail')}}" class="form-horizontal" method="post">
            <div class="form-group">
              <label class="col-sm-2 control-label">Title</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="title" placeholder="Title" required>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">

              </div>
            </div>
            <textarea class="textarea" name="content" placeholder="Place some text here" style="width: 100%; height: 400px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required></textarea>

            <div class="form-group">
              <label class="col-sm-2 control-label">From</label>
              <div class="col-sm-4">

                <input type="text" name="senderName" class="form-control" value="{{CRUDBooster::myName()}}" readonly>

              </div>

              <div class="col-sm-6">
                <div class="input-group">
                  <input type="text" name="senderName" class="form-control" required>
                  <span class="input-group-addon">@smartmaxco.co.id</span>
                </div>
              </div>
            </div>
            <div class="form-group">
              {{ csrf_field() }}
              <input type="hidden" name="clientid" value="{{$client->id}}">
              <input type="hidden" name="recepientName" value="{{$client->name}}">
              <input type="hidden" name="recepientEmail" value="{{$client->email}}">
              <div class="col-sm-12">
                <input type="submit" name="submit" value="SEND" class="btn btn-primary btn-block">
              </div>
            </div>
          </form>

        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="autobot">
          <p><small>not enough data</small></p>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- /.nav-tabs-custom -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

<div class="modal fade" id="whatspp-temp">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Pengumuman</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <div class="alert alert-primary center">
              <h4>Saat ini nomor Whatsapp sedang dalam proses verifikasi Official Whatsapp Business!</h4>
              Agar tidak mengganggu pekerjaan silahkan melakukan follow up dengan menggunakan nomor Whatsapp pribadi.
            </div>
          </div>
        </div>

        @if($client->whatsapp != '-')
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <a href="https://wa.me/{{$client->whatsapp}}" target="_blank" class="btn btn-success btn-flat btn-block">Chat Client di sini</a>
          </div>
        </div>
        @endif
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<div class="modal fade" id="whatspp-image">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Pilih Gambar</h4>
      </div>
      <div class="modal-body">
        <div class="row searchArea">
          <div class="col-md-6">
            <div class="input-group">
              <input type="text" name="keyword-image" placeholder="keyword" id="keywordImage" class="form-control">
              <span class="input-group-btn">
                <button class="btn btn-primary btn-flat" id="search-image" onclick="searchImage()"><i class="fa fa-search margin-r-5"></i></button>
              </span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12" id="image-list">
          </div>
          <div class="col-md-12 inputUploadManual mt-50">
            <div class="row">
              <div class="col-md-12">
                <span id="resultManualPhoto"></span>
              </div>
            </div>
            <form class="form-group" id="formUploadManual" enctype="multipart/form-data">
              <div class="row form-group">

                <div class="col-sm-12">
                  <input type="file" name="image" class"form-control" id="fileManualPhoto">
                </div>
              </div>
              <div class="row form-group">

                <div class="col-sm-12">
                  <input type="text" name="keyword" class="form-control" id="keywordManualPhoto" placeholder="Masukkan keyword...">
                </div>
              </div>
              {{ csrf_field() }}
              <input id="submitManualPhoto" onclick="SubmitUploadManual()" class="form-control btn btn-success" value="submit">
            </form>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn pull-left btn-warning uploadManualButton" onClick="uploadManual()">Upload</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


@endsection



@section("jsonpage")
<script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('.textarea').wysihtml5()
  })
</script>
@endsection