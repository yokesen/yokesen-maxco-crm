@extends("crudbooster::admin_template")
@section("content")
  <section>
    <div class="row">
      <div class="col-md-6">
        <div class="box box-success">
          <div class="box-header">

          </div>
          <div class="box-body">
            <form class="form-horizontal" action="{{ route('CreateClient')}}" method="POST">
              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="name" placeholder="Name" value="{{ $client->name }}">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="email" placeholder="Email" value="{{ $client->email }}" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Phone</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="phone" placeholder="Telepon" value="{{ $client->phone }}" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">WhatsApp</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="whatsapp" placeholder="WhatsApp" value="{{ $client->whatsapp }}" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Origin</label>
                <div class="col-sm-10">
                  <select class="form-control" name="origin">
                    <option value="Manual">Manual</option>
                    <option value="Walk in">Walk in</option>
                    <option value="Reference">Reference</option>
                    <option value="Pameran">Pameran</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  {{ csrf_field() }}
                  <button type="submit" class="btn btn-danger">Submit</button>
                </div>
              </div>
            </form>
          </div>
          <div class="box-header">

          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
