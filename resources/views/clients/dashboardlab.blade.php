@extends("crudbooster::admin_template")
@section('callcss')
  <link rel="stylesheet" href="{{url('/')}}/plugin/jvectormap/jquery-jvectormap-1.2.2.css">
@endsection
@section("content")
  @if(CRUDBooster::me()->level == 1)
  <section class="content"> 
      <div class="row">
        <div class="col-md-7">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Visitors Report</h3>

              <!-- <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="row">
                <div class="col-md-9 col-sm-8">
                  <div class="pad">
                    <!-- Map will be created here -->
                    <div id="world-map-markers" style="height: 325px;"></div>
                  </div>
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-4">
                  <div class="pad box-pane-right bg-green" style="min-height: 280px">
                    <div class="description-block margin-bottom">
                      <!-- <div class="sparkbar pad" data-color="#fff">90,70,90,70,75,80,70</div> -->
                      <h5 class="description-header">{{$visitorCount}}</h5>
                      <span class="description-text">Visits</span>
                    </div>
                    <div class="description-block margin-bottom">
                      <!-- <div class="sparkbar pad" data-color="#fff">90,50,90,70,61,83,63</div> -->
                      <h5 class="description-header">{{$visitRefPercent}}%</h5>
                      <span class="description-text">Referrals</span>
                    </div>
                    <div class="description-block">
                      <!-- <div class="sparkbar pad" data-color="#fff">90,50,90,70,61,83,63</div> -->
                      <h5 class="description-header">{{100 - $visitRefPercent}}%</h5>
                      <span class="description-text">Organic</span>
                    </div>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <div class="box-footer no-border">
              <p>Last 3 Days</p>
            </div>
            <!-- /.box-body -->
          </div>

          <div class="box box-solid bg-teal-gradient">
           <div class="box-header">
             <i class="fa fa-th"></i>

             <h3 class="box-title">Client Graph</h3>

             <!-- <div class="box-tools pull-right">
               <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
               </button>
               <button type="button" class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i>
               </button>
             </div> -->
           </div>
           <div class="box-body border-radius-none">
             <div class="chart" id="line-chart" style="height: 250px;"></div>
           </div>
           <div class="box-footer no-border">
             <div class="row">
               <!-- <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                 <input type="text" class="knob" data-readonly="true" value="20" data-width="60" data-height="60" data-fgColor="#39CCCC">
                 <div class="knob-label">Mail-Orders</div>
               </div>
               <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                 <input type="text" class="knob" data-readonly="true" value="50" data-width="60" data-height="60" data-fgColor="#39CCCC">
                 <div class="knob-label">Online</div>
               </div>
               <div class="col-xs-4 text-center">
                 <input type="text" class="knob" data-readonly="true" value="30" data-width="60" data-height="60" data-fgColor="#39CCCC">
                 <div class="knob-label">In-Store</div>
               </div> -->
             </div>
           </div>
         </div>
        </div>
        <div class="col-md-5">
              <!-- USERS LIST -->
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Team Members</h3>

                  <div class="box-tools pull-right">
                    <span class="label label-danger">{{count($teammember)}} New Members</span>
                    <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button> -->
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                  <ul class="users-list clearfix justify-content-center">
                    @foreach ($teammember as $member)
                    <li>
                      <img src="@if(isset($member->photo)){{url('/')}}/{{$member->photo}} @else {{url('/')}}/images/user-default.png @endif" alt="User Image">
                      <a class="users-list-name" href="{{url('/')}}/crm/mkt-detail/{{$member->id}}">{{$member->name}}</a>
                      <span class="users-list-date">{{date('d-m-Y', strtotime($member->created_at))}}</span>
                      <!-- <span class="users-list-date">Today</span> -->

                    </li>
                    @endforeach
                  </ul>
                  <!-- /.users-list -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                  <a href="{{url('/')}}/crm/team" class="uppercase">View All Users</a>
                </div>
                <!-- /.box-footer -->
              </div>
              <!--/.box -->
            </div>
      </div>
  </section>
  @else
  <section class="content">
  <div class="row">
    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-blue"><i class="ion ion-social-facebook"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Facebook</span>
          <span class="info-box-number">{{ $facebook->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-teal"><i class="ion ion-social-twitter"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Twitter</span>
          <span class="info-box-number">{{ $twitter->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-light-blue"><i class="ion ion-ios-chatboxes"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Messenger</span>
          <span class="info-box-number">{{ $messenger->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-paper-plane"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Telegram</span>
          <span class="info-box-number">{{ $telegram->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-weixin"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">We Chat</span>
          <span class="info-box-number">{{ $wechat->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="ion ion-social-whatsapp"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Whatsapp</span>
          <span class="info-box-number">{{ $whatsapp->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-grey"><i class="ion ion-social-linkedin"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">LinkedIn</span>
          <span class="info-box-number">{{ $linkedin->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-comment-o"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Line</span>
          <span class="info-box-number">{{ $line->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-envelope"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Email Campaign</span>
          <span class="info-box-number">{{ $email->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-sitemap"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Direct Link</span>
          <span class="info-box-number">{{ $direct->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-globe"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Organics</span>
          <span class="info-box-number">{{ $organics->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-thumb-tack"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Ads</span>
          <span class="info-box-number">{{ $ads->count }} <small>Users</small></span>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-4 col-xs-12">
      <div class="small-box bg-gray">
        <div class="inner">
          <h3>{{ $registered->count }}</h3>
          <p>Leads</p>
        </div>
        <div class="icon">
          <i class="fa fa-phone"></i>
        </div>
        <a href="{{url('/')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-lg-4 col-xs-12">
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{ $contact->count }}</h3>
          <p>Contact</p>
        </div>
        <div class="icon">
          <i class="ion ion-android-contacts"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-lg-4 col-xs-12">
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{ $potential->count }}</h3>
          <p>Potential</p>
        </div>
        <div class="icon">
          <i class="fa fa-money"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

  </div>
    <div class="row justify-content-center">
      <div class="col-lg-2 col-xs-12">
      </div>
      <div class="col-lg-4 col-xs-12">
        <div class="small-box bg-green">
          <div class="inner">
            <h3>{{ $win->count }}</h3>
            <p>Win</p>
          </div>
          <div class="icon">
            <i class="fa fa-trophy"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-xs-12">
        <div class="small-box bg-red">
          <div class="inner">
            <h3>{{ $lose->count }}</h3>
            <p>Lose</p>
          </div>
          <div class="icon">
            <i class="fa fa-sign-out"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>

</section>
  @endif
@endsection

@section('jsonpage')
<script src="{{url('/')}}/plugin/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

<script src="{{url('/')}}/plugin/jvectormap/src/map-object.js"></script>
<script src="{{url('/')}}/plugin/jvectormap/src/region.js"></script>
<script src="{{url('/')}}/plugin/jvectormap/src/marker.js"></script>

<script src="{{url('/')}}/plugin/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
<script>
    $(function(){
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

        setTimeout(function(){
          var mapObj = $('#world-map-markers').vectorMap('get', 'mapObject'),
          center = mapObj.pointToLatLng(mapObj.width / 2, mapObj.height / 2);
          if(mapObj.scale!== NaN){
            // var config = {
            //     animate: true,
            //     lat: center.lat,
            //     lng: center.lng,
            //     scale: 1
            // };
            // mapObj.setFocus(config)

          }
          // $('#world-map-markers').vectorMap('set', 'focus', {region: 'ID', animate: true});
        },1000);

        $.post("{{route('apivisitorreport')}}", {
          parent:{{CRUDBooster::myID()}}
        }, function(data, status) {

          if(status==="success"){
            var dataMap = data.reduce(function(arr=[],item,index){
              arr.push(item._count);
              return arr;
            },[]);

            function getMaxOfArray(numArray) {
              return Math.max.apply(null, numArray);
            }

            var dtMax = getMaxOfArray(dataMap);
            console.log(dtMax);
            var markers=[];
            var rMap = [2,3,4,5,6];
            data.forEach(element => {
              var perc = (element._count / dtMax) * 100;
              var r = 2;
              if(perc > 81 || perc === 81 ){
                r = 7;
              }else if(perc > 61 || perc === 61){
                r = 6;
              }else if(perc > 41 || perc === 41){
                r = 5;
              }else if(perc > 21 || perc === 21){
                r = 4;
              }else{
                r = 3;
              }

              markers.push({ latLng: [element.Lat,element.Lng], name: element.cityName,style:{
                // fill: 'yellow',
                r
              }});
            });
            $('#world-map-markers').vectorMap({
                map              : 'world_mill_en',
                normalizeFunction: 'polynomial',
                // focusOn: {
                //   x: 1.5,
                //   y: 1.5,
                //   scale: 3,
                //   animate: true 
                // },
                hoverOpacity     : 0.7,
                hoverColor       : false,
                backgroundColor  : 'transparent',
                regionStyle      : {
                  initial      : {
                    fill            : 'rgba(210, 214, 222, 1)',
                    'fill-opacity'  : 1,
                    stroke          : 'none',
                    'stroke-width'  : 0,
                    'stroke-opacity': 1
                  },
                  hover        : {
                    'fill-opacity': 0.7,
                    cursor        : 'pointer'
                  },
                  selected     : {
                    fill: 'yellow'
                  },
                  selectedHover: {}
                },
                // selectedRegions:['ID'],
                markerStyle      : {
                  initial: {
                    fill  : '#00a65a',
                    stroke: '#111'
                  }
                },
                markers:markers,
                onRegionLabelShow: function(event, label, code) {
                  // console.log('label',label);
                  // $(label).append($("<br/>"));
                  // $(label).append($("<span/>", {
                  //     'class': 'population',
                  //     'html': 'Populație: ' + parseInt(Math.random() * (1000000 - 1000) + 1000)
                  // }));
                },
                onRegionTipShow: function(event, label, code) {
                  console.log('label',label);
                }

              });
          }
        });

        $.post("{{route('apiclientgraph')}}", {
          parent:{{CRUDBooster::myID()}}
        }, function(data, status) {
          if(status==="success"){
            var line = new Morris.Line({
                element          : 'line-chart',
                resize           : true,
                data             :data,
                xkey             : 'y',
                ykeys            : ['item1'],
                labels           : ['Item 1'],
                lineColors       : ['#efefef'],
                lineWidth        : 2,
                hideHover        : 'auto',
                gridTextColor    : '#fff',
                gridStrokeWidth  : 0.4,
                pointSize        : 4,
                pointStrokeColors: ['#efefef'],
                gridLineColor    : '#efefef',
                gridTextFamily   : 'Open Sans',
                gridTextSize     : 10
              });

          }
        })

    });


</script>
@endsection
