@extends("crudbooster::admin_template")

@section('csspage')
<link rel="stylesheet" href="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<style>
  .lb-outerContainer {
    width: 80%;
    height: auto;
  }

  .lb-image {
    width: 100%;
    height: auto;
  }
</style>
@endsection

@section("content")
<div class="row">
  <div class="col-md-4">

    <!-- Profile Image -->
    <div class="box box-primary">
      <div class="box-body box-profile">
        <img class="profile-user-img img-responsive img-circle" src="{{ $client->photo == '' ? '/images/user-default.png' : $client->photo}}" alt="User profile picture">

        <h3 class="profile-username text-center">{{ ucwords($client->name)}}</h3>
        @if(isset($follows) && $follows->status=="Active")
        <p class="text-center text-success">Follow</p>
        @endif
        <p class="text-muted text-center">{{ ucfirst($client->status) }}</p>

        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b>Email</b> <a class="pull-right"><i class="fa fa-envelope-o text-{{$client->contactEmailValidation == "true" ? "success" : "danger"}}"></i> {{ $client->email ? $client->email : '-' }}</a>
          </li>
          <li class="list-group-item">
            <b>Phone</b> <a class="pull-right">{{ $client->phone ? $client->phone : '-' }}</a>
          </li>
          <li class="list-group-item">
            <b>Parent</b> <a class="pull-right">{{ $client->parent ? ucwords($client->induk->name) : '-' }}</a>
          </li>
          <li class="list-group-item">
            <b>Origin</b> <a class="pull-right">{{ $client->origin ? ucwords($client->origin) : '-' }}</a>
          </li>
          <li class="list-group-item">
            <b>Campaign</b> <a class="pull-right">{{ $client->campaign ? $client->campaign : '-' }}</a>
          </li>
        </ul>

        @if($client->status == "lose" || $client->parent == "10037" || $client->parent == "10044" || $client->parent == "10023" || $client->parent == "1000")
        <a href="{{route('clientClaim',$client->id)}}" class="btn btn-success btn-block">Collect this Lead</a>
        @else
        <form method="post" action="{{route('postfollow')}}">
          {{ csrf_field() }}
          <input name="client_id" value="{{$client->id}}" type="hidden">
          <input name="status" value="{{isset($follows) && $follows->status=='Active'?'Inactive':'Active'}}" type="hidden">
          <input type="submit" class="btn btn-primary btn-block" value="{{isset($follows) && $follows->status=='Active'?'Unfollow':'Follow'}}">
        </form>
        @endif

        @if(CRUDBooster::myPrivilegeID() == '3' || CRUDBooster::myPrivilegeID() == '4')
        <hr>
        <?php

        $child1 = DB::table('users_jakarta')->where('parent', CRUDBooster::myID())->get();
        ?>

        <form action="{{ route('assignLead', $client->id)}}" method="post">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="exampleInputEmail1">Assign This Lead to</label>
            @if (count($child1)>0)
            <select class="form-control" name="assign">
              <option value="">---Pilih----</option>


              @foreach ($child1 as $person1)
              @php

              switch ($person1->level) {
              case '2': $color = "teal"; break;
              case '3': $color = "teal"; break;
              case '4': $color = "teal"; break;
              case '5': $color = "orange"; break;
              case '6': $color = "maroon"; break;
              default: $color = "white"; break;
              }

              @endphp
              <option value="{{$person1->id}}" class="bg-{{$color}}">{{$person1->name}}</option>
              @php
              $child2 = DB::table('users_jakarta')->where('parent',$person1->id)->get();
              @endphp
              @if (count($child2)>0)
              @foreach ($child2 as $person2)
              <option value="{{$person2->id}}">{{$person2->name}}</option>
              @php
              $child3 = DB::table('users_jakarta')->where('parent',$person2->id)->get();
              @endphp
              @endforeach
              @if (count($child3)>0)
              @foreach ($child3 as $person3)
              <option value="{{$person3->id}}">{{$person3->name}}</option>
              @endforeach
              @endif
              @endif

              @endforeach


            </select>
            @else
            <p>You don't have team member</p>
            @endif
          </div>
          <div class="form-group">
            <input type="submit" class="form-control btn btn-danger" value="Assign Leads">
          </div>
        </form>
        @endif
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->

    <!-- About Me Box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Client's Profile</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <strong><i class="fa fa-book margin-r-5"></i> Metadata</strong>

        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b>IP addr</b> <a class="pull-right">{{ $client->ipaddress}}</a>
          </li>
          <li class="list-group-item">
            <b>Desktop</b> <a class="pull-right">{{ $client->desktop}}</a>
          </li>
          <li class="list-group-item">
            <b>Device</b> <a class="pull-right">{{ $client->device}}</a>
          </li>
          <li class="list-group-item">
            <b>Lang</b> <a class="pull-right">{{ $client->lang}}</a>
          </li>
        </ul>

        <hr>

        <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

        <p class="text-muted">{{ $client->kota }}</p>

        <hr>

        <strong><i class="fa fa-pencil margin-r-5"></i> Social Media</strong>

        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b>Facebook</b> <a class="pull-right">{{ $client->facebook}}</a>
          </li>
          <li class="list-group-item">
            <b>Google +</b> <a class="pull-right">{{ $client->google}}</a>
          </li>
          <li class="list-group-item">
            <b>Twitter</b> <a class="pull-right">{{ $client->twitter}}</a>
          </li>
          <li class="list-group-item">
            <b>Linkedin</b> <a class="pull-right">{{ $client->linkedin}}</a>
          </li>
          <li class="list-group-item">
            <b>Instagram</b> <a class="pull-right">{{ $client->instagram}}</a>
          </li>
          <li class="list-group-item">
            <b>Bigo</b> <a class="pull-right">{{ $client->bigo}}</a>
          </li>
        </ul>

        <hr>

        <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

        <p></p>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
  <div class="col-md-8">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#timeline" data-toggle="tab">Timeline</a></li>
        <li><a href="#chat" data-toggle="tab">Chat</a></li>
        <li><a href="#settings" data-toggle="tab">Settings</a></li>
        @if($client->contactEmailValidation == "true")
        <li><a href="#email" data-toggle="tab">Email</a></li>
        @endif

      </ul>
      <div class="tab-content">

        <!-- /.tab-pane -->
        <div class="active tab-pane" id="timeline">
          <p><b>Follow Up :</b></p>
          <form class="form-horizontal" method="post" action="{{ route('SaveActivity') }}" enctype="multipart/form-data">
            <div class="form-group">
              <div class="col-sm-12">
                <textarea name="activity" rows="2" class="form-control"></textarea>
              </div>
            </div>
            {{csrf_field()}}
            <input type="hidden" name="client_id" value="{{$client->id}}">
            <input type="hidden" name="parent_id" value="{{$client->parent}}">
            <div class="form-group">
              <div class="col-sm-6">
                <input type="file" name="bukti" class="form-control">
              </div>
              <div class="col-sm-6">
                <button type="submit" class="btn btn-success pull-right">Submit</button>
              </div>
            </div>
          </form>

          <!-- The timeline -->

          <ul class="timeline timeline-inverse">
            @foreach($activities as $activity)

            <!-- timeline time label -->

            <li class="time-label">
              <span class="bg-blue">
                {{ date('d-m-Y',strtotime($activity->created_at))}}
              </span>
            </li>
            <!-- /.timeline-label -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-user bg-aqua"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> {{ $activity->created_at ? $activity->created_at->diffForHumans() : '-' }}</span>

                <h3 class="timeline-header"><a href="{{env('APP_URL')}}/mkt-detail/{{$activity->user_id}}" target="_blank">{{ ucwords($activity->komentator->name) }}</a> {{ $activity->jenis }}</h3>

                <div class="timeline-body">
                  {!! $activity->activity !!}
                  @if(!empty($activity->bukti))
                  <br>
                  <a data-lightbox="roadtrip" href="{{url('/')}}/uploads/{{$activity->bukti}}">
                    <img src="{{url('/')}}/uploads/{{$activity->bukti}}" alt="" width="50%">
                  </a>
                  @endif
                </div>
              </div>
            </li>
            <!-- END timeline item -->
            @endforeach

          </ul>
        </div>
        <!-- /.tab-pane -->

        <div class="tab-pane" id="chat">

          <!-- DIRECT CHAT -->
          <div class="box box-success direct-chat direct-chat-success">
            <div class="box-header with-border">
              <h3 class="box-title">Direct Chat</h3>


            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <!-- Conversations are loaded here -->
              <div class="direct-chat-messages" id="directChatMessages" style="heigth:600px!important;">
                @foreach($chats as $n => $chat)
                @if($chat->fromme == '1')

                <!-- Message to the right -->
                <div class="direct-chat-msg right">
                  <div class="direct-chat-info clearfix">
                    <span class="direct-chat-name pull-right">
                      @if($chat->status == "delivered")
                      <i class="fa fa-check" aria-hidden="true"></i>
                      <i class="fa fa-check" aria-hidden="true" style="margin-left:-10px"></i>
                      @elseif($chat->status == "viewed")
                      <i class="fa fa-check" aria-hidden="true" style="color:#3c8dbc!important;"></i>
                      <i class="fa fa-check" aria-hidden="true" style="color:#3c8dbc!important;margin-left:-10px"></i>
                      @else
                      <i class="fa fa-check" aria-hidden="true"></i>
                      @endif
                    </span>
                    <span class="direct-chat-timestamp pull-right">{{ date('d M, h:i a',strtotime($chat->timestamp.'+7hours')) }}</span>
                  </div>
                  <!-- /.direct-chat-info -->
                  <img class="direct-chat-img" src="/images/user-default.png" alt="message user image">
                  <!-- /.direct-chat-img -->
                  <div class="direct-chat-text pull-right">
                    @if($chat->type == 'image')
                    <img src="{{$chat->message}}" width="150" />
                    <p>{{$chat->caption}}</p>
                    @else
                    {{$chat->message}}
                    @endif
                  </div>
                  <!-- /.direct-chat-text -->
                </div>
                <!-- /.direct-chat-msg -->
                @else
                <!-- Message. Default to the left -->
                <div class="direct-chat-msg">
                  <div class="direct-chat-info clearfix">
                    <span class="direct-chat-timestamp pull-left">{{ date('d M, h:i a',strtotime($chat->timestamp.'+7hours')) }} </span>
                  </div>
                  <!-- /.direct-chat-info -->
                  <img class="direct-chat-img" src="{{ $client->photo == '' ? '/images/user-default.png' : $client->photo}}" alt="message user image">

                  <!-- /.direct-chat-img -->

                  <div class="direct-chat-text pull-left">
                    @if($chat->type == 'image')
                    <img src="{{$chat->message}}" width="150" />
                    <p>{{$chat->caption}}</p>
                    @else
                    {{$chat->message}}
                    @endif
                  </div>
                  <!-- /.direct-chat-text -->
                </div>
                <!-- /.direct-chat-msg -->
                @endif
                @endforeach

              </div>
              <!--/.direct-chat-messages-->

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <p class="text-right"><small>Only Leads owner able to chat with this Client</small></p>
            </div>
            <!-- /.box-footer-->

            <!--/.direct-chat -->
          </div>
          <!-- /.col -->
        </div>

        <div class="tab-pane" id="settings">
          <form class="form-horizontal">
            <div class="form-group">
              <label for="inputName" class="col-sm-2 control-label">Name</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="name" placeholder="Name" value="{{ $client->name }}" readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Tanggal Lahir</label>
              <div class="col-sm-10">
                <input type="date" class="form-control" name="dob" value="{{ $client->dob }}" readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Alamat</label>
              <div class="col-sm-10">
                <textarea class="form-control" name="address" placeholder="Alamat" readonly>{{ $client->address }}</textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Kota</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="kota" placeholder="Kota" value="{{ $client->kota }}" readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">KTP</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="ktp" placeholder="No KTP" value="{{ $client->ktp }}" readonly>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label">Telepon</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="phone" placeholder="Telepon" value="{{ $client->phone }}" readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">WhatsApp</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="whatsapp" placeholder="WhatsApp" value="{{ $client->whatsapp }}" readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">LINE</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="line" placeholder="LINE" value="{{ $client->line }}" readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Telegram</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="telegram" placeholder="Telegram" value="{{ $client->telegram }}" readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Facebook</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="facebook" placeholder="Facebook" value="{{ $client->facebook }}" readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Google</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="google" placeholder="Google" value="{{ $client->google }}" readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Twitter</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="twitter" placeholder="Twitter" value="{{ $client->twitter }}" readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">LinkedIn</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="linkedin" placeholder="LinkedIn" value="{{ $client->linkedin }}" readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Instagram</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="instagram" placeholder="Instagram" value="{{ $client->instagram }}" readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Status</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="status" placeholder="Status" value="{{ $client->status }}" readonly>
              </div>
            </div>
          </form>
        </div>
        <!-- /.tab-pane -->
        <!-- /.tab-pane -->
        <div class="tab-pane" id="email">

          <form action="{{route('postEmail')}}" class="form-horizontal" method="post">
            <div class="form-group">
              <label class="col-sm-2 control-label">Title</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="title" placeholder="Title" required>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">

              </div>
            </div>
            <textarea class="textarea" name="content" placeholder="Place some text here" style="width: 100%; height: 400px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required></textarea>

            <div class="form-group">
              <label class="col-sm-2 control-label">From</label>
              <div class="col-sm-4">

                <input type="text" name="senderName" class="form-control" value="{{CRUDBooster::myName()}}" readonly>

              </div>

              <div class="col-sm-6">
                <div class="input-group">
                  <input type="text" name="senderName" class="form-control" required>
                  <span class="input-group-addon">@smartmaxco.co.id</span>
                </div>
              </div>
            </div>
            <div class="form-group">
              {{ csrf_field() }}
              <input type="hidden" name="clientid" value="{{$client->id}}">
              <input type="hidden" name="recepientName" value="{{$client->name}}">
              <input type="hidden" name="recepientEmail" value="{{$client->email}}">
              <div class="col-sm-12">
                <input type="submit" name="submit" value="SEND" class="btn btn-primary btn-block">
              </div>
            </div>
          </form>

        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- /.nav-tabs-custom -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->



@endsection

@section("jsonpage")
<script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('.textarea').wysihtml5()
  })
</script>
@endsection