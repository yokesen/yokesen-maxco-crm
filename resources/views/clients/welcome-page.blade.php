@extends("crudbooster::admin_template")
@section('callcss')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<link rel="stylesheet" href="{{url('/')}}/plugin/jvectormap/jquery-jvectormap-1.2.2.css">
@endsection
@section("content")
<div class="row">
  <div class="col-md-12 col-sm-12">
    @foreach($announcements as $announcement)
    <a href="{{ route('PengumumanDetail', ['id' => Crypt::encrypt($announcement->id)]) }}">
      <div class="callout callout-{{ $announcement->jenis }}">
        <h4>Pengumuman! <span class="pull-right"><i class="fa fa-clock-o"></i> {{ $announcement->created_at->diffForHumans() }}</span></h4>
        <p>{{ $announcement->pengumuman }} <br> </p>
      </div>
    </a>

    @if($loop->last)
    <div class="center-block text-center" style="margin-bottom: 20px">
      <a href="{{ route('Pengumuman') }}" class="btn btn-primary">View All Announcement</a>
    </div>
    @endif
    @endforeach
  </div>
</div>



<div class="row">

  <div class="col-md-6">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Follow Up Today</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body" style="min-height:400px!important;">
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
              <tr>
                <th>Nama</th>
                <th>Schedulle</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

            @if($todays)
              @foreach ($todays as $m => $isi)
                @if(!empty($isi) && $m <8)
              <tr>
                <td><a href="{{route('ClientDetail',$isi->client_id)}}">{{$isi->name}}</a></td>
                <td><span class="label label-primary">{{$isi->next_fu}}</span></td>
                <td><a href="{{route('ClientDetail',$isi->client_id)}}" class="label label-primary">Today</a></td>
                <td> <a href="{{route('ClientDetail',$isi->client_id)}}" class="label label-primary"> Follow Up Now</a> </td>
              </tr>
                @endif
              @endforeach
            @endif
            </tbody>
          </table>
        </div>
        <!-- /.table-responsive -->
      </div>

    </div>
    <!-- /.box -->
  </div>


  <div class="col-md-6">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Late Follow Up</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body" style="min-height:400px!important;">
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
              <tr>
                <th>Nama</th>
                <th>Schedulle</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            @if($lates)
              @foreach ($lates as $n => $isi)
                @if(!empty($isi) && $n <8)
              <tr>

                <td><a href="{{route('ClientDetail',$isi->client_id)}}">{{$isi->name}}</a></td>
                <td><span class="label label-danger">{{$isi->next_fu}}</span></td>
                <td><a href="{{route('ClientDetail',$isi->client_id)}}" class="label label-danger">Late</a></td>
                <td> <a href="{{route('ClientDetail',$isi->client_id)}}" class="label label-danger"> Follow Up Now</a> </td>
              </tr>
                @endif
              @endforeach
            @endif
            </tbody>
          </table>
        </div>
        <!-- /.table-responsive -->
      </div>

    </div>
    <!-- /.box -->
  </div>



</div>

<div class="row">
  @foreach($campaigns as $isi)
  <div class="col-md-3">
    <!-- Widget: user widget style 1 -->
    <div class="box box-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header bg-black" style="background: url('{{url('/')}}/{{$isi->media}}') no-repeat center center; background-size:cover;">
      </div>
      <div class="widget-user-image">

      </div>
      <div class="box-footer">
          <p>{{$isi->title}}</p>
        <div class="row">

          <!-- /.col -->
          <div class="col-sm-6 border-right">
            <div class="description-block">
              <button data-clipboard-demo data-clipboard-text="{{ $campaign->url . '?ref=' . CRUDBooster::myID() . '&so=direct' . '&campaign=' . $campaign->id }}" class="btn btn-primary btn-block">Copy Link</button>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          <div class="col-sm-6">
            <div class="description-block">
              <a href="{{route('ToolsShare',$isi->id)}}" class="btn btn-warning btn-block">Share Now</a>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
    </div>
    <!-- /.widget-user -->
  </div>
  @endforeach

</div>


@endsection

@section('callcss')
<link rel="stylesheet" href="{{ url('css/custom.css') }}">
@endsection


@section('jsonpage')
<script src="{{url('/')}}/plugin/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

<script src="{{url('/')}}/plugin/jvectormap/src/map-object.js"></script>
<script src="{{url('/')}}/plugin/jvectormap/src/region.js"></script>
<script src="{{url('/')}}/plugin/jvectormap/src/marker.js"></script>

<script src="{{url('/')}}/plugin/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
<script>

window.addEventListener('resize', function(){
    // console.log('this resize');
    // console.log('-->',$('.team-member').height());
});

$(function(){
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $.post("{{route('apivisitorreport')}}", {
        parent:{{CRUDBooster::myID()}}
      }, function(data, status) {

        if(status==="success"){
          var dataMap = data.reduce(function(arr=[],item,index){
            arr.push(item._count);
            return arr;
          },[]);

          function getMaxOfArray(numArray) {
            return Math.max.apply(null, numArray);
          }

          var dtMax = getMaxOfArray(dataMap);
          var markers=[];
          var rMap = [2,3,4,5,6];
          data.forEach(element => {
            var perc = (element._count / dtMax) * 100;
            var r = 2;
            if(perc > 81 || perc === 81 ){
              r = 7;
            }else if(perc > 61 || perc === 61){
              r = 6;
            }else if(perc > 41 || perc === 41){
              r = 5;
            }else if(perc > 21 || perc === 21){
              r = 4;
            }else{
              r = 3;
            }

            markers.push({ latLng: [element.Lat,element.Lng], name: element.cityName,style:{
              // fill: 'yellow',
              r
            }});
          });
          $('#world-map-markers').vectorMap({
              map              : 'world_mill_en',
              normalizeFunction: 'polynomial',
              // focusOn: {
              //   x: 1.5,
              //   y: 1.5,
              //   scale: 3,
              //   animate: true
              // },
              hoverOpacity     : 0.7,
              hoverColor       : false,
              backgroundColor  : 'transparent',
              regionStyle      : {
                initial      : {
                  fill            : 'rgba(210, 214, 222, 1)',
                  'fill-opacity'  : 1,
                  stroke          : 'none',
                  'stroke-width'  : 0,
                  'stroke-opacity': 1
                },
                hover        : {
                  'fill-opacity': 0.7,
                  cursor        : 'pointer'
                },
                selected     : {
                  fill: 'yellow'
                },
                selectedHover: {}
              },
              // selectedRegions:['ID'],
              markerStyle      : {
                initial: {
                  fill  : '#00a65a',
                  stroke: '#111'
                }
              },
              markers:markers,
              onRegionLabelShow: function(event, label, code) {
                // console.log('label',label);
                // $(label).append($("<br/>"));
                // $(label).append($("<span/>", {
                //     'class': 'population',
                //     'html': 'Populație: ' + parseInt(Math.random() * (1000000 - 1000) + 1000)
                // }));
              },
              onRegionTipShow: function(event, label, code) {
                console.log('label',label);
              }

            });
        }
      });
      $.post("{{route('apiclientgraph')}}", {
          parent:{{CRUDBooster::myID()}}
        }, function(data, status) {
          if(status==="success"){
            var line = new Morris.Line({
                element          : 'line-chart',
                resize           : true,
                data             :data,
                xkey             : 'y',
                ykeys            : ['item1'],
                labels           : ['Item 1'],
                lineColors       : ['#efefef'],
                lineWidth        : 2,
                hideHover        : 'auto',
                gridTextColor    : '#fff',
                gridStrokeWidth  : 0.4,
                pointSize        : 4,
                pointStrokeColors: ['#efefef'],
                gridLineColor    : '#efefef',
                gridTextFamily   : 'Open Sans',
                gridTextSize     : 10
              });

          }
        });
});
</script>
@endsection
