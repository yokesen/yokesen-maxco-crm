@extends("crudbooster::admin_template")
@section("content")
  <div class="row">
    <div class="col-md-12 col-sm-12">
      @forelse($announcements as $announcement)
        <a href="{{ route('PengumumanDetail', ['id' => Crypt::encrypt($announcement->id)]) }}">
          <div class="callout callout-{{ $announcement->jenis }}">
            <h4>Pengumuman! <span class="pull-right">{{ $announcement->created_at->diffForHumans() }}</span></h4>
            <p>{{ $announcement->pengumuman }} <br> </p>
          </div>
        </a>
      @empty
        <div class="callout callout-danger">
          <p>There is no announcement.</p>
        </div>
      @endforelse
    </div>
  </div>
@endsection
