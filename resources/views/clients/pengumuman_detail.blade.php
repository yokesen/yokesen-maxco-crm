@extends("crudbooster::admin_template")
@section("content")
  <div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="callout callout-{{ $announcement->jenis }}">
          <h4>Pengumuman! <span class="pull-right"><i class="fa fa-clock-o"></i> {{ date('d M Y', strtotime($announcement->created_at)) }}</span></h4>
          <p>{{ $announcement->pengumuman }} <br> </p>
        </div>
        <div class="well">
          <legend>Detail <span class="pull-right"><i class="fa fa-user"></i> {{ $announcement->req_by }}</span></legend>
          <p>{!! $announcement->content !!}</p>
        </div>
    </div>
  </div>
@endsection
