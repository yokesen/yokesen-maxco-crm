@extends("crudbooster::admin_template")
@section("content")
  @if(CRUDBooster::me()->level == 1)
  <section class="content">
    <div class="row">
      <div class="col-md-4 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-blue"><i class="ion ion-social-facebook"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Facebook</span>
            <span class="info-box-number">{{ $facebook->count }} <small>Users</small></span>
          </div>
        </div>
      </div>

      <div class="col-md-4 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-teal"><i class="ion ion-social-twitter"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Twitter</span>
            <span class="info-box-number">{{ $twitter->count }} <small>Users</small></span>
          </div>
        </div>
      </div>

      <div class="col-md-4 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-light-blue"><i class="ion ion-ios-chatboxes"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Messenger</span>
            <span class="info-box-number">{{ $messenger->count }} <small>Users</small></span>
          </div>
        </div>
      </div>

      <div class="col-md-4 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-aqua"><i class="fa fa-paper-plane"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Telegram</span>
            <span class="info-box-number">{{ $telegram->count }} <small>Users</small></span>
          </div>
        </div>
      </div>

      <div class="col-md-4 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-green"><i class="fa fa-weixin"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">We Chat</span>
            <span class="info-box-number">{{ $wechat->count }} <small>Users</small></span>
          </div>
        </div>
      </div>

      <div class="col-md-4 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-green"><i class="ion ion-social-whatsapp"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Whatsapp</span>
            <span class="info-box-number">{{ $whatsapp->count }} <small>Users</small></span>
          </div>
        </div>
      </div>

      <div class="col-md-4 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-grey"><i class="ion ion-social-linkedin"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">LinkedIn</span>
            <span class="info-box-number">{{ $linkedin->count }} <small>Users</small></span>
          </div>
        </div>
      </div>

      <div class="col-md-4 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-green"><i class="fa fa-comment-o"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Line</span>
            <span class="info-box-number">{{ $line->count }} <small>Users</small></span>
          </div>
        </div>
      </div>

      <div class="col-md-4 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-green"><i class="fa fa-envelope"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Email Campaign</span>
            <span class="info-box-number">{{ $email->count }} <small>Users</small></span>
          </div>
        </div>
      </div>

      <div class="col-md-4 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-green"><i class="fa fa-sitemap"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Direct Link</span>
            <span class="info-box-number">{{ $direct->count }} <small>Users</small></span>
          </div>
        </div>
      </div>

      <div class="col-md-4 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-green"><i class="fa fa-globe"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Organics</span>
            <span class="info-box-number">{{ $organics->count }} <small>Users</small></span>
          </div>
        </div>
      </div>

      <div class="col-md-4 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-green"><i class="fa fa-thumb-tack"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Ads</span>
            <span class="info-box-number">{{ $ads->count }} <small>Users</small></span>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-4 col-xs-12">
        <div class="small-box bg-gray">
          <div class="inner">
            <h3>{{ $registered->count }}</h3>
            <p>Leads</p>
          </div>
          <div class="icon">
            <i class="fa fa-phone"></i>
          </div>
          <a href="{{url('/')}}/crm/clients_po_from_level_7" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-xs-12">
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>{{ $contact->count }}</h3>
            <p>Contact</p>
          </div>
          <div class="icon">
            <i class="ion ion-android-contacts"></i>
          </div>
          <a href="{{url('/')}}/crm/client_my_contacts" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-xs-12">
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3>{{ $potential->count }}</h3>
            <p>Potential</p>
          </div>
          <div class="icon">
            <i class="fa fa-money"></i>
          </div>
          <a href="{{url('/')}}/crm/clients_my_potentials" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

    </div>
      <div class="row justify-content-center">
        <div class="col-lg-2 col-xs-12">
        </div>
        <div class="col-lg-4 col-xs-12">
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ $win->count }}</h3>
              <p>Win</p>
            </div>
            <div class="icon">
              <i class="fa fa-trophy"></i>
            </div>
            <a href="{{url('/')}}/crm/clients_my_wins" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-4 col-xs-12">
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{ $lose->count }}</h3>
              <p>Lose</p>
            </div>
            <div class="icon">
              <i class="fa fa-sign-out"></i>
            </div>
            <a href="{{url('/')}}/crm/clients74" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
  </section>
  @else
  <section class="content">
  <div class="row">
    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-blue"><i class="ion ion-social-facebook"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Facebook</span>
          <span class="info-box-number">{{ $facebook->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-teal"><i class="ion ion-social-twitter"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Twitter</span>
          <span class="info-box-number">{{ $twitter->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-light-blue"><i class="ion ion-ios-chatboxes"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Messenger</span>
          <span class="info-box-number">{{ $messenger->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-paper-plane"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Telegram</span>
          <span class="info-box-number">{{ $telegram->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-weixin"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">We Chat</span>
          <span class="info-box-number">{{ $wechat->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="ion ion-social-whatsapp"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Whatsapp</span>
          <span class="info-box-number">{{ $whatsapp->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-grey"><i class="ion ion-social-linkedin"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">LinkedIn</span>
          <span class="info-box-number">{{ $linkedin->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-comment-o"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Line</span>
          <span class="info-box-number">{{ $line->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-envelope"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Email Campaign</span>
          <span class="info-box-number">{{ $email->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-sitemap"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Direct Link</span>
          <span class="info-box-number">{{ $direct->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-globe"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Organics</span>
          <span class="info-box-number">{{ $organics->count }} <small>Users</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-thumb-tack"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Ads</span>
          <span class="info-box-number">{{ $ads->count }} <small>Users</small></span>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-4 col-xs-12">
      <div class="small-box bg-gray">
        <div class="inner">
          <h3>{{ $registered->count }}</h3>
          <p>Leads</p>
        </div>
        <div class="icon">
          <i class="fa fa-phone"></i>
        </div>
        <a href="{{url('/')}}/crm/clients_po_from_level_7" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-lg-4 col-xs-12">
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{ $contact->count }}</h3>
          <p>Contact</p>
        </div>
        <div class="icon">
          <i class="ion ion-android-contacts"></i>
        </div>
        <a href="{{url('/')}}/crm/client_my_contacts" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-lg-4 col-xs-12">
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{ $potential->count }}</h3>
          <p>Potential</p>
        </div>
        <div class="icon">
          <i class="fa fa-money"></i>
        </div>
        <a href="{{url('/')}}/crm/clients_my_potentials" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

  </div>
    <div class="row justify-content-center">
      <div class="col-lg-2 col-xs-12">
      </div>
      <div class="col-lg-4 col-xs-12">
        <div class="small-box bg-green">
          <div class="inner">
            <h3>{{ $win->count }}</h3>
            <p>Win</p>
          </div>
          <div class="icon">
            <i class="fa fa-trophy"></i>
          </div>
          <a href="{{url('/')}}/crm/clients_my_wins" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-xs-12">
        <div class="small-box bg-red">
          <div class="inner">
            <h3>{{ $lose->count }}</h3>
            <p>Lose</p>
          </div>
          <div class="icon">
            <i class="fa fa-sign-out"></i>
          </div>
          <a href="{{url('/')}}/crm/clients74" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>

</section>
  @endif
@endsection

@section('jsonpage')

@endsection
