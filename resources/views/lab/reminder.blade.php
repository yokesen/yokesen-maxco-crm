@extends("crudbooster::admin_template")

@section('cssonpage')

@endsection

@section("content")
<section>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Late Follow Up</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Nama</th>
                    <th>Last Follow Up</th>
                    <th>Schedulle</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach ($data as $isi)
                      <tr>
                        <td><a href="#">{{$isi->name}}</a></td>
                        <td>{{$isi->activity}}</td>
                        <td><span class="label label-danger">{{$isi->next_fu}}</span></td>
                        <td>Late</td>
                        <td> <a href="#" class="btn btn-danger btn-block"> Follow Up</a> </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
              <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
  </div>
</section>
@endsection

@section('calljs')

@endsection

@section("jsonpage")

@endsection
