@extends("crudbooster::admin_template")

@section('cssonpage')
  <style media="screen">
  .td-structure{
    width: 50px!important;
  }

  .td-conversion{
    width: 100px!important;
  }
  </style>

@endsection

@section("content")
  <section>
    <div class="row">
      <div class="col-md-12">
        <div class="box table-responsive no-padding">
          <div class="box-header with-border">
            <h3 class="box-title">My Team</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body no-padding">
            <table class="table table-striped">
            @foreach($child1 as $c1)

                <tr class="danger">
                  <th colspan="4">Team Structure</th>
                  <th class="text-right">Leads <br> <small> <i>min : 300</i> </small> </th>
                  <th class="text-right">Contacts <br> <small> <i>min : 40%</i> </small> </th>
                  <th class="text-right">Potentials <br> <small> <i>min : 10%</i> </small> </th>
                  <th class="text-right">Winnings <br> <small> <i>min : 2%</i> </small> </th>
                  <th class="text-right">Losings <br> <small> <i>max : 50%</i> </small> </th>
                </tr>
                <?php
                $child2 = DB::table('users_jakarta')->where('parent',$c1->id)->get();
                $leads = DB::table('clients')->where('parent',$c1->id)->count();
                $contacts = DB::table('clients')->where('parent',$c1->id)->where('status','contact')->count();
                $potentials = DB::table('clients')->where('parent',$c1->id)->where('status','potential')->count();
                $win = DB::table('clients')->where('parent',$c1->id)->where('status','win')->count();
                $lose = DB::table('clients')->where('parent',$c1->id)->where('status','lose')->count();
                if($leads > 0){
                  $ctl = $contacts/$leads;
                  $ptc = $potentials/$leads;
                  $wtp = $win/$leads;
                  $ltl = $lose/$leads;
                }else{
                  $ctl = 0;
                  $ptc = 0;
                  $wtp = 0;
                  $lose = 0;
                }

                ?>
                <tr>
                  <td class="td-structure" colspan="4"><a href="{{ route('TeamMarketingDetail',$c1->id)}}">{{$c1->name}}</a></td>
                  <td class="td-conversion text-right"><a href="/crm/clients_po_from_level_7?ref={{ $c1->id }}" class="text-{{$leads > 299 ? 'success' : 'danger'}}">{{$leads}}</a></td>
                  <td class="td-conversion text-right"><a href="/crm/client_my_contacts?ref={{ $c1->id }}" class="text-{{$ctl > 0.399 ? 'success' : 'danger'}}">{{$contacts}}</a></td>
                  <td class="td-conversion text-right"><a href="/crm/clients_my_potentials?ref={{ $c1->id }}" class="text-{{$ptc > 0.099 ? 'success' : 'danger'}}">{{$potentials}}</a></td>
                  <td class="td-conversion text-right"><a href="/crm/clients_my_wins?ref={{ $c1->id }}" class="text-{{$wtp > 0.019 ? 'success' : 'danger'}}">{{$win}}</a></td>
                  <td class="td-conversion text-right"><a href="/crm/clients_my_wins?ref={{ $c1->id }}" class="text-{{$ltl < 0.50 ? 'success' : 'danger'}}">{{$lose}}</a></td>
                </tr>

                @foreach ($child2 as $c2)
                  <?php
                  $child3 = DB::table('users_jakarta')->where('parent',$c2->id)->get();
                  $leads = DB::table('clients')->where('parent',$c2->id)->count();
                  $contacts = DB::table('clients')->where('parent',$c2->id)->where('status','contact')->count();
                  $potentials = DB::table('clients')->where('parent',$c2->id)->where('status','potential')->count();
                  $win = DB::table('clients')->where('parent',$c2->id)->where('status','win')->count();
                  $lose = DB::table('clients')->where('parent',$c2->id)->where('status','lose')->count();
                  if($leads > 0){
                    $ctl = $contacts/$leads;
                    $ptc = $potentials/$leads;
                    $wtp = $win/$leads;
                    $ltl = $lose/$leads;
                  }else{
                    $ctl = 0;
                    $ptc = 0;
                    $wtp = 0;
                    $lose = 0;
                  }

                  ?>
                  <tr>
                    <td class="td-structure" ></td>
                    <td class="td-structure" colspan="3"><a href="{{ route('TeamMarketingDetail',$c2->id)}}">{{$c2->name}}</a></td>
                    <td class="td-conversion text-right"><a href="/crm/clients_po_from_level_7?ref={{ $c2->id }}" class="text-{{$leads > 299 ? 'success' : 'danger'}}">{{$leads}}</a></td>
                    <td class="td-conversion text-right"><a href="/crm/client_my_contacts?ref={{ $c2->id }}" class="text-{{$ctl > 0.399 ? 'success' : 'danger'}}">{{$contacts}}</a></td>
                    <td class="td-conversion text-right"><a href="/crm/clients_my_potentials?ref={{ $c2->id }}" class="text-{{$ptc > 0.099 ? 'success' : 'danger'}}">{{$potentials}}</a></td>
                    <td class="td-conversion text-right"><a href="/crm/clients_my_wins?ref={{ $c2->id }}" class="text-{{$wtp > 0.019 ? 'success' : 'danger'}}">{{$win}}</a></td>
                    <td class="td-conversion text-right"><a href="/crm/clients_my_wins?ref={{ $c2->id }}" class="text-{{$ltl < 0.50 ? 'success' : 'danger'}}">{{$lose}}</a></td>
                  </tr>

                  @foreach ($child3 as $c3)
                    <?php
                    $child4 = DB::table('users_jakarta')->where('parent',$c3->id)->get();
                    $leads = DB::table('clients')->where('parent',$c3->id)->count();
                    $contacts = DB::table('clients')->where('parent',$c3->id)->where('status','contact')->count();
                    $potentials = DB::table('clients')->where('parent',$c3->id)->where('status','potential')->count();
                    $win = DB::table('clients')->where('parent',$c3->id)->where('status','win')->count();
                    $lose = DB::table('clients')->where('parent',$c3->id)->where('status','lose')->count();
                    if($leads > 0){
                      $ctl = $contacts/$leads;
                      $ptc = $potentials/$leads;
                      $wtp = $win/$leads;
                      $ltl = $lose/$leads;
                    }else{
                      $ctl = 0;
                      $ptc = 0;
                      $wtp = 0;
                      $lose = 0;
                    }

                    ?>
                    <tr>
                      <td class="td-structure" ></td>
                      <td class="td-structure" ></td>
                      <td class="td-structure" colspan="2"><a href="{{ route('TeamMarketingDetail',$c3->id)}}">{{$c3->name}}</a></td>
                      <td class="td-conversion text-right"><a href="/crm/clients_po_from_level_7?ref={{ $c3->id }}" class="text-{{$leads > 299 ? 'success' : 'danger'}}">{{$leads}}</a></td>
                      <td class="td-conversion text-right"><a href="/crm/client_my_contacts?ref={{ $c3->id }}" class="text-{{$ctl > 0.399 ? 'success' : 'danger'}}">{{$contacts}}</a></td>
                      <td class="td-conversion text-right"><a href="/crm/clients_my_potentials?ref={{ $c3->id }}" class="text-{{$ptc > 0.099 ? 'success' : 'danger'}}">{{$potentials}}</a></td>
                      <td class="td-conversion text-right"><a href="/crm/clients_my_wins?ref={{ $c3->id }}" class="text-{{$wtp > 0.019 ? 'success' : 'danger'}}">{{$win}}</a></td>
                      <td class="td-conversion text-right"><a href="/crm/clients_my_wins?ref={{ $c3->id }}" class="text-{{$ltl < 0.50 ? 'success' : 'danger'}}">{{$lose}}</a></td>
                    </tr>

                    @foreach ($child4 as $c4)
                      <?php

                      $leads = DB::table('clients')->where('parent',$c4->id)->count();
                      $contacts = DB::table('clients')->where('parent',$c4->id)->where('status','contact')->count();
                      $potentials = DB::table('clients')->where('parent',$c4->id)->where('status','potential')->count();
                      $win = DB::table('clients')->where('parent',$c4->id)->where('status','win')->count();
                      $lose = DB::table('clients')->where('parent',$c4->id)->where('status','lose')->count();
                      if($leads > 0){
                        $ctl = $contacts/$leads;
                        $ptc = $potentials/$leads;
                        $wtp = $win/$leads;
                        $ltl = $lose/$leads;
                      }else{
                        $ctl = 0;
                        $ptc = 0;
                        $wtp = 0;
                        $lose = 0;
                      }

                      ?>
                      <tr>
                        <td class="td-structure" ></td>
                        <td class="td-structure" ></td>
                        <td class="td-structure" ></td>
                        <td class="td-structure"><a href="{{ route('TeamMarketingDetail',$c4->id)}}">{{$c4->name}}</a></td>
                        <td class="td-conversion text-right"><a href="/crm/clients_po_from_level_7?ref={{ $c4->id }}" class="text-{{$leads > 299 ? 'success' : 'danger'}}">{{$leads}}</a></td>
                        <td class="td-conversion text-right"><a href="/crm/client_my_contacts?ref={{ $c4->id }}" class="text-{{$ctl > 0.399 ? 'success' : 'danger'}}">{{$contacts}}</a></td>
                        <td class="td-conversion text-right"><a href="/crm/clients_my_potentials?ref={{ $c4->id }}" class="text-{{$ptc > 0.099 ? 'success' : 'danger'}}">{{$potentials}}</a></td>
                        <td class="td-conversion text-right"><a href="/crm/clients_my_wins?ref={{ $c4->id }}" class="text-{{$wtp > 0.019 ? 'success' : 'danger'}}">{{$win}}</a></td>
                        <td class="td-conversion text-right"><a href="/crm/clients_my_wins?ref={{ $c4->id }}" class="text-{{$ltl < 0.50 ? 'success' : 'danger'}}">{{$lose}}</a></td>
                      </tr>
                    @endforeach
                  @endforeach
                @endforeach
              <tr>
                <td colspan="9"></td>
              </tr>
            @endforeach
          </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
  </section>
@endsection

@section('calljs')

@endsection

@section("jsonpage")

@endsection
