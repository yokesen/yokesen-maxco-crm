@extends("crudbooster::admin_template")
@section("content")

  <div class="input-group">
    <input type="text" name="message" placeholder="Type Message ..." class="form-control">
    <span class="input-group-btn">
      <button class="btn btn-primary btn-flat" ><i class="fa fa-file-video-o margin-r-5"></i></button>
    </span>
    <span class="input-group-btn">
      <button class="btn btn-primary btn-flat" data-toggle="modal" data-target="#whatspp-image"><i class="fa fa-file-image-o margin-r-5"></i></button>
    </span>
    <span class="input-group-btn">
      <button class="btn btn-warning btn-flat">Send</button>
    </span>
  </div>

  <div class="modal fade" id="whatspp-image">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Pilih Gambar</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="input-group">
                  <input type="text" name="keyword-image" placeholder="keyword" id="keywordImage" class="form-control">
                  <span class="input-group-btn">
                    <button class="btn btn-primary btn-flat" id="search-image" onclick="searchImage()" ><i class="fa fa-file-search-o margin-r-5"></i></button>
                  </span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12" id="image-list">

              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success btn-lg">Pilih</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


@endsection

@section('calljs')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
@endsection

@section("jsonpage")
  <script type="text/javascript">


    function searchImage(){
      var keyword = document.getElementById("keywordImage").value;
      $.ajax({
       type:"GET",
       url:"/cari/"+keyword,
       beforeSend: function()
        	            {
        	                $('.ajax-load').show();
        	            }
     })
     .done(function(data)
      {
        if(data.html == " "){
              $('.ajax-load').html("No more records found");
              return;
          }
          $('.ajax-load').hide();
          document.getElementById("image-list").innerHTML = data.html;
      })
      .fail(function(jqXHR, ajaxOptions, thrownError)
      {
            alert('server not responding...');
      });
    }


  </script>
@endsection
