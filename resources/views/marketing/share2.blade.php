@extends("crudbooster::admin_template")
@section("content")
  <div class="row">
    <div class="col-md-12 text-center">
      <h2><span class="label label-primary">{{$share->page}}</span></h2>
    </div>
  </div>
  <hr>

  <div class="row">
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-blue"><i class="fa fa-facebook"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Facebook </span>
          <br>
          <span class="info-box-text"><a target="_blank" href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=facebook&wid=94kf&url={{urlencode($share->url.'?ref='.CRUDBooster::myID().'&so=facebook&campaign='.$share->id)}}&title={{urlencode($share->title)}}&ate=AT-ra-59563471e59e31ba/-/-/59563e2e12e62024/21&frommenu=1&ips=1&uid=574f2829a5396ed9&description={{urlencode($share->desc)}}&screenshot={{urlencode('http://qyptech.com/images/200x200_'.$share->media)}}&uud=1&ct=1&pre=http%3A%2F%2Fqyptech.com%2Fhome&tt=0&captcha_provider=recaptcha2&pro=0" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=facebook&wid=94kf&url={{urlencode($share->url.'?ref='.CRUDBooster::myID().'&so=facebook&campaign='.$share->id)}}&title={{urlencode($share->title)}}&ate=AT-ra-59563471e59e31ba/-/-/59563e2e12e62024/21&frommenu=1&ips=1&uid=574f2829a5396ed9&description={{urlencode($share->desc)}}&screenshot={{urlencode('http://qyptech.com/images/200x200_'.$share->media)}}&uud=1&ct=1&pre=http%3A%2F%2Fqyptech.com%2Fhome&tt=0&captcha_provider=recaptcha2&pro=0', 'newwindow', 'width=600,height=500'); return false;"><i class="fa fa-facebook-official" aria-hidden="true"></i> Facebook</a></span>

        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>

    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-teal"><i class="fa fa-twitter"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Twitter </span>
          <br>
          <span class="info-box-text"><a href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=twitter&wid=94kf&url={{urlencode($share->url.'?ref='.CRUDBooster::myID().'&so=twitter&campaign='.$share->id)}}&title={{urlencode($share->title)}}&ate=AT-ra-59563471e59e31ba/-/-/59563e2e12e62024/24&frommenu=1&uid=574f2829a5396ed9&description={{urlencode($share->desc)}}&screenshot={{urlencode('http://qyptech.com/images/200x200_'.$share->media)}}&uud=1&ct=1&pre=http%3A%2F%2Fqyptech.com%2Fhome&tt=0&captcha_provider=recaptcha2&pro=0" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=twitter&wid=94kf&url={{urlencode($share->url.'?ref='.CRUDBooster::myID().'&so=twitter&campaign='.$share->id)}}&title={{urlencode($share->title)}}&ate=AT-ra-59563471e59e31ba/-/-/59563e2e12e62024/24&frommenu=1&uid=574f2829a5396ed9&description={{urlencode($share->desc)}}&screenshot={{urlencode('http://qyptech.com/images/200x200_'.$share->media)}}&uud=1&ct=1&pre=http%3A%2F%2Fqyptech.com%2Fhome&tt=0&captcha_provider=recaptcha2&pro=0', 'newwindow', 'width=600,height=500'); return false;"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter</a></span>

        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>

    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon box-messenger"><i class="ion ion-ios-chatboxes"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Messenger </span>
          <br>
          <span class="info-box-text"><a href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=messenger&wid=94kf&url={{urlencode($share->url.'?ref='.CRUDBooster::myID().'&so=messenger&campaign='.$share->id)}}&title={{urlencode($share->title)}}&ate=AT-ra-59563471e59e31ba/-/-/59563e2e12e62024/25&frommenu=1&uid=574f2829a5396ed9&description={{urlencode($share->desc)}}&screenshot={{urlencode('http://qyptech.com/images/200x200_'.$share->media)}}&uud=1&ct=1&pre=http%3A%2F%2Fqyptech.com%2Fhome&tt=0&captcha_provider=recaptcha2&pro=0" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=messenger&wid=94kf&url={{urlencode($share->url.'?ref='.CRUDBooster::myID().'&so=messenger&campaign='.$share->id)}}&title={{urlencode($share->title)}}&ate=AT-ra-59563471e59e31ba/-/-/59563e2e12e62024/25&frommenu=1&uid=574f2829a5396ed9&description={{urlencode($share->desc)}}&screenshot={{urlencode('http://qyptech.com/images/200x200_'.$share->media)}}&uud=1&ct=1&pre=http%3A%2F%2Fqyptech.com%2Fhome&tt=0&captcha_provider=recaptcha2&pro=0', 'newwindow', 'width=600,height=500'); return false;"><i class="fa fa-commenting" aria-hidden="true"></i> Messenger</a></span>

        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>

    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon box-skype"><i class="fa fa-skype"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Skype </span>
          <br>
          <span class="info-box-text"><a href="https://web.skype.com/share?url={{urlencode($share->url.'?ref='.CRUDBooster::myID().'&so=skype&campaign='.$share->id)}}.WVY-LnXzPMk.skype&lang=en&flow_id=59563e2e3ea939b6&source=AddThis" onclick="window.open('https://web.skype.com/share?url={{urlencode($share->url.'?ref='.CRUDBooster::myID().'&so=messenger&campaign='.$share->id)}}.WVY-LnXzPMk.skype&lang=en&flow_id=59563e2e3ea939b6&source=AddThis', 'newwindow', 'width=600,height=500'); return false;"><i class="fa fa-skype" aria-hidden="true"></i> Skype</a></span>

        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>

    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon box-telegram"><i class="fa fa-send"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Telegram </span>
          <br>
          <span class="info-box-text"><a href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=telegram&wid=94kf&url={{urlencode($share->url.'?ref='.CRUDBooster::myID().'&so=telegram&campaign='.$share->id)}}&title={{urlencode($share->title)}}&ate=AT-ra-59563471e59e31ba/-/-/59563e2e12e62024/32&frommenu=1&ips=1&uid=574f2829a5396ed9&description={{urlencode($share->desc)}}&screenshot={{urlencode('http://qyptech.com/images/200x200_'.$share->media)}}&uud=1&ct=1&pre=http%3A%2F%2Fqyptech.com%2Fhome&tt=0&captcha_provider=recaptcha2&pro=0" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=telegram&wid=94kf&url={{urlencode($share->url.'?ref='.CRUDBooster::myID().'&so=telegram&campaign='.$share->id)}}&title={{urlencode($share->title)}}&ate=AT-ra-59563471e59e31ba/-/-/59563e2e12e62024/32&frommenu=1&ips=1&uid=574f2829a5396ed9&description={{urlencode($share->desc)}}&screenshot={{urlencode('http://qyptech.com/images/200x200_'.$share->media)}}&uud=1&ct=1&pre=http%3A%2F%2Fqyptech.com%2Fhome&tt=0&captcha_provider=recaptcha2&pro=0', 'newwindow', 'width=600,height=500'); return false;"><i class="fa fa-paper-plane" aria-hidden="true"></i> Telegram</a></span>

        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>

    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon box-wechat"><i class="fa fa-wechat"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">WeChat </span>
          <br>
          <span class="info-box-text"><a href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=wechat&wid=94kf&url={{urlencode($share->url.'?ref='.CRUDBooster::myID().'&so=WeChat&campaign='.$share->id)}}&title={{urlencode($share->title)}}&ate=AT-ra-59563471e59e31ba/-/-/59563e2e12e62024/34&frommenu=1&uid=574f2829a5396ed9&description={{urlencode($share->desc)}}&screenshot={{urlencode('http://qyptech.com/images/200x200_'.$share->media)}}&uud=1&ct=1&pre=http%3A%2F%2Fqyptech.com%2Fhome&tt=0&captcha_provider=recaptcha2&pro=0" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=wechat&wid=94kf&url={{urlencode($share->url.'?ref='.CRUDBooster::myID().'&so=WeChat&campaign='.$share->id)}}&title={{urlencode($share->title)}}&ate=AT-ra-59563471e59e31ba/-/-/59563e2e12e62024/34&frommenu=1&uid=574f2829a5396ed9&description={{urlencode($share->desc)}}&screenshot={{urlencode('http://qyptech.com/images/200x200_'.$share->media)}}&uud=1&ct=1&pre=http%3A%2F%2Fqyptech.com%2Fhome&tt=0&captcha_provider=recaptcha2&pro=0', 'newwindow', 'width=600,height=500'); return false;"><i class="fa fa-weixin" aria-hidden="true"></i> WeChat</a></span>

        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>


    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon box-whatsapp"><i class="fa fa-whatsapp"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Whatsapp </span>
          <br>
          <span class="info-box-text"><a href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=whatsapp&wid=94kf&url={{urlencode($share->url.'?ref='.CRUDBooster::myID().'&so=Whatsapp&campaign='.$share->id)}}&title={{urlencode($share->title)}}&ate=AT-ra-59563471e59e31ba/-/-/59563e2e12e62024/19&frommenu=1&ips=1&uid=574f2829a5396ed9&description={{urlencode($share->desc)}}&screenshot={{urlencode('http://qyptech.com/images/200x200_'.$share->media)}}&uud=1&ct=1&pre=http%3A%2F%2Fqyptech.com%2Fhome&tt=0&captcha_provider=recaptcha2&pro=0" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=whatsapp&wid=94kf&url={{urlencode($share->url.'?ref='.CRUDBooster::myID().'&so=Whatsapp&campaign='.$share->id)}}&title={{urlencode($share->title)}}&ate=AT-ra-59563471e59e31ba/-/-/59563e2e12e62024/19&frommenu=1&ips=1&uid=574f2829a5396ed9&description={{urlencode($share->desc)}}&screenshot={{urlencode('http://qyptech.com/images/200x200_'.$share->media)}}&uud=1&ct=1&pre=http%3A%2F%2Fqyptech.com%2Fhome&tt=0&captcha_provider=recaptcha2&pro=0', 'newwindow', 'width=600,height=500'); return false;"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</a></span>

        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>


    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon box-line"><i class="fa fa-comment-o"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Line </span>
          <br>
          <span class="info-box-text"><a href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=lineme&wid=94kf&url={{urlencode($share->url.'?ref='.CRUDBooster::myID().'&so=Line&campaign='.$share->id)}}&title={{urlencode($share->title)}}&ate=AT-ra-59563471e59e31ba/-/-/59563e2e12e62024/20&frommenu=1&uid=574f2829a5396ed9&description={{urlencode($share->desc)}}&screenshot={{urlencode('http://qyptech.com/images/200x200_'.$share->media)}}&uud=1&ct=1&pre=http%3A%2F%2Fqyptech.com%2Fhome&tt=0&captcha_provider=recaptcha2&pro=0" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=lineme&wid=94kf&url={{urlencode($share->url.'?ref='.CRUDBooster::myID().'&so=Line&campaign='.$share->id)}}&title={{urlencode($share->title)}}&ate=AT-ra-59563471e59e31ba/-/-/59563e2e12e62024/20&frommenu=1&uid=574f2829a5396ed9&description={{urlencode($share->desc)}}&screenshot={{urlencode('http://qyptech.com/images/200x200_'.$share->media)}}&uud=1&ct=1&pre=http%3A%2F%2Fqyptech.com%2Fhome&tt=0&captcha_provider=recaptcha2&pro=0', 'newwindow', 'width=600,height=500'); return false;">
            <i class="fa fa-comment-o" aria-hidden="true"></i> Line</a>
          </span>

        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>

    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon box-linkedin"><i class="ion ion-social-linkedin"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">LinkedIn </span>
          <br>
          <span class="info-box-text"><a href="window.open('https://www.linkedin.com/shareArticle?mini=true&url={{urlencode($share->url.'?ref='.CRUDBooster::myID().'&so=Linkedin&campaign='.$share->id)}}&title={{urlencode($share->title)}}&ro=false&summary=&source="
          onclick="window.open('https://www.linkedin.com/shareArticle?mini=true&url={{urlencode($share->url.'?ref='.CRUDBooster::myID().'&so=Linkedin&campaign='.$share->id)}}&title={{urlencode($share->title)}}&ro=false&summary=&source=', 'newwindow', 'width=600,height=500'); return false;">
            <i class="ion ion-social-linkedin" aria-hidden="true"></i> LinkedIn</a>
          </span>

        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>

    <!--<div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon box-share-link"><i class="fa fa-link"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Copy to Clipboard </span>
          <br>
          <span class="info-box-text">
            <a data-clipboard-demo data-clipboard-text="{{ $share->url.'?ref='.CRUDBooster::myID().'&so=direct&campaign='.$share->id }}">
              <i class="fa fa-link" aria-hidden="true"></i> Copy to Clipboard
            </a>
          </span>

        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>-->

  </div>

  <div class="row">
    <div class="col-md-7">
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Email Campaign</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <iframe src="{{ url('crm/email-template') }}/{{$share->id}}" style="border:none;width:100%;min-height:900px;"></iframe>
        </div>
      </div>
    </div>

    <div class="col-md-5">
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">Send To :</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              @if( !$leads->isEmpty() )
              <form action="#" method="post">
                @foreach($leads as $lead)
                <div class="form-group">
                  <label><input type="checkbox" name="recipients[]" value="{{$lead->email}}">  {{$lead->email}}</label>
                </div>
                @endforeach
                {{ $leads->links() }}
                <div class="form-group clearfix">
                  {{ csrf_field()}}
                  <div class="col-md-8 col-md-offset-2">
                    <input type="submit" class="button btn btn-lg btn-primary btn-block" value="SEND">
                  </div>
                </div>
              </form>
              @else
              <div class="callout callout-warning">
                <h4>Perhatian</h4>
                <p>Anda belum punya Leads</p>
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <hr>



@endsection

@section('jsonpage')
<script>
$("document").ready(function() {
  $(".copy-link").click(function() {
    $(".share-link").focus();
    $(".share-link").select();
    document.execCommand("copy");
  });
});
</script>
@endsection

@section('cssonpage')
<style>
  .box-linkedin{
    background:#0077b5;
    color:#fff;
  }

  .box-wechat{
    background:#7bb32e;
    color:#fff;
  }

  .box-line{
    background:#00c300;
    color:#fff;
  }

  .box-whatsapp{
    background:#25d366;
    color:#fff;
  }

  .box-telegram{
    background:#0088cc;
    color:#fff;
  }

  .box-skype{
    background:#00aff0;
    color:#fff;
  }

  .box-messenger{
    background:#0084ff;
    color:#fff;
  }

  .box-share-link{
    background:#e67e22;
    color:#fff;
  }

</style>
@endsection
