@extends("crudbooster::admin_template")

@section('cssonpage')
  <style>
  .box-body p img{
    width : 100% !important;
  }
  </style>
@endsection

@section("content")
  <div class="row">
    <div class="col-md-8">
        <!-- Widget: user widget style 1 -->
        <div class="box box-widget widget-user">
          <!-- Add the bg color to the header using any of the bg-* classes -->
          <div class="widget-user-header bg-black" style="background: url('/images/slank.jpeg') center center;">
            <h3 class="widget-user-username">{{ $user->name }}</h3>
            <h5 class="widget-user-desc">{{ $user->longname }}</h5>
          </div>
          <div class="widget-user-image">
            <img class="img-circle" src="/images/emma.jpg" alt="User Avatar">
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-sm-4 border-right">
                <div class="description-block">
                  <h5 class="description-header">3,200</h5>
                  <span class="description-text">TOTAL LEADS</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-4 border-right">
                <div class="description-block">
                  <h5 class="description-header">13,000</h5>
                  <span class="description-text">CONTACTABLES</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-4">
                <div class="description-block">
                  <h5 class="description-header">35</h5>
                  <span class="description-text">POTENTIALS</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
        </div>
        <!-- /.widget-user -->
      </div>
      <!-- /.col -->
  </div>
  @foreach($laporan as $mkt)

    <div class="row">
        <div class="col-md-8">
          <!-- Box Comment -->
          <div class="box box-widget">
            <div class="box-header with-border">
              <div class="user-block">
                <img class="img-circle" src="/images/user-default.png" alt="User Image">
                <span class="username"><a href="#">{{ $mkt->komentator->name }}</a></span>
                <span class="description">Shared publicly - {{ $mkt->created_at}}</span>
              </div>
              <!-- /.user-block -->
              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
                  <i class="fa fa-circle-o"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <!-- post text -->
              {!!$mkt->content!!}
              {{-- <button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button> --}}
              {{-- <button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button> --}}
              <span class="pull-right text-muted">{{ count($mkt->comments) }} comments</span>
            </div>
            <div class="box-footer box-comments">
              @foreach($mkt->comments as $comment)
              <div class="box-comment">
                <img class="img-circle img-sm" src="/images/user-default.png" alt="User Image">
                <div class="comment-text">
                  <span class="username">
                    {{ $comment->komentator->name }}
                    <span class="text-muted pull-right">{{ $comment->created_at }}</span>
                  </span>
                  {!! $comment->content !!}
                </div>
              </div>
              @endforeach

            </div>
            <!-- /.box-footer -->
            <div class="box-footer">
              <form action="{{route('postComment')}}" method="post">
                {{ csrf_field() }}
                <div class="row">
                  <div class="col-md-10 col-sm-10 col-xs-10">
                    <img class="img-responsive img-circle img-sm" src="/images/user-default.png" alt="Alt Text">
                    <!-- .img-push is used to add margin to elements next to floating images -->
                    <div class="img-push">
                      <input type="text" class="form-control input-sm" placeholder="You comment here" name="comment" row="2">
                      <input type="hidden" class="form-control input-sm" name="post_id" value="{{ $mkt->id }}">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-2">
                    <div class="form-group">
                      <input type="submit" class="form-control" value="Reply">
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->
@endforeach
      @endsection
