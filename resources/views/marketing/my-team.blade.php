@extends("crudbooster::admin_template")

@section('cssonpage')
  <style media="screen">
  .td-structure{
    width: 50px!important;
  }

  .td-conversion{
    width: 100px!important;
  }
  </style>

@endsection

<?php
date_default_timezone_set("Asia/Jakarta");
$month = date('Y-m-d');
$start = strtotime('2019-05-01');
$date = date('Y-m-d H:i:s');
$today = strtotime($date);
$days = ($today - $start)/(60*60*24);

?>

@section("content")
  <section>
    <div class="row">
      <div class="col-md-12">
        <div class="box table-responsive no-padding">
          <div class="box-header with-border">
            <div class="row">
              <div class="col-md-8">
                <h3 class="box-title text-left">My Team</h3>
              </div>
              <div class="col-md-2">
                <button type="button" class="btn btn-default dropdown-toggle right btn-block" data-toggle="dropdown">Branch
                    <span class="fa fa-caret-down"></span></button>
                  <ul class="dropdown-menu">

                    @foreach($stores as $store)
                      <li><a href="{{route('TeamToko',$store->head)}}">{{$store->nama_toko}}</a></li>
                    @endforeach

                  </ul>

              </div>
              <div class="col-md-2">
                <button type="button" class="btn btn-default dropdown-toggle right btn-block" data-toggle="dropdown">Month
                    <span class="fa fa-caret-down"></span></button>
                  <ul class="dropdown-menu">
                    <li><a href="{{route('TeamIndex')}}">Year to Date</a></li>
                    <li><a href="{{route('TeamIndexMonth',strtotime($month))}}">{{date('M Y',strtotime($month))}}</a></li>
                    <li><a href="{{route('TeamIndexMonth',strtotime($month.'-1 month'))}}">{{date('M Y',strtotime($month.'-1 month'))}}</a></li>
                    <li><a href="{{route('TeamIndexMonth',strtotime($month.'-2 month'))}}">{{date('M Y',strtotime($month.'-2 month'))}}</a></li>
                    <li><a href="{{route('TeamIndexMonth',strtotime($month.'-3 month'))}}">{{date('M Y',strtotime($month.'-3 month'))}}</a></li>
                    <li><a href="{{route('TeamIndexMonth',strtotime($month.'-4 month'))}}">{{date('M Y',strtotime($month.'-4 month'))}}</a></li>
                    <li><a href="{{route('TeamIndexMonth',strtotime($month.'-5 month'))}}">{{date('M Y',strtotime($month.'-5 month'))}}</a></li>

                  </ul>

              </div>
            </div>
            <div class="row" style="margin-top:10px;">
              <div class="col-md-6 text-right">
                search by date :
              </div>
              <form class="form-group" action="{{route('TeamIndexCustom')}}" method="post">
                <div class="col-md-2">
                  <input type="date" name="date_start" class="form-control">
                </div>
                <div class="col-md-2">
                  <input type="date" name="date_end" class="form-control">
                </div>
                {{ csrf_field() }}
                <div class="col-md-2">
                  <input type="submit" name="submit" class="form-control" value="SEARCH">
                </div>
              </form>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body no-padding">
            <table class="table table-striped">
            @foreach($child1 as $c1)

                <tr class="primary">
                  <th colspan="4">Team Structure</th>
                  <th class="text-right">Today F-up <br> <small> <i>min:20</i> </small> </th>
                  <th class="text-right">Daily F-up <br> <small> <i>avg:20</i> </small> </th>
                  <th class="text-right">Late F-up <br> <small> <i>Max:0</i> </small> </th>
                  <th class="text-right">Data/Leads <br> <small> <i>min:300</i> </small> </th>
                  <th class="text-right">Contacts <br> <small> <i>min:40%</i> </small> </th>
                  <th class="text-right">Potentials <br> <small> <i>min:10%</i> </small> </th>
                  <th class="text-right">Winnings <br> <small> <i>min:2%</i> </small> </th>
                  <th class="text-right">Losings <br> <small> <i>max:50%</i> </small> </th>
                  <th class="text-right">LastSeen <br> <small> <i>max:2days</i> </small> </th>
                </tr>
                <?php
                $today = date('Y-m-d H:i:s');
                $child2 = DB::table('users_jakarta')->whereNull('deleted_at')->where('parent',$c1->id)->where('status','active')->get();
                $leads = DB::table('clients')->where('parent',$c1->id)->count();
                $contacts = DB::table('clients')->where('parent',$c1->id)->where('status','contact')->count();
                $potentials = DB::table('clients')->where('parent',$c1->id)->where('status','potential')->count();
                $win = DB::table('clients')->where('parent',$c1->id)->where('status','win')->count();
                $lose = DB::table('clients')->where('parent',$c1->id)->where('status','lose')->count();
                $list = DB::table('activities')->distinct()->select('client_id')->where('user_id',$c1->id)->count();
                $fup = $list/$days;
                $daftar = DB::table('activities')->distinct()->select('client_id')->where('user_id',$c1->id)->get();
                $lup = 0;
                $tup = 0;
                $nup = 0;
                foreach($daftar as $isi){
                  $client = DB::table('activities')->join('clients','clients.id','activities.client_id')->where('activities.client_id',$isi->client_id)->select('activities.next_fu','activities.client_id','clients.name','clients.status','clients.parent')->first();
                  if($client->parent == $c1->id){
                    if($client->status != 'lose' && $client->status != 'win'){
                      if($client->next_fu < $date){
                        $lup++;
                      }elseif($client->next_fu == $date){
                        $tup++;
                      }else{
                        $nup++;
                      }
                    }
                  }
                }
                $dlist = DB::table('activities')->distinct()->select('client_id')->where('user_id',$c1->id)->whereDate('created_at',date('Y-m-d'))->count();

                if($leads > 0){
                  $ctl = $contacts/$leads;
                  $ptc = $potentials/$leads;
                  $wtp = $win/$leads;
                  $ltl = $lose/$leads;
                }else{
                  $ctl = 0;
                  $ptc = 0;
                  $wtp = 0;
                  $lose = 0;
                }
                $last_login = DB::table('cms_logs')->where('id_cms_users',$c1->id)->orderby('id','desc')->first();
                $date = date('Y-m-d H:i:s');
                $count_time = strtotime($date) - strtotime($last_login->created_at);
                $count_hour = $count_time/(60*60);
                $count_day = ($count_hour/24);
                if($count_day > 1 && $count_day < 2.01){
                  $last = 'yesterday';
                }elseif($count_day > 0 && $count_day < 1){
                  $last = date('H:i',strtotime($last_login->created_at));
                }elseif($count_day > 2 && $count_day < 1000 ){
                  $last = number_format($count_day,0,'','').' days';
                }else{
                  $last = 'Never';
                }
                ?>
                <tr>
                  <td class="td-structure" colspan="4"><a href="{{ route('TeamMarketingDetail',$c1->id)}}">{{$c1->name}}</a></td>
                  <td class="td-conversion text-right"><a href="#" class="text-{{$dlist > 10 ? 'success' : 'danger'}}">{{$dlist}}</a></td>
                  <td class="td-conversion text-right"><a href="#" class="text-{{$fup > 19 ? 'success' : 'danger'}}">{{number_format($fup,1,".",",")}}</a></td>
                  <td class="td-conversion text-right"><a href="#" class="text-{{$lup < 1 ? 'success' : 'danger'}}">{{$lup}}</a></td>
                  <td class="td-conversion text-right"><a href="/crm/clients_po_from_level_7?ref={{ $c1->id }}" class="text-{{$leads > 299 ? 'success' : 'danger'}}">{{$leads}}</a></td>
                  <td class="td-conversion text-right"><a href="/crm/client_my_contacts?ref={{ $c1->id }}" class="text-{{$ctl > 0.399 ? 'success' : 'danger'}}">{{$contacts}}</a></td>
                  <td class="td-conversion text-right"><a href="/crm/clients_my_potentials?ref={{ $c1->id }}" class="text-{{$ptc > 0.099 ? 'success' : 'danger'}}">{{$potentials}}</a></td>
                  <td class="td-conversion text-right"><a href="/crm/clients_my_wins?ref={{ $c1->id }}" class="text-{{$wtp > 0.019 ? 'success' : 'danger'}}">{{$win}}</a></td>
                  <td class="td-conversion text-right"><a href="/crm/clients74?ref={{ $c1->id }}" class="text-{{$ltl < 0.50 ? 'success' : 'danger'}}">{{$lose}}</a></td>
                  <td class="td-conversion text-right"><a href="{{ route('TeamMarketingDetail',$c1->id)}}" class="text-{{$count_day < 2 ? 'success' : 'danger'}}">{{$last}}</a></td>
                </tr>

                @foreach ($child2 as $c2)
                  <?php
                  $child3 = DB::table('users_jakarta')->whereNull('deleted_at')->where('parent',$c2->id)->where('status','active')->get();
                  $leads = DB::table('clients')->where('parent',$c2->id)->count();
                  $contacts = DB::table('clients')->where('parent',$c2->id)->where('status','contact')->count();
                  $potentials = DB::table('clients')->where('parent',$c2->id)->where('status','potential')->count();
                  $win = DB::table('clients')->where('parent',$c2->id)->where('status','win')->count();
                  $lose = DB::table('clients')->where('parent',$c2->id)->where('status','lose')->count();
                  $list = DB::table('activities')->distinct()->select('client_id')->where('user_id',$c2->id)->count();
                  $fup = $list/$days;
                  $daftar = DB::table('activities')->distinct()->select('client_id')->where('user_id',$c2->id)->get();
                  $lup = 0;
                  $tup = 0;
                  $nup = 0;
                  foreach($daftar as $isi){
                    $client = DB::table('activities')->join('clients','clients.id','activities.client_id')->where('activities.client_id',$isi->client_id)->select('activities.next_fu','activities.client_id','clients.name','clients.status','clients.parent')->first();
                    if($client->parent == $c2->id){
                      if($client->status != 'lose' && $client->status != 'win'){
                        if($client->next_fu < $date){
                          $lup++;
                        }elseif($client->next_fu == $date){
                          $tup++;
                        }else{
                          $nup++;
                        }
                      }
                    }
                  }
                  $dlist = DB::table('activities')->distinct()->select('client_id')->where('user_id',$c2->id)->whereDate('created_at',date('Y-m-d'))->count();
                  if($leads > 0){
                    $ctl = $contacts/$leads;
                    $ptc = $potentials/$leads;
                    $wtp = $win/$leads;
                    $ltl = $lose/$leads;
                  }else{
                    $ctl = 0;
                    $ptc = 0;
                    $wtp = 0;
                    $lose = 0;
                  }
                  $last_login = DB::table('cms_logs')->where('id_cms_users',$c2->id)->orderby('id','desc')->first();
                  $date = date('Y-m-d H:i:s');
                  $count_time = strtotime($date) - strtotime($last_login->created_at);
                  $count_hour = $count_time/(60*60);
                  $count_day = ($count_hour/24);
                  if($count_day > 1 && $count_day < 2.01){
                    $last = 'yesterday';
                  }elseif($count_day > 0 && $count_day < 1){
                    $last = date('H:i',strtotime($last_login->created_at));
                  }elseif($count_day > 2 && $count_day < 1000 ){
                    $last = number_format($count_day,0,'','').' days';
                  }else{
                    $last = 'Never';
                  }
                  ?>
                  <tr class="success">
                    <td class="td-structure" ></td>
                    <td class="td-structure" colspan="3"><a href="{{ route('TeamMarketingDetail',$c2->id)}}">{{$c2->name}}</a></td>
                    <td class="td-conversion text-right"><a href="#" class="text-{{$dlist > 10 ? 'success' : 'danger'}}">{{$dlist}}</a></td>
                    <td class="td-conversion text-right"><a href="#" class="text-{{$fup > 19 ? 'success' : 'danger'}}">{{number_format($fup,1,".",",")}}</a></td>
                    <td class="td-conversion text-right"><a href="#" class="text-{{$lup < 1 ? 'success' : 'danger'}}">{{$lup}}</a></td>
                    <td class="td-conversion text-right"><a href="/crm/clients_po_from_level_7?ref={{ $c2->id }}" class="text-{{$leads > 299 ? 'success' : 'danger'}}">{{$leads}}</a></td>
                    <td class="td-conversion text-right"><a href="/crm/client_my_contacts?ref={{ $c2->id }}" class="text-{{$ctl > 0.399 ? 'success' : 'danger'}}">{{$contacts}}</a></td>
                    <td class="td-conversion text-right"><a href="/crm/clients_my_potentials?ref={{ $c2->id }}" class="text-{{$ptc > 0.099 ? 'success' : 'danger'}}">{{$potentials}}</a></td>
                    <td class="td-conversion text-right"><a href="/crm/clients_my_wins?ref={{ $c2->id }}" class="text-{{$wtp > 0.019 ? 'success' : 'danger'}}">{{$win}}</a></td>
                    <td class="td-conversion text-right"><a href="/crm/clients74?ref={{ $c2->id }}" class="text-{{$ltl < 0.50 ? 'success' : 'danger'}}">{{$lose}}</a></td>
                    <td class="td-conversion text-right"><a href="{{ route('TeamMarketingDetail',$c2->id)}}" class="text-{{$count_day < 2 ? 'success' : 'danger'}}">{{$last}}</a></td>
                  </tr>

                  @foreach ($child3 as $c3)
                    <?php
                    $child4 = DB::table('users_jakarta')->whereNull('deleted_at')->where('parent',$c3->id)->where('status','active')->get();
                    $leads = DB::table('clients')->where('parent',$c3->id)->count();
                    $contacts = DB::table('clients')->where('parent',$c3->id)->where('status','contact')->count();
                    $potentials = DB::table('clients')->where('parent',$c3->id)->where('status','potential')->count();
                    $win = DB::table('clients')->where('parent',$c3->id)->where('status','win')->count();
                    $lose = DB::table('clients')->where('parent',$c3->id)->where('status','lose')->count();
                    $list = DB::table('activities')->distinct()->select('client_id')->where('user_id',$c3->id)->count();
                    $fup = $list/$days;
                    $daftar = DB::table('activities')->distinct()->select('client_id')->where('user_id',$c3->id)->get();
                    $lup = 0;
                    $tup = 0;
                    $nup = 0;
                    foreach($daftar as $isi){
                      $client = DB::table('activities')->join('clients','clients.id','activities.client_id')->where('activities.client_id',$isi->client_id)->select('activities.next_fu','activities.client_id','clients.name','clients.status','clients.parent')->first();
                      if($client->parent == $c3->id){
                        if($client->status != 'lose' && $client->status != 'win'){
                          if($client->next_fu < $date){
                            $lup++;
                          }elseif($client->next_fu == $date){
                            $tup++;
                          }else{
                            $nup++;
                          }
                        }
                      }
                    }
                    $dlist = DB::table('activities')->distinct()->select('client_id')->where('user_id',$c3->id)->whereDate('created_at',date('Y-m-d'))->count();
                    if($leads > 0){
                      $ctl = $contacts/$leads;
                      $ptc = $potentials/$leads;
                      $wtp = $win/$leads;
                      $ltl = $lose/$leads;
                    }else{
                      $ctl = 0;
                      $ptc = 0;
                      $wtp = 0;
                      $lose = 0;
                    }
                    $last_login = DB::table('cms_logs')->where('id_cms_users',$c3->id)->orderby('id','desc')->first();
                    $date = date('Y-m-d H:i:s');
                    $count_time = strtotime($date) - strtotime($last_login->created_at);
                    $count_hour = $count_time/(60*60);
                    $count_day = ($count_hour/24);
                    if($count_day > 1 && $count_day < 2.01){
                      $last = 'yesterday';
                    }elseif($count_day > 0 && $count_day < 1){
                      $last = date('H:i',strtotime($last_login->created_at));
                    }elseif($count_day > 2 && $count_day < 1000 ){
                      $last = number_format($count_day,0,'','').' days';
                    }else{
                      $last = 'Never';
                    }
                    ?>
                    <tr>
                      <td class="td-structure" ></td>
                      <td class="td-structure" ></td>
                      <td class="td-structure" colspan="2"><a href="{{ route('TeamMarketingDetail',$c3->id)}}">{{$c3->name}}</a></td>
                      <td class="td-conversion text-right"><a href="#" class="text-{{$dlist > 10 ? 'success' : 'danger'}}">{{$dlist}}</a></td>
                      <td class="td-conversion text-right"><a href="#" class="text-{{$fup > 19 ? 'success' : 'danger'}}">{{number_format($fup,1,".",",")}}</a></td>
                      <td class="td-conversion text-right"><a href="#" class="text-{{$lup < 1 ? 'success' : 'danger'}}">{{$lup}}</a></td>
                      <td class="td-conversion text-right"><a href="/crm/clients_po_from_level_7?ref={{ $c3->id }}" class="text-{{$leads > 299 ? 'success' : 'danger'}}">{{$leads}}</a></td>
                      <td class="td-conversion text-right"><a href="/crm/client_my_contacts?ref={{ $c3->id }}" class="text-{{$ctl > 0.399 ? 'success' : 'danger'}}">{{$contacts}}</a></td>
                      <td class="td-conversion text-right"><a href="/crm/clients_my_potentials?ref={{ $c3->id }}" class="text-{{$ptc > 0.099 ? 'success' : 'danger'}}">{{$potentials}}</a></td>
                      <td class="td-conversion text-right"><a href="/crm/clients_my_wins?ref={{ $c3->id }}" class="text-{{$wtp > 0.019 ? 'success' : 'danger'}}">{{$win}}</a></td>
                      <td class="td-conversion text-right"><a href="/crm/clients74?ref={{ $c3->id }}" class="text-{{$ltl < 0.50 ? 'success' : 'danger'}}">{{$lose}}</a></td>
                      <td class="td-conversion text-right"><a href="{{ route('TeamMarketingDetail',$c3->id)}}" class="text-{{$count_day < 2 ? 'success' : 'danger'}}">{{$last}}</a></td>
                    </tr>

                    @foreach ($child4 as $c4)
                      <?php

                      $leads = DB::table('clients')->where('parent',$c4->id)->count();
                      $contacts = DB::table('clients')->where('parent',$c4->id)->where('status','contact')->count();
                      $potentials = DB::table('clients')->where('parent',$c4->id)->where('status','potential')->count();
                      $win = DB::table('clients')->where('parent',$c4->id)->where('status','win')->count();
                      $lose = DB::table('clients')->where('parent',$c4->id)->where('status','lose')->count();
                      $list = DB::table('activities')->distinct()->select('client_id')->where('user_id',$c4->id)->count();
                      $fup = $list/$days;
                      $daftar = DB::table('activities')->distinct()->select('client_id')->where('user_id',$c4->id)->get();
                      $lup = 0;
                      $tup = 0;
                      $nup = 0;
                      foreach($daftar as $isi){
                        $client = DB::table('activities')->join('clients','clients.id','activities.client_id')->where('activities.client_id',$isi->client_id)->select('activities.next_fu','activities.client_id','clients.name','clients.status','clients.parent')->first();
                        if($client->parent == $c4->id){
                          if($client->status != 'lose' && $client->status != 'win'){
                            if($client->next_fu < $date){
                              $lup++;
                            }elseif($client->next_fu == $date){
                              $tup++;
                            }else{
                              $nup++;
                            }
                          }
                        }
                      }
                      $dlist = DB::table('activities')->distinct()->select('client_id')->where('user_id',$c4->id)->whereDate('created_at',date('Y-m-d'))->count();
                      if($leads > 0){
                        $ctl = $contacts/$leads;
                        $ptc = $potentials/$leads;
                        $wtp = $win/$leads;
                        $ltl = $lose/$leads;
                      }else{
                        $ctl = 0;
                        $ptc = 0;
                        $wtp = 0;
                        $lose = 0;
                      }
                      $last_login = DB::table('cms_logs')->where('id_cms_users',$c4->id)->orderby('id','desc')->first();
                      $date = date('Y-m-d H:i:s');
                      $count_time = strtotime($date) - strtotime($last_login->created_at);
                      $count_hour = $count_time/(60*60);
                      $count_day = ($count_hour/24);
                      if($count_day > 1 && $count_day < 2.01){
                        $last = 'yesterday';
                      }elseif($count_day > 0 && $count_day < 1){
                        $last = date('H:i',strtotime($last_login->created_at));
                      }elseif($count_day > 2 && $count_day < 1000 ){
                        $last = number_format($count_day,0,'','').' days';
                      }else{
                        $last = 'Never';
                      }
                      ?>
                      <tr>
                        <td class="td-structure" ></td>
                        <td class="td-structure" ></td>
                        <td class="td-structure" ></td>
                        <td class="td-structure"><a href="{{ route('TeamMarketingDetail',$c4->id)}}">{{$c4->name}}</a></td>
                        <td class="td-conversion text-right"><a href="#" class="text-{{$dlist > 10 ? 'success' : 'danger'}}">{{$dlist}}</a></td>
                        <td class="td-conversion text-right"><a href="#" class="text-{{$fup > 19 ? 'success' : 'danger'}}">{{number_format($fup,1,".",",")}}</a></td>
                        <td class="td-conversion text-right"><a href="#" class="text-{{$lup < 1 ? 'success' : 'danger'}}">{{$lup}}</a></td>
                        <td class="td-conversion text-right"><a href="/crm/clients_po_from_level_7?ref={{ $c4->id }}" class="text-{{$leads > 299 ? 'success' : 'danger'}}">{{$leads}}</a></td>
                        <td class="td-conversion text-right"><a href="/crm/client_my_contacts?ref={{ $c4->id }}" class="text-{{$ctl > 0.399 ? 'success' : 'danger'}}">{{$contacts}}</a></td>
                        <td class="td-conversion text-right"><a href="/crm/clients_my_potentials?ref={{ $c4->id }}" class="text-{{$ptc > 0.099 ? 'success' : 'danger'}}">{{$potentials}}</a></td>
                        <td class="td-conversion text-right"><a href="/crm/clients_my_wins?ref={{ $c4->id }}" class="text-{{$wtp > 0.019 ? 'success' : 'danger'}}">{{$win}}</a></td>
                        <td class="td-conversion text-right"><a href="/crm/clients74?ref={{ $c4->id }}" class="text-{{$ltl < 0.50 ? 'success' : 'danger'}}">{{$lose}}</a></td>
                        <td class="td-conversion text-right"><a href="{{ route('TeamMarketingDetail',$c4->id)}}" class="text-{{$count_day < 2 ? 'success' : 'danger'}}">{{$last}}</a></td>
                      </tr>
                    @endforeach
                  @endforeach
                @endforeach
              <tr>
                <td colspan="10"></td>
              </tr>
            @endforeach
          </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
  </section>
@endsection

@section('calljs')

@endsection

@section("jsonpage")

@endsection
