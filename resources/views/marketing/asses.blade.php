@extends("crudbooster::admin_template")

@section('cssonpage')

@endsection

@section("content")


  <div class="row">
    <div class="col-md-6">
      <!-- Box Comment -->
      <div class="box box-widget">
        <div class="box-header with-border">
          <div class="user-block">
            <img class="img-circle" src="/images/user-default.png" alt="User Image">
            <span class="username"><a href="#">DEST</a></span>
            <span class="description">Shared publicly - 14-09-2017 16:32</span>
          </div>
          <!-- /.user-block -->
          <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
              <i class="fa fa-circle-o"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <video width="100%" controls>
              <source src="#" type="video/mp4">
              </video>

              <p>Pengenalan Cara Menggunakan Software</p>
            </div>
            <!-- /.box-body -->

            <!-- /.box-footer -->
            <div class="box-footer">


              <a href="#" class="btn btn-default btn-xs" download><i class="fa fa-download"></i> Download</a>
              <span class="pull-right text-muted">0 likes</span>
            </div>
            <!-- /.box-footer -->
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Assesment</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="form-group">
                <p>Pertanyaan no 1 : ini adalah pertanyaan nomor 1 berkaitan dengan video disamping</p>
                  <div class="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                      Jawaban a
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                      Jawaban b
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                      Jawaban c
                    </label>
                  </div>
                </div>

                <div class="form-group">
                  <p>Pertanyaan no 2 : ini adalah pertanyaan nomor 2 berkaitan dengan video disamping</p>
                    <div class="radio">
                      <label>
                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                        Jawaban a
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                        Jawaban b
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                        Jawaban c
                      </label>
                    </div>
                  </div>

                  <div class="form-group">
                    <p>Pertanyaan no 3 : ini adalah pertanyaan nomor 3 berkaitan dengan video disamping</p>
                      <div class="radio">
                        <label>
                          <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                          Jawaban a
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                          Jawaban b
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                          Jawaban c
                        </label>
                      </div>
                    </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="javascript:void(0)" class="btn btn-sm btn-danger btn-flat pull-left">Submit Assesment</a>
              <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View Next Assesment</a>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>


      @endsection

      @section('jsonpage')

      @endsection
