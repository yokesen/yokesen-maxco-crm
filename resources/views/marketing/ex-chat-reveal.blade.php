@foreach($chats as $n => $chat)
  @if($chat->fromme == '1')

    <!-- Message to the right -->
    <div class="direct-chat-msg right">
      <div class="direct-chat-info clearfix">
        <span class="direct-chat-name pull-right">&nbsp{{ CRUDBooster::myName() }}&nbsp</span>
        <span> </span>
        <span class="direct-chat-timestamp pull-right">{{ date('d M, h:i a',strtotime($chat->timestamp.'+7hours')) }}</span>
      </div>
      <!-- /.direct-chat-info -->
      <img class="direct-chat-img" src="{{ CRUDBooster::myPhoto() }}" alt="message user image">
      <!-- /.direct-chat-img -->
      <div class="direct-chat-text pull-right">
        @if($chat->type == 'image')
          <img src="{{$chat->message}}" width="150"/>
          <p>{{$chat->caption}}</p>
        @else
          {{$chat->message}}
        @endif
      </div>
      <!-- /.direct-chat-text -->
    </div>
    <!-- /.direct-chat-msg -->
  @else
    <!-- Message. Default to the left -->
    <div class="direct-chat-msg">
      <div class="direct-chat-info clearfix">
        <span class="direct-chat-name pull-left">&nbsp{{ $client->name }}&nbsp</span>
        <span> </span>
        <span class="direct-chat-timestamp pull-left">{{ date('d M, h:i a',strtotime($chat->timestamp.'+7hours')) }} </span>
      </div>
      <!-- /.direct-chat-info -->
      <img class="direct-chat-img" src="{{ $client->photo == '' ? '/images/user-default.png' : $client->photo}}" alt="message user image">

      <!-- /.direct-chat-img -->

      <div class="direct-chat-text pull-left">
        @if($chat->type == 'image')
          <img src="{{$chat->message}}" width="150"/>
          <p>{{$chat->caption}}</p>
        @else
          {{$chat->message}}
        @endif
      </div>
      <!-- /.direct-chat-text -->
    </div>
    <!-- /.direct-chat-msg -->
  @endif
@endforeach
