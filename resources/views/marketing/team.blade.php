@extends("crudbooster::admin_template")
@section("calljs")
  <script src="../js/treeview-bootstrap.js"></script>
  <script src="../js/treeview-jquery.js"></script>
@endsection

@section("callcss")
  <link href="../css/treeview-bootstrap.css" rel="stylesheet">
@endsection

@section("cssonpage")
<style type="text/css" id="treeview6-style"> .treeview .list-group-item{cursor:pointer}.treeview
span.indent{margin-left:10px;margin-right:10px}.treeview
span.icon{width:12px;margin-right:5px}.treeview .node-disabled{color:silver;cursor:not-allowed}.node-treeview6{color:#428bca;}.node-treeview6:not(.node-disabled):hover{background-color:#F5F5F5;}
</style>
@endsection

@section("content")
<div class="row">
  <div class="col-md-10">
    <h2>My Team</h2>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div id="treeview6" class="treeview">
    </div>
  </div>
</div>


@endsection

@section('cssonpage')
<style>
  #treeview6.treeview ul.list-group li.list-group-item span.badge{
    background:green !important;
  }
</style>
@endsection

@section('jsonpage')
<script type="text/javascript">

  		$(function() {

        var defaultData = [
        @if($ones)
          @foreach($ones as $satu)
            @if($satu['parent'] == $user_id)
            {
              text : '{{$satu['name']}}',
              tags : ['{{$satu['fu']}}','{{$satu['client']}}','{{$satu['total']}}'],
              href : '{{ config('app.url') }}/mkt-detail/{{$satu['user_id']}}',
              nodes : [
                @foreach($twos as $dua)
                  @if($dua['parent'] == $satu['user_id'])
                  {
                    text : '{{$dua['name']}}',
                    tags : ['{{$dua['fu']}}','{{$dua['client']}}','{{$dua['total']}}'],
                    href : '{{ config('app.url') }}/mkt-detail/{{$dua['user_id']}}',
                    nodes : [
                      @foreach($threes as $tiga)
                        @if($tiga['parent'] == $dua['user_id'])
                        {
                          text : '{{$tiga['name']}}',
                          tags : ['{{$tiga['fu']}}','{{$tiga['client']}}','{{$tiga['total']}}'],
                          href : '{{ config('app.url') }}/mkt-detail/{{$tiga['user_id']}}',
                          nodes : [
                            @foreach($fours as $empat)
                              @if($empat['parent'] == $tiga['user_id'])
                              {
                                text : '{{$empat['name']}}',
                                tags : ['{{$empat['fu']}}','{{$empat['client']}}','{{$empat['total']}}'],
                                href : '{{ config('app.url') }}/mkt-detail/{{$empat['user_id']}}',
                                nodes : [
                                  @foreach($fives as $lima)
                                    @if($lima['parent'] == $empat['user_id'])
                                    {
                                      text : '{{$lima['name']}}',
                                      tags : ['{{$lima['fu']}}','{{$lima['client']}}','{{$lima['total']}}'],
                                      href : '{{ config('app.url') }}/mkt-detail/{{$lima['user_id']}}',
                                      nodes : [
                                        @foreach($sixes as $enam)
                                          @if($enam['parent'] == $lima['user_id'])
                                          {
                                            text : '{{$enam['name']}}',
                                            tags : ['{{$enam['fu']}}','{{$enam['client']}}','{{$enam['total']}}'],
                                            href : '{{ config('app.url') }}/mkt-detail/{{$enam['user_id']}}',
                                          },
                                          @endif
                                        @endforeach
                                      ]
                                    },
                                    @endif
                                  @endforeach
                                ]
                              },
                              @endif
                            @endforeach
                          ]
                        },
                        @endif
                      @endforeach
                    ]
                  },
                  @endif
                @endforeach
              ]
            },
            @endif
          @endforeach
        @endif
        ];



        $('#treeview6').treeview({
          levels: 1,
          color: "#428bca",
          expandIcon: "glyphicon glyphicon-stop",
          collapseIcon: "glyphicon glyphicon-unchecked",
          nodeIcon: "glyphicon glyphicon-user",
          showTags: true,
          data: defaultData,
          enableLinks: true,
        });

  		});
  	</script>
@endsection
