@extends("crudbooster::admin_template")

@section('cssonpage')

@endsection

@section("content")

        <!-- Info boxes -->
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-aqua"><i class="fa fa-pie-chart"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Hours /year</span>
                <span class="info-box-number">40<small>%</small></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-file-video-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Moduls</span>
                <span class="info-box-number">13</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix visible-sm-block"></div>

          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-green"><i class="fa fa-check-square-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Assesment</span>
                <span class="info-box-number">9</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="fa fa-file-text-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Modul to go</span>
                <span class="info-box-number">37</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
          <div class="col-md-8">
            <!-- TABLE: LATEST ORDERS -->
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Learning Moduls Management</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="table-responsive">
                  <table class="table no-margin">
                    <thead>
                    <tr>
                      <th>Modul ID</th>
                      <th>Modul Name</th>
                      <th>Status</th>
                      <th>Scoring</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td><a href="#">OR9842</a></td>
                      <td>How to handle objection</td>
                      <td><span class="label label-success">Passed</span></td>
                      <td>
                        <div class="sparkbar" data-color="#00a65a" data-height="20">90,80,90,-70,61,-83,63</div>
                      </td>
                    </tr>
                    <tr>
                      <td><a href="#">OR1848</a></td>
                      <td>Failure to Succeed</td>
                      <td><span class="label label-warning">Watched</span></td>
                      <td>
                        <div class="sparkbar" data-color="#f39c12" data-height="20">85,87,90,-20</div>
                      </td>
                    </tr>
                    <tr>
                      <td><a href="#">OR7429</a></td>
                      <td>New Deposito Schema</td>
                      <td><span class="label label-danger">Warning</span></td>
                      <td>
                        <div class="sparkbar" data-color="#f56954" data-height="20">0</div>
                      </td>
                    </tr>
                    <tr>
                      <td><a href="#">OR7429</a></td>
                      <td>Call Center Attitude</td>
                      <td><span class="label label-info">on Progress</span></td>
                      <td>
                        <div class="sparkbar" data-color="#00c0ef" data-height="20">0</div>
                      </td>
                    </tr>
                    <tr>
                      <td><a href="#">OR1848</a></td>
                      <td>Greeting to Potentials</td>
                      <td><span class="label label-warning">Watched</span></td>
                      <td>
                        <div class="sparkbar" data-color="#f39c12" data-height="20">90,80,-90</div>
                      </td>
                    </tr>
                    <tr>
                      <td><a href="#">OR7429</a></td>
                      <td>October promotion : Deposito</td>
                      <td><span class="label label-success">Passed</span></td>
                      <td>
                        <div class="sparkbar" data-color="#f56954" data-height="20">90,95,60,70,-20,70,90</div>
                      </td>
                    </tr>
                    <tr>
                      <td><a href="#">OR9842</a></td>
                      <td>Company Profile Q4 2018</td>
                      <td><span class="label label-success">Passed</span></td>
                      <td>
                        <div class="sparkbar" data-color="#00a65a" data-height="20">90,80,90,-70,61,-83,63</div>
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.box-body -->
              <div class="box-footer clearfix">
                <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Next Moduls</a>
                <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Moduls</a>
              </div>
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->
          </div>
          <div class="col-md-4">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Goal Completion</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">


                <div class="progress-group">
                  <span class="progress-text">Company Profile</span>
                  <span class="progress-number"><b>16</b>/20</span>

                  <div class="progress sm">
                    <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Self Development</span>
                  <span class="progress-number"><b>31</b>/40</span>

                  <div class="progress sm">
                    <div class="progress-bar progress-bar-red" style="width: 77%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Product Knowledge</span>
                  <span class="progress-number"><b>48</b>/80</span>

                  <div class="progress sm">
                    <div class="progress-bar progress-bar-green" style="width: 60%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">SOP/SLA/Service Excellence</span>
                  <span class="progress-number"><b>25</b>/50</span>

                  <div class="progress sm">
                    <div class="progress-bar progress-bar-yellow" style="width: 50%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Submit Schedulles</a>
              <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Schedulles</a>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>


@endsection

@section('jsonpage')

@endsection
