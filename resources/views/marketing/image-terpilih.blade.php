<div class="row">
  <div class="col-sm-8 col-sm-offset-2" style="padding:20px!important; text-align:center!important;">
    <?php
      $img = unserialize($data->productImages);
    ?>
    <img src="{{url('/')}}/uploads/{{$img[0]}}" alt="" width="100%">
  </div>
</div>
<div class="row">
  <div class="col-sm-8 col-sm-offset-2">
    <form action="{{route('whatsappImageSend',[$client,$data->id])}}" method="post">
      <div class="input-group">
        <input type="text" name="caption" placeholder="Type Caption ..." class="form-control">
        <span class="input-group-btn">
          <button type="submit" class="btn btn-success btn-flat">Send</button>
        </span>
          {{csrf_field()}}
      </div>
  </div>
</div>
