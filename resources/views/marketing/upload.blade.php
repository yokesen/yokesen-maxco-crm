@extends("crudbooster::admin_template")

@section("content")
  <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Import Data Email</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('uploadProcess')}}" method="POST" enctype="multipart/form-data">
              <div class="box-body">

                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" id="email" name="email">

                  <p class="help-block">Lihat contoh file excel disini | <a href="/excel/contoh_import_email.xlsx">contohfile</a></p>
                </div>

              </div>
              <!-- /.box-body -->
              {{ csrf_field() }}
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      @if($ndata > 0 || $ndouble > 0)
      <div class="row">
        <div class="col-md-3">
            <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{$ndata}}</h3>

              <p>Total Uploaded</p>
            </div>
            <div class="icon">
            </div>
          </div>
        </div>
        <div class="col-md-3">

        </div>
        <div class="col-md-3">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{$ndouble}}</h3>

              <p>Failed</p>
            </div>
            <div class="icon">
            </div>
          </div>
        </div>
        <div class="col-md-3">

        </div>
      </div>
      <div class="row">
        <div class="col-md-6">


          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Successfuly Uploaded</h3>

              <div class="box-tools">

              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table">
                <tr>
                  <th style="width: 10px">No</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Status</th>
                </tr>
                @foreach($data as $n => $isi)
                <tr>
                  <td>{{$n+1}}</td>
                  <td>{{$isi->name}}</td>
                  <td>{{$isi->email}}</td>
                  <td>{{$isi->phone}}</td>
                  <td>{{$isi->status}}</td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>

        <div class="col-md-6">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Fail</h3>

              <div class="box-tools">

              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table">
                <tr>
                  <th style="width: 10px">No</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Status</th>
                </tr>
                @foreach($double as $x => $con)
                <tr>
                  <td>{{$x+1}}</td>
                  <td>{{$con->name}}</td>
                  <td>{{$con->email}}</td>
                  <td>{{$con->phone}}</td>
                  <td>{{$con->status}}</td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
      @endif
    </section>
@endsection
