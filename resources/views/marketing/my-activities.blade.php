@extends("crudbooster::admin_template")
@section("content")
<div class="row">
  <div class="col-md-6 col-sm-12 col-xs-12">
    <ul class="timeline">
    @if($data)
      @foreach($data as $isi)
      <!-- timeline time label -->
      <li class="time-label">
          <span class="bg-blue">
              {{date('d-M-Y',strtotime($isi['laporan']->updated_at))}}
          </span>
      </li>
      <!-- /.timeline-label -->

      <!-- timeline item -->
      <li>
          <!-- timeline icon -->
          <i class="fa fa-envelope bg-blue"></i>
          <div class="timeline-item">
              <span class="time"><i class="fa fa-clock-o"></i> {{date('H:i',strtotime($isi['laporan']->updated_at))}}</span>

              <h3 class="timeline-header"><a href="#">Follow up</a></h3>

              <div class="timeline-body">
                  {!!$isi['laporan']->content!!}
              </div>

              <div class="timeline-footer">
                <p>in reply to : </p>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title">{!!$isi['postup']->name!!}</h3>
                  </div>
                  <div class="panel-body">
                    {!!$isi['postup']->content!!}
                  </div>
                </div>
                  <a href="/crm/mkt-detail/{{$isi['postup']->user_id}}" class="btn btn-primary btn-xs">Detail</a>
              </div>
          </div>
      </li>
      <!-- END timeline item -->
      @endforeach
    @endif
    </ul>
  </div>

  <div class="col-md-6 col-sm-12 col-xs-12">
    <ul class="timeline">
    @if($prospects)
      @foreach($prospects as $activity)
      <!-- timeline time label -->
      <li class="time-label">
          <span class="bg-yellow">
              {{date('d-M-Y',strtotime($activity->updated_at))}}
          </span>
      </li>
      <!-- /.timeline-label -->

      <!-- timeline item -->
      <li>
          <!-- timeline icon -->
          <i class="fa fa-envelope bg-blue"></i>
          <div class="timeline-item">
              <span class="time"><i class="fa fa-clock-o"></i> {{date('H:i',strtotime($activity->updated_at))}}</span>

              <h3 class="timeline-header"><a href="#">Comment</a></h3>

              <div class="timeline-body">
                {{$activity->activity}}
              </div>

              <div class="timeline-footer">
                  <a href="/crm/client-detail-other/{{$activity->client_id}}" class="btn btn-primary btn-xs">Detail</a>
              </div>
          </div>
      </li>
      <!-- END timeline item -->
      @endforeach
    @endif
    </ul>
  </div>
</div>


@endsection
