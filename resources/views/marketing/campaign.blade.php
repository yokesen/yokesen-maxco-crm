@extends("crudbooster::admin_template")
@section("content")

  @foreach ($campaigns->chunk(4) as $chunk)
    <div class="row">
    @foreach($chunk as $isi)
    <div class="col-md-3">
      <!-- Widget: user widget style 1 -->
      <div class="box box-widget widget-user">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-black" style="background: url('{{url('/')}}/{{$isi->media}}') no-repeat center center; background-size:cover;">
        </div>
        <div class="widget-user-image">

        </div>
        <div class="box-footer">
           <p>{{$isi->title}}</p>
          <div class="row">

            <!-- /.col -->
            <div class="col-sm-6 border-right">
              <div class="description-block">
                <button data-clipboard-demo data-clipboard-text="{{ $isi->url . '?ref=' . CRUDBooster::myID() . '&so=direct' . '&campaign=' . $isi->id }}" class="btn btn-primary btn-block">Copy Link</button>
              </div>
              <!-- /.description-block -->
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
              <div class="description-block">
                <a href="{{route('ToolsShare',$isi->id)}}" class="btn btn-warning btn-block">Share Now</a>
              </div>
              <!-- /.description-block -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
      <!-- /.widget-user -->
    </div>
    @endforeach
    </div>
  @endforeach




@endsection


@section('calljs')

 <script src="{{url('/')}}/js/clipboard.min.js"></script>
@endsection

@section('jsonpage')
<script type="text/javascript">
$(function() {
  var clipboard = new Clipboard('.btn');
  clipboard.on('success', function(e) {
    swal({
      type: 'success',
      title: 'Text copied!',
      text: 'ctrl+v to paste',
      timer: 2000,
      showConfirmButton: false,
    })
  });
})
</script>
@endsection
