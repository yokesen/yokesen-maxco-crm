@extends("crudbooster::admin_template")

@section('cssonpage')
  <style>
  .box-body p img{
    width : 100% !important;
  }
  </style>
@endsection

@section("content")
  <div class="row">
    <div class="col-md-8">
      <div class="box box-widget widget-user">
        <div class="widget-user-header bg-black" style="background: url('/images/8057b41c01b62d9.jpg') center center; height:250px !important"></div>
        <div class="widget-user-image">
          <img class="img-circle" src="{{ $user->photo ? url($user->photo) : '/images/user-default.png' }}" alt="User Avatar">
        </div>
        <div class="box-footer">
          <div class="row">
            <h3 class="text-center">{{ ucfirst($user->name) }}</h3>
            <h5 class="text-center">{{ $user->longname }}</h5>
            <h5 class="text-center">{{ $id }}</h5>
          </div>
          <div class="row">
            <?php
            $leads = DB::table('clients')->where('parent',$id)->count();
            $contacts = DB::table('clients')->where('parent',$id)->where('status','contact')->count();
            $potentials = DB::table('clients')->where('parent',$id)->where('status','potential')->count();
            $win = DB::table('clients')->where('parent',$id)->where('status','win')->count();
            $lose = DB::table('clients')->where('parent',$id)->where('status','lose')->count();
            if($leads > 0){
              $ctl = $contacts/$leads;
              $ptc = $potentials/$leads;
              $wtp = $win/$leads;
              $ltl = $lose/$leads;
            }else{
              $ctl = 0;
              $ptc = 0;
              $wtp = 0;
              $lose = 0;
            }
             ?>
            <div class="col-sm-4 border-right">
              <div class="description-block">
                <h5 class="description-header"><a href="/crm/clients_po_from_level_7?ref={{ $id }}">{{$leads}}</a></h5>
                <span class="description-text">TOTAL LEADS</span>
              </div>
            </div>
            <div class="col-sm-4 border-right">
              <div class="description-block">
                <h5 class="description-header"><a href="/crm/client_my_contacts?ref={{ $id }}">{{$contacts}}</a></h5>
                <span class="description-text">CONTACTABLES</span>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="description-block">
                <h5 class="description-header"><a href="/crm/clients_my_potentials?ref={{ $id }}">{{$potentials}}</a></h5>
                <span class="description-text">POTENTIALS</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      @if($laporan)
      @foreach($laporan as $isi)

        <div class="box box-widget">
          <div class="box-header with-border">
            <div class="user-block">

              <img class="img-circle" src="{{ $isi['client']->photo ? url('/').'/'.$isi['client']->photo : '/images/user-default.png'}}" alt="User Image" />
              <span class="username"><a href="{{url('/')}}/crm/client-detail-other/{{$isi['client']->id}}">{{ ucfirst($isi['client']->name) }}</a></span>
              <span class="description">{{$isi['client']->whatsapp}}</span>
            </div>
            <div class="box-tools">
              <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
                <i class="fa fa-circle-o"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>

          <div class="box-body box-comments">
            @foreach($isi['comments'] as $comment)
            <div class="box-comment">
              <a href="">
                <img class="img-circle img-sm" src="{{ $comment->photo ? url('/').'/'.$comment->photo : '/images/user-default.png' }}" alt="User Image">
              </a>
              <div class="comment-text">
                <span class="username">
                  <a href="{{ url('crm/mkt-detail/' . $comment->user_id ) }}">{{ ucfirst($comment->name) }}</a>
                  <span class="text-muted pull-right">{{ date('d-m-y H:i',strtotime($comment->created_at))}}</span>
                </span>
                {{ $comment->activity }}
              </div>
            </div>
            @endforeach
          </div>
          <div class="box-footer">

            <form action="{{ route('SaveActivity') }}" method="post">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-10 col-sm-10 col-xs-10">
                  <img class="img-responsive img-circle img-sm" src="{{ CRUDBooster::myPhoto() }}" alt="Alt Text">
                  <!-- .img-push is used to add margin to elements next to floating images -->
                  <div class="img-push">
                    <input type="text" class="form-control input-sm" placeholder="Your comment here" name="activity" row="2" required>
                    <input type="hidden" name="client_id" value="{{$isi['client']->id}}">
                    <input type="hidden" name="parent_id" value="{{$isi['client']->parent}}">
                  </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-2">
                  <div class="form-group">
                    <input type="submit" class="form-control" value="Reply">
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      @endforeach
    @endif
    </div>

    <div class="col-md-4">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Struktur Tim</h3>
        </div>
        <div class="box-body no-padding">
          <table class="table">
            <tr>
              <th>Nama </th>
              <th>Jabatan</th>
              <th style="width: 40px">Status</th>
            </tr>
            @if($atasan)
              @foreach($atasan as $bos)
                <tr>
                  <td>{{ $bos->name }}</td>
                  <td>{{ $bos->shortname }}</td>
                  <td><span class="badge bg-{{ $bos->status == 'Active' ? 'green' : 'warning' }}">{{ $bos->status }}</span></td>
                </tr>
              @endforeach
            @endif
          </table>
        </div>
      </div>

      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Last Activities</h3>
        </div>
        <div class="box-body no-padding">
          <table class="table">
            <tr>
              <th>Aktivitas </th>
              <th>Tanggal</th>
            </tr>
            @if($log)
              @foreach($log as $d)
                <tr>
                  <td>{{ $d->description }}</td>
                  <td>{{ $d->created_at }}</td>
                </tr>
              @endforeach
            @endif
          </table>
        </div>
      </div>

      @if(count($internal) >0 )
        @foreach($internal as $mkt)
          <div class="box box-widget">
            <div class="box-header with-border">
              <div class="user-block">
                <img class="img-circle" src="{{ $mkt->komentator->photo ? url($mkt->komentator->photo) : 'https://www.gravatar.com/avatar/04a5d9e00ddb6d83b7c8171bedcd4a99?s=100' }}" alt="User Image">
                <span class="username"><a href="#">{{ ucfirst($mkt->komentator->name) }}</a></span>
                <span class="description">{{ $mkt->created_at ? $mkt->created_at->diffForHumans() : '' }}</span>
              </div>
              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
                  <i class="fa fa-circle-o"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="box-body">
                <!-- post text -->
                {!!$mkt->content!!}
                {{-- <button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button> --}}
                {{-- <button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button> --}}
                <span class="pull-right text-muted">{{ count($mkt->comments) }} comments</span>
              </div>
              <div class="box-footer box-comments" id="internalChatMessages" style="overflow: auto; width:100%; height:250px!important;">
                @foreach($mkt->comments as $comment)
                  <div class="box-comment">
                    <a href="{{ url('crm/mkt-detail/' . $comment->komentator->id ) }}">
                      <img class="img-circle img-sm" src="{{ $comment->komentator->photo ? url($comment->komentator->photo) : 'https://www.gravatar.com/avatar/04a5d9e00ddb6d83b7c8171bedcd4a99?s=100' }}" alt="User Image">
                    </a>
                    <div class="comment-text">
                      <span class="username">
                        <a href="{{ url('crm/mkt-detail/' . $comment->komentator->id ) }}">{{ ucfirst($comment->komentator->name) }}</a>
                        <span class="text-muted pull-right">{{ $comment->created_at ? $comment->created_at->diffForHumans() : '' }}</span>
                      </span>
                      {!! $comment->content !!}
                    </div>
                  </div>
                @endforeach
              </div>
              <div class="box-footer">
                <form action="{{route('postComment')}}" method="post">
                  {{ csrf_field() }}
                  <div class="row">
                    <div class="col-xs-8">
                      <img class="img-responsive img-circle img-sm" src="{{ CRUDBooster::myPhoto() }}" alt="Alt Text">
                      <!-- .img-push is used to add margin to elements next to floating images -->
                      <div class="img-push">
                        <input type="text" class="form-control input-sm" placeholder="Your comment here" name="comment" row="2" required>
                        <input type="hidden" class="form-control input-sm" name="post_id" value="{{ $mkt->id }}">
                        <input type="hidden" class="form-control input-sm" name="poster_id" value="{{ $mkt->user_id }}">
                      </div>
                    </div>
                    <div class="col-xs-4">
                      <div class="form-group">
                        <input type="submit" class="form-control" value="Reply">
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          @endforeach
        @else

          <div class="box box-widget">
            <div class="box-header with-border">
              <div class="user-block">
                <div class="user-block">
                  <img class="img-circle" src="https://www.gravatar.com/avatar/04a5d9e00ddb6d83b7c8171bedcd4a99?s=100" alt="User Image">
                  <span class="username"><a href="#">{{ ucfirst($user->name) }}</a></span>
                  <span class="description">{{ $mkt->created_at ? $mkt->created_at->diffForHumans() : '' }}</span>
                </div>
              </div>
              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
                  <i class="fa fa-circle-o"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="box-body">
                <span class="pull-right text-muted"> Click button to chat with {{ ucfirst($user->name) }}</span>

              </div>
              <div class="box-footer box-comments" id="internalChatMessages" style="overflow: auto; width:100%; height:250px!important;">

              </div>
              <div class="box-footer">
                <a href="{{route('TeamMarketingChat',$id)}}" class="btn btn-success btn-block">Chat Now!!</a>
              </div>
            </div>
          @endif
    </div>
  </div>
@endsection

@section('jsonpage')
<script type="text/javascript">
  $(document).ready(function(){
    var elem = document.getElementById('internalChatMessages');
    elem.scrollTop = elem.scrollHeight;
  });
</script>
@endsection
