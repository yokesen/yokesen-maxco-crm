@extends('beautymail::templates.sunny')

@section('content')

    @include ('beautymail::templates.sunny.heading' , [
        'heading' => 'Hello!'.$name,
        'level' => 'h1',
    ])

    @include('beautymail::templates.sunny.contentStart')

        <p>Today will be a great day! {{$email}}</p>
        <p>Dikirim untuk {{$name}} {{$time}}</p>

    @include('beautymail::templates.sunny.contentEnd')

    @include('beautymail::templates.sunny.button', [
        	'title' => 'Baca Smart Maxco App',
        	'link' => 'https://smartmaxco.co.id/get-smart-maxco-app'
    ])

@stop
