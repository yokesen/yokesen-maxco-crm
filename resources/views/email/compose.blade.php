@extends('beautymail::templates.sunny')

@section('content')

    @include ('beautymail::templates.sunny.heading' , [
        'heading' => $title,
        'level' => 'h1',
    ])

    @include('beautymail::templates.sunny.contentStart')

        {!!$content!!}

    @include('beautymail::templates.sunny.contentEnd')

    

@stop
