@extends("crudbooster::admin_template")

@section('cssonpage')

@endsection

@section("content")
  <form class="form-horizontal" action="{{ route('MegaUpdateProfile',$client->id)}}" method="POST">

    <div class="row"> <!--Start ROW-->

      <div class="col-md-12"> <!--Start col-md-6-->

        <div class="form-group">
          <label class="col-sm-2 control-label">PLU ID</label>
          <div class="col-sm-2">
            <input type="text" class="form-control" name="plu_id_1" placeholder="Plu_ID" value="{{ $client->Type }}" {{ $client->Type ? 'readonly' : '' }}>
          </div>
        </div>
      </div>
    </div>
  </form>
@endsection
