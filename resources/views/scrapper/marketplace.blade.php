@extends("crudbooster::admin_template")

@section('cssonpage')

@endsection

@section("content")
  <div class="row">
    <div class="col-md-3">
      <h3>MARKETPLACE DATA SCRAPPER</h3>
      <form action="{{route('DataScraperMarketplace')}}" method="POST">
        <div class="form-group">
          <input type="text" name="keyword" class="form-control" placeholder="keyword">
        </div>
        {{csrf_field()}}
        <div class="form-group">
          <input type="submit" class="form-control btn btn-primary" value="Submit Status">
        </div>
      </form>
    </div>

    <div class="col-md-9">

      @if($activities)

      <ul class="timeline timeline-inverse">
        @foreach($activities as $activity)
          <!-- timeline time label -->

          <li class="time-label">
            <span class="bg-blue">
              {{ date('d-m-Y')}}
            </span>
          </li>
          <!-- /.timeline-label -->
          <!-- timeline item -->
          <li>
            <i class="fa fa-user bg-aqua"></i>

            <div class="timeline-item">
              <span class="time"><i class="fa fa-clock-o"></i> {{$activity->title}}</span>

              <h3 class="timeline-header"><a href="{{$activity->link}}" target="_blank" >{{$activity->displayLink}}</a></h3>

              <div class="timeline-body">
                <div class="row">
                  <div class="col-md-3">
                    @if($activity->pagemap->cse_thumbnail[0]->src)
                    <img src="{{$activity->pagemap->cse_thumbnail[0]->src}}" alt="image search" width="150">
                    @endif
                  </div>
                  <div class="col-md-8">
                    {!! $activity->htmlSnippet !!}
                  </div>
                </div>
              </div>
            </div>
          </li>
          <!-- END timeline item -->
        @endforeach
      </ul>
      @endif

    </div>
  </div>
@endsection
