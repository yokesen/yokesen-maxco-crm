@extends("crudbooster::admin_template")

@section('cssonpage')

@endsection

@section("content")

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Budget Version</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="{{route('HippoHitung')}}" method="post">
                <div class="form-group">
                  <label>Social Media Development</label>
                  <select class="form-control" name="smdev">
                    <option value="1" {{$smd == '1' ? 'selected':''}}>Develop</option>
                    <option value="2" {{$smd == '2' ? 'selected':''}}>No Develop</option>
                  </select>
                </div>

                <div class="form-group">
                  <label>Sofware Maintenance</label>
                  <select class="form-control" name="smf">
                    <option value="1" {{$sofa == '1' ? 'selected':''}}>Mingguan</option>
                    <option value="2" {{$sofa == '2' ? 'selected':''}}>Bulanan</option>
                    <option value="3" {{$sofa == '3' ? 'selected':''}}>Kuartal</option>
                  </select>
                </div>

                <div class="form-group">
                  <label>Ads & Activation</label>
                  <select class="form-control" name="ads">
                    <option value="1" {{$ads == '1' ? 'selected':''}}>100%</option>
                    <option value="2" {{$ads == '2' ? 'selected':''}} >50%</option>
                    <option value="3" {{$ads == '3' ? 'selected':''}}>33.3%</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>On Demand Budget</label>
                  <select class="form-control" name="odb">
                    <option value="1" {{$odb == '1' ? 'selected':''}}>Standard</option>
                    <option value="2" {{$odb == '2' ? 'selected':''}}>Less</option>
                    <option value="3" {{$odb == '3' ? 'selected':''}}>Least</option>
                  </select>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="submit" name="submit" value="submit" class="btn btn-block btn-success">
              </form>

            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Total Investment</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tr>
                  <th style="width: 10px" class="hidden-xs">#</th>
                  <th>Description</th>
                  <th>Type</th>
                  <th>Amount</th>
                  <th class="hidden-xs">Value</th>
                </tr>
                <tr>
                  <td class="hidden-xs">1.</td>
                  <td>Mainframe Investment</td>
                  <td><span class="badge bg-blue">One Time</span></td>
                  <td class="text-right"><p id="roiMain">{{number_format($roiMain,'0','.','.')}}</p></td>
                  <td class="hidden-xs">
                    <div class="progress progress-xs">
                      <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="hidden-xs">2.</td>
                  <td>Software Maintenance</td>
                  <td><span class="badge bg-success">Annually</span></td>
                  <td class="text-right"><p id="roiSoft">{{number_format($roiSofa,'0','.','.')}}</p></td>
                  <td class="hidden-xs">
                    <div class="progress progress-xs">
                      <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="hidden-xs">3.</td>
                  <td>Social Media Maintain</td>
                  <td><span class="badge bg-success">Annually</span></td>
                  <td class="text-right"><p>{{number_format($roiSocmed,'0','.','.')}}</p></td>
                  <td class="hidden-xs">
                    <div class="progress progress-xs">
                      <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="hidden-xs">4.</td>
                  <td>Ads & Activation</td>
                  <td><span class="badge bg-success">Annually</span></td>
                  <td class="text-right"><p>{{number_format($roiAds,'0','.','.')}}</p></td>
                  <td class="hidden-xs">
                    <div class="progress progress-xs">
                      <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">SubTotal</td>
                  <td class="text-right"><p id="totalROI"><strong>{{number_format($subtotal,'0','.','.')}}</strong></p></td>
                  <td colspan="2"  class="hidden-xs"></td>
                </tr>
                <tr>
                  <td class="hidden-xs">5.</td>
                  <td>On Demand Budget</td>
                  <td><span class="badge bg-success">Annually</span></td>
                  <td class="text-right"><p>{{number_format($roiDemand,'0','.','.')}}</p></td>
                  <td class="hidden-xs">
                    <div class="progress progress-xs">
                      <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">Total</td>
                  <td class="text-right"><p id="totalROI">{{number_format($total,'0','.','.')}}</p></td>
                  <td colspan="2" class="hidden-xs"></td>
                </tr>

              </table>
            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">ROI Calculator</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tr>
                  <td>Impression</td>
                  <td class="text-center">
                    <span class="badge bg-red" style="width: 100%;">{{number_format($p1,'0','.','.')}}</span>
                  </td>
                </tr>
                <tr>
                  <td>Traffic</td>
                  <td class="text-center">
                    <span class="badge bg-red" style="width: 90%;">{{number_format($p2,'0','.','.')}}</span>
                  </td>
                </tr>
                <tr>
                  <td>Funnel</td>
                  <td class="text-center">
                    <span class="badge bg-yellow" style="width: 80%;">{{number_format($p3,'0','.','.')}}</span>
                  </td>
                </tr>
                <tr>
                  <td>Leads</td>
                  <td class="text-center">
                    <span class="badge bg-primary" style="width: 70%;">{{number_format($p4,'0','.','.')}}</span>
                  </td>
                </tr>
                <tr>
                  <td>Contact</td>
                  <td class="text-center">
                    <span class="badge bg-blue" style="width: 60%;">{{number_format($p5,'0','.','.')}}</span>
                  </td>
                </tr>
                <tr>
                  <td>Potential</td>
                  <td class="text-center">
                    <span class="badge bg-green" style="width: 50%;">{{number_format($p6,'0','.','.')}}</span>
                  </td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <div class="row hidden-xs">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Budget Map</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table">
                <tr>
                  <td style="width: 20%" class="hidden-xs"></td>
                  <td style="width: 20%" class="hidden-xs"></td>
                  <td style="width: 20%"><button type="button" name="button" class="btn btn-lg btn-block btn-primary"><p>{{number_format($roiMain,'0','.','.')}}</p><p>Mainframe Investment</p></button></td>
                  <td style="width: 20%" class="hidden-xs"></td>
                  <td style="width: 20%" class="hidden-xs"></td>
                </tr>

                <tr>
                  <td style="width: 20%"><button type="button" name="button" class="btn btn-lg btn-block btn-warning"><p id="softwareMaintenance">{{number_format($roiSofa,'0','.','.')}}</p><p>Software Maintenance</p></button></td>
                  <td style="width: 20%" class="hidden-xs"></td>
                  <td style="width: 20%" class="hidden-xs"></td>
                  <td style="width: 20%"><button type="button" name="button" class="btn btn-lg btn-block btn-primary"><p>{{number_format($roiSocmed,'0','.','.')}}</p><p>Social Media Maintain</p></button></td>
                  <td style="width: 20%" class="hidden-xs"></td>
                </tr>

                <tr>
                  <td style="width: 20%" class="hidden-xs"></td>
                  <td style="width: 20%"><button type="button" name="button" class="btn btn-lg btn-block btn-warning"><p id="adsActivation">{{number_format($roiAds,'0','.','.')}}</p><p>Ads & Activation</p></button></td>
                  <td style="width: 20%" class="hidden-xs"></td>
                  <td style="width: 20%" class="hidden-xs"></td>
                  <td style="width: 20%" class="hidden-xs"></td>
                </tr>

                <tr>
                  <td style="width: 20%" class="hidden-xs"></td>
                  <td style="width: 20%" class="hidden-xs"></td>
                  <td style="width: 20%" class="hidden-xs"></td>
                  <td style="width: 20%" class="hidden-xs"></td>
                  <td style="width: 20%"><button type="button" name="button" class="btn btn-lg btn-block btn-danger"><p id="ondemandBudget">{{number_format($roiDemand,'0','.','.')}}</p><p>On Demand Budget</p></button></td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">

            </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Payment Schedule</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="timeline">
                <?php $total = 0;?>

                @foreach ($payments as $i => $daftar)
                  <!-- timeline time label -->
                  <li class="time-label">
                    <span class="bg-green">
                      {{date('M-Y',strtotime(date('2018-12-01').'+ '.$i.'months'))}}
                    </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <!-- timeline icon -->
                    <i class="fa fa-calendar-times-o bg-blue"></i>
                    <div class="timeline-item">
                      <h3 class="timeline-header">{{date('F-Y',strtotime(date('2018-12-01').'+ '.$i.'months'))}}</h3>
                      <div class="timeline-body">
                        <div class="row">
                          <div class="col-md-6 col-sm-12">
                            <table class="table col-md-12 col-lg-6">
                              <?php $subtotal = 0;?>
                              @foreach ($daftar as $key => $value)
                                <tr>
                                  <td>{{$key}}</td>
                                  <td class="text-right">{{number_format($value,'0','.','.')}}</td>
                                  <?php
                                  $subtotal = $subtotal + $value;
                                  $total = $total + $value;
                                  ?>
                                </tr>
                              @endforeach
                              <tr>
                                <td class="text-right"><strong>Total</strong></td>
                                <td class="text-right"><strong>{{number_format($subtotal,'0','.','.')}}</strong></td>
                              </tr>
                            </table>
                          </div>
                        </div>
                      </div>

                      <div class="timeline-footer">

                      </div>
                    </div>
                  </li>
                  <!-- END timeline item -->
                @endforeach

              </ul>

            </div>
          </div>

        </div>
      </div>
    </section>

@endsection

@section('jsonpage')
<script type="text/javascript">
  var rMain = document.getElementById("roiMain").value;

  function softwareMaintenanceFunction(){
    var x = document.getElementById("smf").value;
    if(x==1){
      document.getElementById("softwareMaintenance").innerHTML = "500.000.000";
      document.getElementById("sfmain").innerHTML = "500.000.000";
    }else if(x==2){
      document.getElementById("softwareMaintenance").innerHTML = "304.000.000";
      document.getElementById("sfmain").innerHTML = "304.000.000";
    }else if(x==3){
      document.getElementById("softwareMaintenance").innerHTML = "140.000.000";
      document.getElementById("sfmain").innerHTML = "140.000.000";
    }
  }
</script>
@endsection
