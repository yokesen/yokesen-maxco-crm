@extends("crudbooster::admin_template")
@section("content")
<div class="row">
  @foreach($videos as $video)
    <div class="col-md-6">
      <!-- Box Comment -->
      <div class="box box-widget">
        <div class="box-header with-border">
          <div class="user-block">
            <img class="img-circle" src="/images/user-default.png" alt="User Image">
            <span class="username"><a href="#">OIMS</a></span>
            <span class="description">Shared publicly - {{date('d-m-Y H:i', strtotime($video->created_at))}}</span>
          </div>
          <!-- /.user-block -->
          <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
              <i class="fa fa-circle-o"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <video width="100%" controls>
            <source src="{{url('/')}}/video/{{$video->url}}" type="video/mp4">
          </video>

          <p>{{$video->judul}}</p>
        </div>
        <!-- /.box-body -->

        <!-- /.box-footer -->
        <div class="box-footer">
          {{-- <button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button> --}}
          {{-- <button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button> --}}
          <a href="{{url('/')}}//video/{{$video->url}}" class="btn btn-default btn-xs" download><i class="fa fa-download"></i> Download</a>
          <span class="pull-right text-muted">0 likes</span>
        </div>
        <!-- /.box-footer -->
      </div>
      <!-- /.box -->
    </div>
  @endforeach
    <!-- /.col -->
    </div>


@endsection

@section('callcss')
<link rel="stylesheet" href="{{ url('css/custom.css') }}">
@endsection
