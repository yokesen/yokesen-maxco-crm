@extends("crudbooster::admin_template")
@section("content")
<div class="row">
  <div class="col-md-8">
  @foreach($banners as $banner)
    <div class="box box-widget">
      <div class="box-header with-border">
        <div class="user-block">
          <img class="img-circle" src="/images/user-default.png" alt="User Image">
          <span class="username"><a href="#">OIMS</a></span>
          <span class="description">Shared publicly - {{date('d-m-Y H:i', strtotime($banner->created_at))}}</span>
        </div>
        <div class="box-tools">
          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
            <i class="fa fa-circle-o"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <img src="{{url('/')}}/{{ $banner->desktop_url}}" class="img-reponsive" style="width:100%">
        <p>{{$banner->judul}}</p>
      </div>

      <div class="box-footer">
        <a href="{{url('/')}}/{{ $banner->desktop_url}}" class="btn btn-default btn-xs" download><i class="fa fa-download"></i> Download</a>
        <span class="pull-right text-muted">0 likes</span>
      </div>
    </div>
  @endforeach
  </div>
  <div class="col-md-4">
  @foreach($banners as $banner)
    <div class="box box-widget">
      <div class="box-header with-border">
        <div class="user-block">
          <img class="img-circle" src="/images/user-default.png" alt="User Image">
          <span class="username"><a href="#">OIMS</a></span>
          <span class="description">Shared publicly - {{date('d-m-Y H:i', strtotime($banner->created_at))}}</span>
        </div>
        <div class="box-tools">
          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
            <i class="fa fa-circle-o"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <img src="{{url('/')}}//{{ $banner->mobile_url}}" class="img-reponsive" style="width:100%">
        <p>{{$banner->judul}}</p>
      </div>

      <div class="box-footer">
        <a href="{{url('/')}}//{{ $banner->mobile_url}}" class="btn btn-default btn-xs" download><i class="fa fa-download"></i> Download</a>
        <span class="pull-right text-muted">0 likes</span>
      </div>
    </div>
  @endforeach
  </div>
</div>


@endsection

@section('callcss')
<link rel="stylesheet" href="{{ url('css/custom.css') }}">
@endsection
