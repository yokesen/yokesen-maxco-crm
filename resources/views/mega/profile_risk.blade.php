<form class="form-horizontal" action="#" method="POST">
  <h4><strong>Tujuan Investasi</strong></h4>
  <div class="row"> <!--Start ROW-->
    <div class="col-sm-offset-1 col-sm-10"> <!--Start col-md-6-->

      <div class="form-group">

        <label>Apakah tujuan investasi anda ? (dapat lebih dari 1 jawaban)</label>
        <div class="checkbox">
          <label>
            <input type="checkbox">
            a.	Keamanan dana investasi
          </label>
        </div>

        <div class="checkbox">
          <label>
            <input type="checkbox">
            b.	Memperoleh pendapatan yang rutin
          </label>
        </div>

        <div class="checkbox">
          <label>
            <input type="checkbox">
            c.	Pendapatan dan pertumbuhan dalam jangka Panjang
          </label>
        </div>

        <div class="checkbox">
          <label>
            <input type="checkbox">
            d.	Pertumbuhan lebih tinggi meski dengan risiko investasi yang tinggi
          </label>
        </div>

        <div class="checkbox">
          <label>
            <input type="checkbox">
            e.	Lainnya
          </label>
        </div>

      </div>
    </div>
  </div><!--end Row-->

  <hr>
  <h4><strong>Sikap Umum Terhadap Risiko Investasi</strong></h4>

  <div class="row"> <!--Start ROW-->
    <div class="col-sm-offset-1 col-sm-10"> <!--Start col-md-6-->

      <div class="form-group">
        <label>1.	Berapakah usia anda saat ini ?</label>
        <div class="radio">
          <label>
            <input type="radio" name="risk1" value="4">
            a.	Antara 18 s/d 45 tahun
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk1" value="3">
            b.	Antara 46 s/d 55 tahun
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk1" value="2">
            c.	Antara 56 s/d 65 tahun
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk1" value="1">
            d.	Diatas 66 tahun
          </label>
        </div>
      </div>
    </div>
  </div><!--end Row-->

  <hr>

  <div class="row"> <!--Start ROW-->
    <div class="col-sm-offset-1 col-sm-10"> <!--Start col-md-6-->

      <div class="form-group">
        <label>2.	Berapa lama anda berencana untuk melakukan investasi ?</label>
        <div class="radio">
          <label>
            <input type="radio" name="risk2" value="4">
            a.	> 5 tahun
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk2" value="3">
            b.	> 2 tahun - < 5 tahun
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk2" value="2">
            c.	1 tahun - 2 tahun
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk2" value="1">
            d.	Antara < 1 tahun
          </label>
        </div>
      </div>
    </div>
  </div><!--end Row-->

  <hr>

  <div class="row"> <!--Start ROW-->
    <div class="col-sm-offset-1 col-sm-10"> <!--Start col-md-6-->

      <div class="form-group">
        <label>3.	Jenis investasi apakah yang saat ini telah anda miliki ?</label>
        <div class="radio">
          <label>
            <input type="radio" name="risk3" value="4">
            a.	Tabungan / Deposito / Emas / Reksadana / Obligasi / Saham / Valas / Unit Link
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk3" value="3">
            b.	Tabungan / Deposito / Emas / Reksadana / Obligasi / Unit Link
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk3" value="2">
            c.	Tabungan / Deposito / Obligasi
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk3" value="1">
            d.	Tabungan / Deposito
          </label>
        </div>
      </div>
    </div>
  </div><!--end Row-->

  <hr>

  <div class="row"> <!--Start ROW-->
    <div class="col-sm-offset-1 col-sm-10"> <!--Start col-md-6-->

      <div class="form-group">
        <label>4.	Berapa persen dari asset yang anda miliki saat ini (tidak termasuk property) yang disimpan dalam produk investasi dan/atau unit link yang nilainya dapat berfluktuasi?</label>
        <div class="radio">
          <label>
            <input type="radio" name="risk4" value="4">
            a.	Diatas 50%
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk4" value="3">
            b.	Antara >25% s/d 50%
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk4" value="2">
            c.	Antara >10% s/d 25%
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk4" value="1">
            d.	Antara >0% s/d 10%
          </label>
        </div>
      </div>
    </div>
  </div><!--end Row-->

  <hr>

  <div class="row"> <!--Start ROW-->
    <div class="col-sm-offset-1 col-sm-10"> <!--Start col-md-6-->

      <div class="form-group">
        <label>5.	Seberapa jauhkan pengetahuan anda, terutama tentang investasi di Pasar Modal dan Forex (Valas) ?</label>
        <div class="radio">
          <label>
            <input type="radio" name="risk5" value="4">
            a.	Sangat memahami, terutama tentang risiko yang melekat dalam investasi di Pasar Modal dan juga strateginya
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk5" value="3">
            b.	Memiliki pengetahuan tentang investasi di Pasar Modal, baik hasil maupun risiko yang melekat
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk5" value="2">
            c.	Memiliki sedikit pengetahuan, terutama tentang tingkat hasilnya
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk5" value="1">
            d.	Belum memahami sama sekali
          </label>
        </div>
      </div>
    </div>
  </div><!--end Row-->

  <hr>

  <div class="row"> <!--Start ROW-->
    <div class="col-sm-offset-1 col-sm-10"> <!--Start col-md-6-->

      <div class="form-group">
        <label>6.	Pada umumnya berapa persen dari pendapatan bulanan rumah tangga anda yang dapat dan akan digunakan untuk berinvestasi dan/atau unit link dan/atau tabungan ?</label>
        <div class="radio">
          <label>
            <input type="radio" name="risk6" value="4">
            a.	Diatas 50%
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk6" value="3">
            b.	Antara >25% s/d 50%
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk6" value="2">
            c.	Antara >10% s/d 25%
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk6" value="1">
            d.	Antara >0% s/d 10%
          </label>
        </div>
      </div>
    </div>
  </div><!--end Row-->

  <hr>

  <div class="row"> <!--Start ROW-->
    <div class="col-sm-offset-1 col-sm-10"> <!--Start col-md-6-->

      <div class="form-group">
        <label>7.	Jika investasi anda mulai merugi, apa yang akan anda lakukan ?</label>
        <div class="radio">
          <label>
            <input type="radio" name="risk7" value="4">
            a.	Menambah jumlah investasi, fluktuasi negatif dalam jangka waktu tertentu merupakan peluang untuk memperoleh potensi imbal hasil yang maksimal
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk7" value="3">
            b.	Tidak panik, karena masih ada kemungkinan nilai investasi akan kembali dan berpotensi untuk mendapatkan keuntungan dalam jangka Panjang
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk7" value="2">
            c.	Dijual jika potensi kerugian tersebut telah mencapai lebih dari 5%
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk7" value="1">
            d.	Langsung dijual, karena tidak bisa menerima penurunan nilai pokok investasi
          </label>
        </div>
      </div>
    </div>
  </div><!--end Row-->

  <hr>

  <div class="row"> <!--Start ROW-->
    <div class="col-sm-offset-1 col-sm-10"> <!--Start col-md-6-->

      <div class="form-group">
        <label>8.	Dalam berjalannya waktu, terjadi kenaikan dan penurunan nilai investasi yang biasa disebut fluktuasi. Pada umumnya semakin tinggi risiko investasi, semakin tinggi pula fluktuasinya yang biasanya sejalan dengan semakin tinggi potensi keuntungannya. Tingkat fluktuasi manakah yang anda terima ?</label>
        <div class="radio">
          <label>
            <input type="radio" name="risk8" value="4">
            a.	> 25%
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk8" value="3">
            b.	15% - ≤ 25%
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk8" value="2">
            c.	5% - < 15%
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk8" value="1">
            d.	0% - < 5%
          </label>
        </div>
      </div>
    </div>
  </div><!--end Row-->

  <hr>

  <div class="row"> <!--Start ROW-->
    <div class="col-sm-offset-1 col-sm-10"> <!--Start col-md-6-->

      <div class="form-group">
        <label>9.	Seberapa seringkah anda mengevaluasi hasil investasi anda :</label>
        <div class="radio">
          <label>
            <input type="radio" name="risk9" value="4">
            a.	Setiap Minggu
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk9" value="3">
            b.	Setiap Bulan
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk9" value="2">
            c.	Setiap 3 bulan
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="risk9" value="1">
            d.	Setiap 6 bulan atau lebih
          </label>
        </div>
      </div>
    </div>
  </div><!--end Row-->

  <hr>

  <div class="form-group">
    <div class="col-sm-offset-1 col-sm-10">
      {{ csrf_field() }}
      <button type="submit" class="btn btn-block btn-danger">Submit</button>
    </div>
  </div>
</form>
