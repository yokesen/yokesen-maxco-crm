<form class="form-horizontal" action="{{ route('MegaUpdateProfile',$client->id)}}" method="POST">

  <div class="row"> <!--Start ROW-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">Type</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Type" placeholder="Type" value="{{ $client->Type }}" {{ $client->Type ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">SA Code</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="SA_Code" placeholder="SA_Code" value="{{ $client->SA_Code }}" {{ $client->SA_Code ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">SID</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="SID" placeholder="SID" value="{{ $client->SID }}" {{ $client->SID ? 'readonly' : '' }}>
        </div>
      </div>

    </div> <!--end col-md-6-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">First Name</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="First_Name" placeholder="First_Name" value="{{ $client->First_Name }}" {{ $client->First_Name ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Middle Name</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Middle_Name" placeholder="Middle_Name" value="{{ $client->Middle_Name }}" {{ $client->Middle_Name ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Last Name</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Last_Name" placeholder="Last_Name" value="{{ $client->Last_Name }}" {{ $client->Last_Name ? 'readonly' : '' }}>
        </div>
      </div>
    </div> <!--end col-md-6-->

  </div><!--end Row-->

  <hr>

  <div class="row"> <!--Start ROW-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">Country of Nationality</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Country_of_Nationality" placeholder="Country_of_Nationality" value="{{ $client->Country_of_Nationality }}" {{ $client->Country_of_Nationality ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">ID No</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="ID_No" placeholder="ID_No" value="{{ $client->ID_No }}" {{ $client->ID_No ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">ID Expiration Date</label>
        <div class="col-sm-6">
          <input type="date" class="form-control" name="ID_Expiration_Date" placeholder="ID_Expiration_Date" value="{{ $client->ID_Expiration_Date }}" {{ $client->ID_Expiration_Date ? 'readonly' : '' }}>
        </div>
      </div>

    </div> <!--end col-md-6-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">NPWP No</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="NPWP_No" placeholder="NPWP_No" value="{{ $client->NPWP_No }}" {{ $client->NPWP_No ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">NPWP Registration Date</label>
        <div class="col-sm-6">
          <input type="date" class="form-control" name="NPWP_Registration_Date" placeholder="NPWP_Registration_Date" value="{{ $client->NPWP_Registration_Date }}" {{ $client->NPWP_Registration_Date ? 'readonly' : '' }}>
        </div>
      </div>

    </div> <!--end col-md-6-->

  </div><!--end Row-->

  <hr>

  <div class="row"> <!--Start ROW-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">Country of Birth</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Country_of_Birth" placeholder="Country_of_Birth" value="{{ $client->Country_of_Birth }}" {{ $client->Country_of_Birth ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">SA Code</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="SA_Code" placeholder="SA_Code" value="{{ $client->SA_Code }}" {{ $client->SA_Code ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Place of Birth</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Place_of_Birth" placeholder="Place_of_Birth" value="{{ $client->Place_of_Birth }}" {{ $client->Place_of_Birth ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Date of Birth</label>
        <div class="col-sm-6">
          <input type="date" class="form-control" name="Date_of_Birth" placeholder="Date_of_Birth" value="{{ $client->Date_of_Birth }}" {{ $client->Date_of_Birth ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Gender</label>
        <div class="col-sm-6">
          <select name="Gender" class="form-control" {{ $client->Gender ? ' disabled="true"' : '' }}>
            <option value="1" {{$client->Gender == '1' ? 'selected' : ''}}>1 Laki-laki</option>
            <option value="2" {{$client->Gender == '2' ? 'selected' : ''}}>2 Perempuan</option>
          </select>
        </div>
      </div>

    </div> <!--end col-md-6-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">Educational Background</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Educational_Background" placeholder="Educational_Background" value="{{ $client->Educational_Background }}" {{ $client->Educational_Background ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Mother Maiden Name</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Mother_Maiden_Name" placeholder="Mother_Maiden_Name" value="{{ $client->Mother_Maiden_Name }}" {{ $client->Mother_Maiden_Name ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Religion</label>
        <div class="col-sm-6">
          <select name="Religion" class="form-control" {{ $client->Religion ? ' disabled="true"' : '' }}>
            <option value="1" {{$client->Religion == '1' ? 'selected' : ''}}>1 Islam</option>
            <option value="2" {{$client->Religion == '2' ? 'selected' : ''}}>2 Kristen</option>
            <option value="3" {{$client->Religion == '3' ? 'selected' : ''}}>3 Katholik</option>
            <option value="4" {{$client->Religion == '4' ? 'selected' : ''}}>4 Budha</option>
            <option value="5" {{$client->Religion == '5' ? 'selected' : ''}}>5 Hindu</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Occupation</label>
        <div class="col-sm-6">
          <select name="Occupation" class="form-control" {{ $client->Occupation ? ' disabled="true"' : '' }}>
            <option value="1" {{$client->Religion == '1' ? 'selected' : ''}}>1 kerja 1</option>
            <option value="2" {{$client->Religion == '2' ? 'selected' : ''}}>2 kerja 2</option>
            <option value="3" {{$client->Religion == '3' ? 'selected' : ''}}>3 kerja 3</option>
            <option value="4" {{$client->Religion == '4' ? 'selected' : ''}}>4 kerja 4</option>
            <option value="5" {{$client->Religion == '5' ? 'selected' : ''}}>5 kerja 5</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Income Level</label>
        <div class="col-sm-6">
          <select name="Occupation" class="form-control" {{ $client->Income_Level ? ' disabled="true"' : '' }}>
            <option value="1" {{$client->Income_Level == '1' ? 'selected' : ''}}>1 < 50.000.000</option>
            <option value="2" {{$client->Income_Level == '2' ? 'selected' : ''}}>2 50.000.000-100.000.000</option>
            <option value="3" {{$client->Income_Level == '3' ? 'selected' : ''}}>3 100.000.000-200.000.000</option>
            <option value="4" {{$client->Income_Level == '4' ? 'selected' : ''}}>4 200.000.000-500.000.000</option>
            <option value="5" {{$client->Income_Level == '5' ? 'selected' : ''}}>5 > 500.000.000</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Marital Status</label>
        <div class="col-sm-6">
          <select name="Marital_Status" class="form-control" {{ $client->Marital_Status ? ' disabled="true"' : '' }}>
            <option value="1" {{$client->Marital_Status == '1' ? 'selected' : ''}}>1 Tidak Kawin</option>
            <option value="2" {{$client->Marital_Status == '2' ? 'selected' : ''}}>2 Kawin</option>
            <option value="3" {{$client->Marital_Status == '3' ? 'selected' : ''}}>3 Duda/Janda</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Spouse_Name</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Spouse_Name" placeholder="Spouse_Name" value="{{ $client->Spouse_Name }}" {{ $client->Spouse_Name ? 'readonly' : '' }}>
        </div>
      </div>

    </div> <!--end col-md-6-->

  </div><!--end Row-->

  <hr>

  <div class="row"> <!--Start ROW-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">Investor Risk Profile</label>
        <div class="col-sm-6">
          <select name="Investor_Risk_Profile" class="form-control" {{ $client->Investor_Risk_Profile ? ' disabled="true"' : '' }}>
            <option value="1" {{$client->Investor_Risk_Profile == '1' ? 'selected' : ''}}>1 Conservative</option>
            <option value="2" {{$client->Investor_Risk_Profile == '2' ? 'selected' : ''}}>2 Moderate</option>
            <option value="3" {{$client->Investor_Risk_Profile == '3' ? 'selected' : ''}}>3 Growth</option>
            <option value="4" {{$client->Investor_Risk_Profile == '4' ? 'selected' : ''}}>4 Aggresive</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Investment Objective</label>
        <div class="col-sm-6">
          <select name="Investment_Objective" class="form-control" {{ $client->Investment_Objective ? ' disabled="true"' : '' }}>
            <option value="1" {{$client->Investment_Objective == '1' ? 'selected' : ''}}>1 Gain</option>
            <option value="2" {{$client->Investment_Objective == '2' ? 'selected' : ''}}>2 Hedge</option>
            <option value="3" {{$client->Investment_Objective == '3' ? 'selected' : ''}}>3 Spekulasi</option>
            <option value="4" {{$client->Investment_Objective == '4' ? 'selected' : ''}}>4 Lain-lain</option>
          </select>
        </div>
      </div>


    </div> <!--end col-md-6-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">Source of Fund</label>
        <div class="col-sm-6">
          <select name="Source_of_Fund" class="form-control" {{ $client->Source_of_Fund ? ' disabled="true"' : '' }}>
            <option value="1" {{$client->Source_of_Fund == '1' ? 'selected' : ''}}>1 sumber 1</option>
            <option value="2" {{$client->Source_of_Fund == '2' ? 'selected' : ''}}>2 sumber 2</option>
            <option value="3" {{$client->Source_of_Fund == '3' ? 'selected' : ''}}>3 sumber 3</option>
            <option value="4" {{$client->Source_of_Fund == '4' ? 'selected' : ''}}>4 sumber 4</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Asset Owner</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Asset_Owner" placeholder="Asset_Owner" value="{{ $client->Asset_Owner }}" {{ $client->Asset_Owner ? 'readonly' : '' }}>
        </div>
      </div>

    </div> <!--end col-md-6-->

  </div><!--end Row-->

  <hr>

  <div class="row"> <!--Start ROW-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">KTP Address</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="KTP_Address" placeholder="KTP_Address" value="{{ $client->KTP_Address }}" {{ $client->KTP_Address ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">KTP City Code</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="KTP_City_Code" placeholder="KTP_City_Code" value="{{ $client->KTP_City_Code }}" {{ $client->KTP_City_Code ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">KTP Postal Code</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="KTP_Postal_Code" placeholder="KTP_Postal_Code" value="{{ $client->KTP_Postal_Code }}" {{ $client->KTP_Postal_Code ? 'readonly' : '' }}>
        </div>
      </div>

    </div> <!--end col-md-6-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">Home Phone</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Home_Phone" placeholder="Home_Phone" value="{{ $client->Home_Phone }}" {{ $client->Home_Phone ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Mobile Phone</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Mobile_Phone" placeholder="Mobile_Phone" value="{{ $client->Mobile_Phone }}" {{ $client->Mobile_Phone ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Facsimile</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Facsimile" placeholder="Facsimile" value="{{ $client->Facsimile }}" {{ $client->Facsimile ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Email</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Email" placeholder="Email" value="{{ $client->Email }}" {{ $client->Email ? 'readonly' : '' }}>
        </div>
      </div>

    </div> <!--end col-md-6-->

  </div><!--end Row-->

  <hr>

  <div class="row"> <!--Start ROW-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">Correspondence Address</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Correspondence_Address" placeholder="Correspondence_Address" value="{{ $client->Correspondence_Address }}" {{ $client->Correspondence_Address ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Correspondence City Code</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Correspondence_City_Code" placeholder="Correspondence_City_Code" value="{{ $client->Correspondence_City_Code }}" {{ $client->Correspondence_City_Code ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Correspondence City Name</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Correspondence_City_Name" placeholder="Correspondence_City_Name" value="{{ $client->Correspondence_City_Name }}" {{ $client->Correspondence_City_Name ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Correspondence Postal Code</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Correspondence_Postal_Code" placeholder="Correspondence_Postal_Code" value="{{ $client->Correspondence_Postal_Code }}" {{ $client->Correspondence_Postal_Code ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Country of Correspondence</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Country_of_Correspondence" placeholder="Country_of_Correspondence" value="{{ $client->Country_of_Correspondence }}" {{ $client->Country_of_Correspondence ? 'readonly' : '' }}>
        </div>
      </div>


    </div> <!--end col-md-6-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">Domicile Address</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Domicile_Address" placeholder="Domicile_Address" value="{{ $client->Domicile_Address }}" {{ $client->Domicile_Address ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Domicile City Code</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Domicile_City_Code" placeholder="Domicile_City_Code" value="{{ $client->Domicile_City_Code }}" {{ $client->Domicile_City_Code ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Domicile City Name</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Domicile_City_Name" placeholder="Domicile_City_Name" value="{{ $client->Domicile_City_Name }}" {{ $client->Domicile_City_Name ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Domicile Postal_Code</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Domicile_Postal_Code" placeholder="Domicile_Postal_Code" value="{{ $client->Domicile_Postal_Code }}" {{ $client->Domicile_Postal_Code ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Country of Domicile</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="Country_of_Domicile" placeholder="Country_of_Domicile" value="{{ $client->Country_of_Domicile }}" {{ $client->Country_of_Domicile ? 'readonly' : '' }}>
        </div>
      </div>

    </div> <!--end col-md-6-->

  </div><!--end Row-->

  <hr>

  <div class="row"> <!--Start ROW-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">Statement Type</label>
        <div class="col-sm-6">
          <select name="Statement_Type" class="form-control" {{ $client->Statement_Type ? ' disabled="true"' : '' }}>
            <option value="1" {{$client->Statement_Type == '1' ? 'selected' : ''}}>1 tipe 1</option>
            <option value="2" {{$client->Statement_Type == '2' ? 'selected' : ''}}>2 tipe 2</option>
            <option value="3" {{$client->Statement_Type == '3' ? 'selected' : ''}}>3 tipe 3</option>
            <option value="4" {{$client->Statement_Type == '4' ? 'selected' : ''}}>4 tipe 4</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">FATCA Status</label>
        <div class="col-sm-6">
          <select name="FATCA_Status" class="form-control" {{ $client->FATCA_Status ? ' disabled="true"' : '' }}>
            <option value="1" {{$client->FATCA_Status == '1' ? 'selected' : ''}}>1 status 1</option>
            <option value="2" {{$client->FATCA_Status == '2' ? 'selected' : ''}}>2 status 2</option>
            <option value="3" {{$client->FATCA_Status == '3' ? 'selected' : ''}}>3 status 3</option>
            <option value="4" {{$client->FATCA_Status == '4' ? 'selected' : ''}}>4 status 4</option>
          </select>
        </div>
      </div>

    </div> <!--end col-md-6-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">TIN Foreign TIN</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="TIN_Foreign_TIN" placeholder="TIN_Foreign_TIN" value="{{ $client->TIN_Foreign_TIN }}" {{ $client->TIN_Foreign_TIN ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">TIN Foreign TIN Issuance Country</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="TIN_Foreign_TIN_Issuance_Country" placeholder="TIN_Foreign_TIN_Issuance_Country" value="{{ $client->TIN_Foreign_TIN_Issuance_Country }}" {{ $client->TIN_Foreign_TIN_Issuance_Country ? 'readonly' : '' }}>
        </div>
      </div>

    </div> <!--end col-md-6-->

  </div><!--end Row-->

  <hr>

  <div class="row"> <!--Start ROW-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment Bank BIC Code 1</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_Bank_BIC_Code_1" placeholder="REDM_Payment_Bank_BIC_Code_1" value="{{ $client->REDM_Payment_Bank_BIC_Code_1 }}" {{ $client->REDM_Payment_Bank_BIC_Code_1 ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment Bank BI Member Code 1</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_Bank_BI_Member_Code_1" placeholder="REDM_Payment_Bank_BI_Member_Code_1" value="{{ $client->REDM_Payment_Bank_BI_Member_Code_1 }}" {{ $client->REDM_Payment_Bank_BI_Member_Code_1 ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment Bank Name 1</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_Bank_Name_1" placeholder="REDM_Payment_Bank_Name_1" value="{{ $client->REDM_Payment_Bank_Name_1 }}" {{ $client->REDM_Payment_Bank_Name_1 ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment Bank Country 1</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_Bank_Country_1" placeholder="REDM_Payment_Bank_Country_1" value="{{ $client->REDM_Payment_Bank_Country_1 }}" {{ $client->REDM_Payment_Bank_Country_1 ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment Bank Branch 1</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_Bank_Branch_1" placeholder="REDM_Payment_Bank_Branch_1" value="{{ $client->REDM_Payment_Bank_Branch_1 }}" {{ $client->REDM_Payment_Bank_Branch_1 ? 'readonly' : '' }}>
        </div>
      </div>

    </div> <!--end col-md-6-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment AC CCY 1</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_AC_CCY_1" placeholder="REDM_Payment_AC_CCY_1" value="{{ $client->REDM_Payment_AC_CCY_1 }}" {{ $client->REDM_Payment_AC_CCY_1 ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment AC No 1</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_AC_No_1" placeholder="REDM_Payment_AC_No_1" value="{{ $client->REDM_Payment_AC_No_1 }}" {{ $client->REDM_Payment_AC_No_1 ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment AC Name 1</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_AC_Name_1" placeholder="REDM_Payment_AC_Name_1" value="{{ $client->REDM_Payment_AC_Name_1 }}" {{ $client->REDM_Payment_AC_Name_1 ? 'readonly' : '' }}>
        </div>
      </div>

    </div> <!--end col-md-6-->

  </div><!--end Row-->

  <hr>

  <div class="row"> <!--Start ROW-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment Bank BIC Code 2</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_Bank_BIC_Code_2" placeholder="REDM_Payment_Bank_BIC_Code_2" value="{{ $client->REDM_Payment_Bank_BIC_Code_2 }}" {{ $client->REDM_Payment_Bank_BIC_Code_2 ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment Bank BI Member Code 2</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_Bank_BI_Member_Code_2" placeholder="REDM_Payment_Bank_BI_Member_Code_2" value="{{ $client->REDM_Payment_Bank_BI_Member_Code_2 }}" {{ $client->REDM_Payment_Bank_BI_Member_Code_2 ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment Bank Name 2</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_Bank_Name_2" placeholder="REDM_Payment_Bank_Name_2" value="{{ $client->REDM_Payment_Bank_Name_2 }}" {{ $client->REDM_Payment_Bank_Name_2 ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment Bank Country 2</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_Bank_Country_2" placeholder="REDM_Payment_Bank_Country_2" value="{{ $client->REDM_Payment_Bank_Country_2 }}" {{ $client->REDM_Payment_Bank_Country_2 ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment Bank Branch 2</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_Bank_Branch_2" placeholder="REDM_Payment_Bank_Branch_2" value="{{ $client->REDM_Payment_Bank_Branch_2 }}" {{ $client->REDM_Payment_Bank_Branch_2 ? 'readonly' : '' }}>
        </div>
      </div>

    </div> <!--end col-md-6-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment AC CCY 2</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_AC_CCY_2" placeholder="REDM_Payment_AC_CCY_2" value="{{ $client->REDM_Payment_AC_CCY_2 }}" {{ $client->REDM_Payment_AC_CCY_2 ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment AC No 2</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_AC_No_2" placeholder="REDM_Payment_AC_No_2" value="{{ $client->REDM_Payment_AC_No_2 }}" {{ $client->REDM_Payment_AC_No_2 ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment AC Name 2</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_AC_Name_2" placeholder="REDM_Payment_AC_Name_2" value="{{ $client->REDM_Payment_AC_Name_2 }}" {{ $client->REDM_Payment_AC_Name_2 ? 'readonly' : '' }}>
        </div>
      </div>

    </div> <!--end col-md-6-->

  </div><!--end Row-->

  <hr>

  <div class="row"> <!--Start ROW-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment Bank BIC Code 3</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_Bank_BIC_Code_3" placeholder="REDM_Payment_Bank_BIC_Code_3" value="{{ $client->REDM_Payment_Bank_BIC_Code_3 }}" {{ $client->REDM_Payment_Bank_BIC_Code_3 ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment Bank BI Member Code 3</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_Bank_BI_Member_Code_3" placeholder="REDM_Payment_Bank_BI_Member_Code_3" value="{{ $client->REDM_Payment_Bank_BI_Member_Code_3 }}" {{ $client->REDM_Payment_Bank_BI_Member_Code_3 ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment Bank Name 3</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_Bank_Name_3" placeholder="REDM_Payment_Bank_Name_3" value="{{ $client->REDM_Payment_Bank_Name_3 }}" {{ $client->REDM_Payment_Bank_Name_3 ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment Bank Country 3</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_Bank_Country_3" placeholder="REDM_Payment_Bank_Country_3" value="{{ $client->REDM_Payment_Bank_Country_3 }}" {{ $client->REDM_Payment_Bank_Country_3 ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment Bank Branch 3</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_Bank_Branch_3" placeholder="REDM_Payment_Bank_Branch_3" value="{{ $client->REDM_Payment_Bank_Branch_3 }}" {{ $client->REDM_Payment_Bank_Branch_3 ? 'readonly' : '' }}>
        </div>
      </div>

    </div> <!--end col-md-6-->

    <div class="col-md-6"> <!--Start col-md-6-->

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment AC CCY 3</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_AC_CCY_3" placeholder="REDM_Payment_AC_CCY_3" value="{{ $client->REDM_Payment_AC_CCY_3 }}" {{ $client->REDM_Payment_AC_CCY_3 ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment AC No 3</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_AC_No_3" placeholder="REDM_Payment_AC_No_3" value="{{ $client->REDM_Payment_AC_No_3 }}" {{ $client->REDM_Payment_AC_No_3 ? 'readonly' : '' }}>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">REDM Payment AC Name 3</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="REDM_Payment_AC_Name_3" placeholder="REDM_Payment_AC_Name_3" value="{{ $client->REDM_Payment_AC_Name_3 }}" {{ $client->REDM_Payment_AC_Name_3 ? 'readonly' : '' }}>
        </div>
      </div>

    </div> <!--end col-md-6-->

  </div><!--end Row-->

  <hr>

  <div class="form-group">
    <div class="col-sm-offset-1 col-sm-10">
      {{ csrf_field() }}
      <button type="submit" class="btn btn-block btn-danger">Submit</button>
    </div>
  </div>
</form>
