@extends("crudbooster::admin_template")
@section("content")
  <div class="row">
    <div class="col-md-3">
      <div class="box box-primary">
        <div class="box-body box-profile">
          <img class="profile-user-img img-responsive img-circle" src="{{ $client->photo == '' ? '/images/user-default.png' : $client->photo}}" alt="User profile picture">

          <h3 class="profile-username text-center">{{ ucwords($client->First_Name) }}</h3>

          <p class="text-muted text-center">{{ ucfirst($client->status) }}</p>

          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              @if($client->flag == 0)
                <a href="{{route('MegacreateSID',$client->id)}}" class="btn btn-warning btn-block">Create SID</a>
              @else
                <a download href="{{url('/')}}/storage/sid/sid{{$client->id}}.txt" class="btn btn-block btn-success">Download SID to S-Invest</a>
              @endif

            </li>
            <li class="list-group-item">
              <strong>Risk Profile : </strong>
              @if($client->Investor_Risk_Profile == '1')
                Conservative
              @elseif($client->Investor_Risk_Profile == '1')
                Moderate
              @elseif($client->Investor_Risk_Profile == '1')
                Growth
              @elseif($client->Investor_Risk_Profile == '1')
                Aggresive
              @endif
            </li>


          </ul>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <div class="col-md-9">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#settings" data-toggle="tab">Profile Detail</a></li>
          <li><a href="#risk" data-toggle="tab">Risk Profile</a></li>
          <li><a href="#transaksi" data-toggle="tab">Transaksi</a></li>
        </ul>

        <div class="tab-content">
          <div class="active tab-pane" id="settings">
            @include('mega.profile_detail')
          </div>
          <!-- /.tab-pane -->
          <div class="tab-pane" id="risk">
            @include('mega.profile_risk')
          </div>
          <!-- /.tab-pane -->
          <div class="tab-pane" id="transaksi">
            @include('mega.profile_transaction')
          </div>
          <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

@endsection
