<div class="row">
  <div class="col-sm-12">
    <form class="form-horizontal" action="{{ route('MegacreateSubscription',$client->id)}}" method="POST">

      <div class="row"> <!--Start ROW-->
        <div class="col-md-12"> <!--Start col-md-6-->
          <div class="form-group">
            <label class="col-sm-3 control-label">Produk Reksadana *</label>
            <div class="col-sm-5 col-offset-sm-2">
              <select name="reksadana" class="form-control" required>
                <option>---pilih---</option>
                <option value="912" >Ashmore-Dana-Ekuitas-Nusantara</option>
                <option value="941" >Ashmore-Dana-Obligasi-Nusantara</option>
                <option value="913" >Ashmore-Dana-Progresif-Nusantara</option>
                <option value="359" >Mega-Dana-Kas</option>
                <option value="371" >Mega-Dana-Obligasi-Dua</option>
                <option value="778" >Mega-Asset-Greater-Infrastructure</option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="row">

        <div class="col-md-6">
          <div class="form-group">
            <label class="col-sm-6 control-label">Amount *</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" name="amount" placeholder="Amount in Rupiah" required>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label class="col-sm-6 control-label">Fee *</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" name="fee" placeholder="fee in percent" required>
            </div>
          </div>
        </div>
      </div>

      <hr>

      <div class="form-group">
        <div class="col-sm-offset-1 col-sm-10">
          {{ csrf_field() }}
          <button type="submit" class="btn btn-block btn-primary">Submit</button>
        </div>
      </div>

      <hr>
    </form>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Responsive Hover Table</h3>

        <div class="box-tools">
          <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

            <div class="input-group-btn">
              <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr>
            <th>No</th>
            <th>Nama Reksadana</th>
            <th>Profil Risiko Investor</th>
            <th>Profil Risiko Produk</th>
            <th>Nominal Pemesanan / Jumlah Unit</th>
            <th>Biaya (%)</th>
            <th>Total</th>
          </tr>
          @foreach($reksadana as $nrek => $reks)
          <tr>
            <td>{{ $nrek + 1}}</td>
            <td>{{ str_replace('/reksadana/produk/'.$reks->reksadana.'/','',$reks->nama) }}</td>
            <td>
              @if($client->Investor_Risk_Profile == '1')
                Conservative
              @elseif($client->Investor_Risk_Profile == '1')
                Moderate
              @elseif($client->Investor_Risk_Profile == '1')
                Growth
              @elseif($client->Investor_Risk_Profile == '1')
                Aggresive
              @endif
            </td>
            <td>
              @if($reks->jenis == 'PASAR UANG')
                Conservative
              @elseif($reks->jenis == 'OBLIGASI')
                Moderate
              @elseif($reks->jenis == 'CAMPURAN')
                Growth
              @elseif($reks->jenis == 'SAHAM')
                Aggresive
              @else
                {{$reks->jenis}}
              @endif
            </td>
            <td class="text-right">{{ number_format($reks->amount,0,'.',',')}}</td>
            <td class="text-right">{{ number_format($reks->fee,2,'.',',')}}</td>
            <td class="text-right">{{ number_format(($reks->amount * $reks->fee /100) + $reks->amount,0,'.',',')}}</td>
          </tr>
          @endforeach

        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>
