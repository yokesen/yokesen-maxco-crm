@extends("crudbooster::admin_template")
@section("content")
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Daftar Aset Kelolaan</h3>

          <div class="box-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

              <div class="input-group-btn">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Reksadana</th>
                <th>Tgl Transaksi</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $year = date('Y');
              $days = cal_days_in_month(CAL_GREGORIAN, 2,$year) + 337;
              ?>
              @foreach($daftar as $n => $reksadana)
                <tr>
                  <td colspan="4" class="text-center"><strong>{{ str_replace("/reksadana/produk/".$daftar[$n][0]->reksadana."/",'',$daftar[$n][0]->nama)}}</strong></td>
                </tr>
                <?php $total =0;?>
                @foreach($reksadana as $x => $trx)
                  <tr>
                    <td>{{$x+1}}</td>
                    <td>{{ str_replace("/reksadana/produk/".$trx->reksadana."/",'',$trx->nama)}}</td>
                    <td>{{$trx->tgl_approve}}</td>
                    <td class="text-right">{{number_format($trx->amount,0,',','.')}}</td>
                  </tr>
                  <?php $total += $trx->amount;?>
                @endforeach
                <tr>
                  <td colspan="3" class="text-right">Total</td>
                  <td class="text-right"><strong>{{number_format($total,0,',','.')}}</strong></td>
                </tr>
                <tr>
                  <td colspan="3" class="text-right">Fee 0.5 % (for {{$days}})</td>

                  <td class="text-right"><strong>{{number_format($total * 0.005 / $days,0,',','.')}}</strong></td>
                </tr>
                <tr>
                  <td colspan="4"></td>
                </tr>

              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
