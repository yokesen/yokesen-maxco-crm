<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return Redirect::to(env('APP_URL'));
});

Route::prefix('lab')->group(function () {
  Route::get('maxco-api', 'LabController@apiMaxco');
  Route::get('geolocation', 'DashboardController@geolocation');
  Route::get('email','MailController@testmail');
  Route::get('parent','ClientController@auto_parent_name');
  Route::get('email/validation','MailController@verifikasi_email');
  Route::get('registeribportal','LabController@registeribportal');
  Route::get('phone','LabController@phoneEqualWhatsapp');
});

Route::get('/no-whatsapp/{email}','whatsappController@noWhatsapp')->name('noWhatsapp');
Route::post('/no-whatsapp/process/{email}','whatsappController@post_temp')->name('postWhatsapp');
Route::get('/verify-whatsapp/verify','whatsappController@verify_whatsapp')->name('verify_whatsapp');
Route::post('/verificating-whatsapp','whatsappController@verificating_whatsapp')->name('verificatingWhataspp');

Route::get('/no-phonenumber/{email}','phoneController@nophonenumber')->name('nophonenumber');
Route::post('/no-phonenumber/process/{email}','phoneController@postphonenumber')->name('postPhonenumber');
Route::get('/verify-phone-number/verify/{email}','phoneController@verify_phonenumber')->name('verifyphonenumber');
Route::post('/verificating-phone-number','phoneController@verificating_phonenumber')->name('verificatingphonenumber');

Route::get('/1/laporan','ToolsController@laporan');
Route::get('/export/laporan','ToolsController@export');

Route::middleware('Login')->group(function () {

  Route::get('crm/data-login/','DataController@login')->name('secretLoginDataApi');
  Route::get('crm/data-list/','DataController@dataList')->name('secretDataListApi');
  Route::get('crm/client-claim/{id}','ClientController@claim')->name('clientClaim');

  Route::get('crm/client-detail/{id}',[
    'uses' => 'ClientController@detail',
    'as' => 'ClientDetail'
  ]);

  Route::post('email/post','MailController@daftarkirim')->name('postEmail');

  Route::get('crm/lab/client-lab/{id}',[
    'uses' => 'ClientController@labClient',
    'as' => 'ClientDetailLab'
  ]);

  Route::post('crm/update-client-detail/{id}',[
    'uses' => 'ClientController@updateDetail',
    'as' => 'UpdateClientDetail'
  ]);

  Route::get('crm/client-detail-other/{id}',[
    'uses' => 'ClientController@detail_other',
    'as' => 'ClientDetailOther'
  ]);


  Route::post('crm/client-activity',[
    'uses' => 'ClientController@saveactivity',
    'as' => 'SaveActivity'
  ]);

  Route::get('crm/tools/video',[
    'uses' => 'VideoController@index',
    'as' => 'ToolsVideo'
  ]);

  Route::get('crm/tools/download',[
    'uses' => 'VideoController@downloadVideo',
    'as' => 'DownloadVideo'
  ]);

  Route::get('crm/tools/banner',[
    'uses' => 'BannerController@index',
    'as' => 'ToolsBanner'
  ]);

  Route::get('crm/welcome-page',[
    'uses' => 'WelcomeController@index',
    'as' => 'WelcomePage'
  ]);

  Route::get('crm/dashboard',[
    'uses' => 'DashboardController@index',
    'as' => 'Dashboard'
  ]);

  Route::get('crm/board/dashboard',[
    'uses' => 'DashboardController@board',
    'as' => 'boardDashboard'
  ]);

  Route::get('crm/dashboard-lab',[
    'uses' => 'DashboardController@lab',
    'as' => 'DashboardLab'
  ]);

  Route::post('crm/api/clientgraph',[
    'uses' => 'DashboardController@clientgraph',
    'as' => 'apiclientgraph'
  ]);
  Route::post('crm/api/visitorreport',[
    'uses' => 'DashboardController@visitorreport',
    'as' => 'apivisitorreport'
  ]);

  Route::get('crm/team',[
    'uses' => 'TeamController@myTeam',
    'as' => 'TeamIndex'
  ]);

  Route::get('crm/toko/{id}',[
    'uses' => 'TeamController@myTeamStore',
    'as' => 'TeamToko'
  ]);

  Route::get('crm/team/{month}',[
    'uses' => 'TeamController@myTeamMonth',
    'as' => 'TeamIndexMonth'
  ]);

  Route::post('crm/team-date-custom/',[
    'uses' => 'TeamController@myTeamCustom',
    'as' => 'TeamIndexCustom'
  ]);

  Route::get('crm/pengumuman-view',[
    'uses' => 'PengumumanController@index',
    'as' => 'Pengumuman'
  ]);

  Route::get('crm/pengumuman-detail/{id}',[
    'uses' => 'PengumumanController@detail',
    'as' => 'PengumumanDetail'
  ]);

  Route::get('crm/laporan-view',[
    'uses' => 'LaporanController@index',
    'as' => 'TeamLaporan'
  ]);

  Route::post('crm/post-comment',[
    'uses' => 'TeamController@postComment',
    'as' => 'postComment'
  ]);

  Route::get('crm/mkt-detail/{id}',[
    'uses' => 'TeamController@marketing',
    'as' => 'TeamMarketingDetail'
  ]);

  Route::get('crm/mkt-chat/{id}',[
    'uses' => 'TeamController@initiateChat',
    'as' => 'TeamMarketingChat'
  ]);

  Route::get('crm/mkt-learning',function(){
    return view('marketing.lms');
  });

  Route::get('crm/mkt-asses',function(){
    return view('marketing.asses');
  });

  Route::get('crm/campaigns','TeamController@campaign')->name('campaignIndex');

  Route::get('crm/mest-calculate',[
    'uses' =>'TransactionController@mestika',
    'as' => 'mestikaPrice'
  ]);

  Route::post('crm/mest-calculate',[
    'uses' =>'TransactionController@hitung',
    'as' => 'mestikaHitung'
  ]);

  Route::get('crm/hippo-calculate',[
    'uses' =>'TransactionController@hippo',
    'as' => 'hippoPrice'
  ]);

  Route::post('crm/hippo-calculate',[
    'uses' =>'TransactionController@hip_hitung',
    'as' => 'HippoHitung'
  ]);

  //Start counter share
  Route::get('crm/api_nambah_counter/{id}/{link}',[
    'uses' => 'TeamController@share_counter',
    'as' => 'TeamShareCounter'
  ]);

  //End Counter share

  Route::get('crm/reporting','TeamController@reporting')->name('teamReporting');

  Route::post('crm/change-date-follow-up/{id}',[
    'uses' => 'ClientController@dateFollowUp',
    'as' => 'ChangeDateFollowUp'
  ]);

  Route::post('crm/change-status-follow-up/{id}',[
    'uses' => 'ClientController@statusFollowUp',
    'as' => 'ChangeStatusClient'
  ]);

  Route::post('crm/assign-lead/{id}',[
    'uses' => 'ClientController@assignLead',
    'as' => 'assignLead'
  ]);

  Route::post('crm/selected-share-campaign',[
    'uses' => 'ToolsController@selectedshare',
    'as' => 'PostSelectedShare'
  ]);

  Route::get('crm/share-campaign/{id}',[
    'uses' => 'ToolsController@share',
    'as' => 'ToolsShare'
  ]);

  Route::get('crm/share-campaign2/{id}',[
    'uses' => 'ToolsController@share2',
    'as' => 'ToolsShare2'
  ]);

  Route::get('crm/email-template/{email_id}',[
    'uses' => 'ToolsController@emailTemplate',
    'as' => 'ToolsEmailTemplate'
  ]);

  Route::get('crm/my-activities/',[
    'uses' => 'TeamController@myActivities',
    'as' => 'MyActivities'
  ]);

  Route::get('crm/locked-inactive/',[
    'uses' => 'TeamController@inactive',
    'as' => 'Inactive'
  ]);

  Route::post('send-email',[
    'uses' => 'ToolsController@sendemail',
    'as' => 'SendEmail'
  ]);

  Route::get('crm/release/reksadana/{id}',[
    'uses' => 'ReksadanaController@release',
    'as' => 'ReksadanaRelease'
  ]);

  Route::get('crm/email-blast/campaign/{id}',[
    'uses' => 'MailController@blastemail',
    'as' => 'BlastEmailRelease'
  ]);

  Route::post('warroom/search/social',[
    'uses' => 'SocialDataController@social_search',
    'as' => 'DataScraperSocial'
  ]);

  Route::post('warroom/search/marketplace',[
    'uses' => 'SocialDataController@marketplace_search',
    'as' => 'DataScraperMarketplace'
  ]);

  Route::get('warroom/search/twitter/{q}',[
    'uses' => 'SocialDataController@twitter_search',
    'as' => 'DataScraperTwitter'
  ]);

  Route::get('crm/search/twitter',[
    'uses' => 'SocialDataController@view_twitter_search',
    'as' => 'ViewDataScraperTwitter'
  ]);

  Route::get('crm/search/social',[
    'uses' => 'SocialDataController@view_social_search',
    'as' => 'ViewDataScraperSocial'
  ]);

  Route::get('crm/search/marketplace',[
    'uses' => 'SocialDataController@view_marketplace_search',
    'as' => 'ViewDataScraperMarketplace'
  ]);

  Route::get('crm/sid/{id}',[
    'uses' => 'MegaController@viewprofile',
    'as' => 'MegaViewProfile'
  ]);

  Route::post('crm/update/profile/sid/{id}',[
    'uses' => 'MegaController@update',
    'as' => 'MegaUpdateProfile'
  ]);

  Route::get('crm/createSID/{id}',[
    'uses' => 'MegaController@makeprofiletxt',
    'as' => 'MegacreateSID'
  ]);

  Route::get('crm/createSub',[
    'uses' => 'MegaController@makeSub',
    'as' => 'MegacreateSub'
  ]);

  Route::post('crm/mega/subscription/{id}',[
    'uses' => 'MegaController@subscription',
    'as' => 'MegacreateSubscription'
  ]);

  Route::get('crm/mega/fee/transaction',[
    'uses' => 'TransactionController@list',
    'as' => 'MegaFeeTransc'
  ]);

  Route::get('crm/quickchicken/plu',[
    'uses' => 'QuickChickenController@plu',
    'as' => 'QCPlu'
  ]);

  Route::get('crm/quickchicken/publish_plu',[
    'uses' => 'QuickChickenController@publish_plu',
    'as' => 'QCPublishPlu'
  ]);

  Route::get('crm/push_social',[
    'uses' => 'SocialDataController@push_social',
    'as' => 'PushSocial'
  ]);

  Route::get('lab',[
    'uses' => 'whatsappController@lab'
  ]);

  Route::post('/crm/whatsapp/send/{phone}',[
    'uses' => 'whatsappController@send',
    'as' => 'whatsappSend'
  ]);

  Route::post('/crm/whatsapp-image/send/{phone}/{id}',[
    'uses' => 'whatsappController@sendImage',
    'as' => 'whatsappImageSend'
  ]);

  Route::get('/upload',[
    'uses' => 'UploadController@upload',
    'as' => 'uploadView'
  ]);

  Route::post('/upload',[
    'uses' => 'UploadController@upload_process',
    'as' => 'uploadProcess'
  ]);

  Route::get('/lab/show',[
    'uses' => 'LabController@showdata',
    'as' => 'labShow'
  ]);

  Route::post('/lab/upload',[
    'uses' => 'LabController@postUploadImage',
    'as' => 'labUploadImage'
  ]);

  Route::get('/lab/reminder',[
    'uses' => 'LabController@LoadDataReminder'
  ]);

  Route::get('/cari/{key}',[
    'uses' => 'LabController@loadData'
  ]);

  Route::get('/pilih/{id}',[
    'uses' => 'LabController@dataImage'
  ]);

  Route::get('/chat-reveal/{id}',[
    'uses' => 'whatsappController@reveal'
  ]);

  Route::get('/chat/pilihan/{id}/{client}',[
    'uses' => 'whatsappController@terpilih'
  ]);

  Route::get('/crm/pictures/addpictures',[
    'uses' => 'ToolsController@addpictures',
    'as' => 'addpictures'
  ]);

  Route::get('/crm/client/add',[
    'uses' => 'ClientController@addClient',
    'as' => 'tambahCLient'
  ]);

  Route::post('/crm/client/add',[
    'uses' => 'ClientController@postClient',
    'as' => 'CreateClient'
  ]);

  Route::post('/crm/upload/whatsapp','whatsappController@uploadFile')->name('whatsappUpload');
  Route::post('/crm/client/follow',[
    'uses' => 'ClientController@postFollow',
    'as' => 'postfollow'
  ]);

});
